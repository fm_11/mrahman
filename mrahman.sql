/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.26 : Database - db_mrahman
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_mrahman` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_mrahman`;

/*Table structure for table `designation` */

DROP TABLE IF EXISTS `designation`;

CREATE TABLE `designation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `remarks` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `designation` */

insert  into `designation`(`id`,`code`,`name`,`is_active`,`remarks`) values (2,'GIS','GIS & Remote Sensing',1,'GIS & Remote Sensing'),(6,'DS','Data Science & Machine Learning',1,'Data Science & Machine Learning'),(7,'BDP','Big Data Analytics',1,'Big Data Analytics'),(9,'GD','Graphics Design',1,'Graphics Design'),(10,'RES','Robotics and Embedded Systems ',1,'Robotics and Embedded Systems '),(11,'MD','Mobile Development (Flutter)',1,'Mobile Development (Flutter)'),(12,'WordPress','WordPress',1,'WordPress'),(13,'CMS','CMS',1,'CMS'),(14,'WDD','Web Design and Development',1,'web design and development'),(15,'MAD','Mobile App Development ',1,'mobile app development '),(16,'ESCI','Email Signature Creation and Integration',1,'email signature creation and integration'),(17,'ETC','Email Template Creation',1,'email template creation'),(18,'106','Assistant Director (Operations)',1,'Assistant Director (Operations) at Bangladesh Center for Analytics - BCA'),(19,'101','Honorary Chairman, BoG, BCA ',1,'Honorary Chairman at Bangladesh Center for Analytics - BCA'),(20,'102','Assistant Director (Administration & Finance)',1,'Assistant Director (Administration & Finance) at Bangladesh Center for Analytics - BCA\r\n'),(21,'103','Program Director ( OpenTalk)',1,'Program Director ( OpenTalk) at Bangladesh Center for Analytics - BCA '),(22,'104','Executive Director (Acting) & Director (Technical)',1,'Executive Director (Acting) & Director (Technical) at Bangladesh Center for Analytics - BCA '),(23,'105','Public Relationship Officer (PRO)',1,'Public Relationship Officer (PRO) at Bangladesh Center for Analytics - BCA '),(24,'106','Software Engineer / Assistant Director (Technical)',1,'Software Engineer / Assistant Director (Technical) at Bangladesh Center for Analytics - BCA '),(25,'107','Director ( Operations)',1,'Director ( Operations) at Bangladesh Center for Analytics - BCA '),(26,'108','Honorary Adviser,  BoG, BCA',1,' Honorary Adviser,  BoG, BCA at Bangladesh Center for Analytics - BCA '),(27,'IOT','Internet of Things (IoT)',1,'Internet of Things (IoT)'),(28,'CS','Cyber Security',1,'Cyber Security'),(29,'Power BI','Business Intelligence (Power BI)',1,'Business Intelligence (Power BI)');

/*Table structure for table `tbl_about_me` */

DROP TABLE IF EXISTS `tbl_about_me`;

CREATE TABLE `tbl_about_me` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `about_1` text,
  `about_2` text,
  `file_location` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_about_me` */

insert  into `tbl_about_me`(`id`,`title`,`about_1`,`about_2`,`file_location`) values (1,'About Me','<p xss=\"removed\"><span xss=\"removed\">Welcome!<o></o></span></p><p xss=\"removed\"><span xss=\"removed\"> </span></p><p xss=\"removed\">\r\n\r\n\r\n\r\n</p><p xss=\"removed\"><span xss=\"removed\">My name is Muhammad\r\nM Rahman. I am working as a Research Scientist in the Department of Materials\r\nScience and NanoEngineering at Rice University, USA. I completed a Ph.D. in\r\nFiber Science from Cornell University. Before joining to a Ph.D. program, I\r\nreceived my B.Sc. degree in Mechanical Engineering from Bangladesh University\r\nof Engineering and Technology (BUET), Bangladesh and M.Sc. degree in Mechanical\r\nEngineering from Tuskegee University, USA.  </span></p><p xss=\"removed\"><span xss=\"removed\"><o><br></o></span></p><p xss=\"removed\"><span xss=\"removed\">I am a curious scientist working in the interdisciplinary area of mechanical and materials engineering with a chemical and biological outlook. My research interests focus on the development of high-performance composite materials using multifunctional fibers, polymers, and nanomaterials with an emphasis in sustainable design. The ultimate goal of my research is to open up new avenues in the fabrication of composite materials with a simple, cost-effective and eco-friendly methodology in structural applications considering environmental and economic viewpoint. And, the revelation of structure, composition and property relationship behavior among the novel combinations of these materials is the primary concern of my research.</span><span xss=\"removed\"> </span><br></p><p xss=\"removed\"><span xss=\"removed\"> </span></p><p xss=\"removed\"><span xss=\"removed\"><o><span xss=\"removed\">I hope you enjoy my website and find some useful information here. Please feel free to contact me if you have any questions.</span><br></o></span></p>','<p xss=\"removed\"><span xss=removed>I am a curious scientist working in the interdisciplinary\r\narea of mechanical and materials engineering with a chemical and biological\r\noutlook. My research interests focus on the development of high-performance\r\ncomposite materials using multifunctional fibers, polymers, and nanomaterials\r\nwith an emphasis in sustainable design. The ultimate goal of my research is to\r\nopen up new avenues in the fabrication of composite materials with a simple,\r\ncost-effective and eco-friendly methodology in structural applications\r\nconsidering environmental and economic viewpoint. And, the revelation of\r\nstructure, composition and property relationship behavior among the novel\r\ncombinations of these materials is the primary concern of my research.</span><span xss=removed> </span><br></p>\r\n\r\n<p xss=removed><span xss=removed> </span></p>\r\n\r\n<span xss=removed>I hope\r\nyou enjoy my website and find some useful information here. Please feel free to\r\ncontact me if you have any questions.</span>','about_me_1628873789.jpg');

/*Table structure for table `tbl_blood_group` */

DROP TABLE IF EXISTS `tbl_blood_group`;

CREATE TABLE `tbl_blood_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`(333))
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_blood_group` */

insert  into `tbl_blood_group`(`id`,`name`) values (1,'A+'),(2,'B+'),(3,'O+'),(4,'AB+'),(5,'A-'),(6,'B-'),(7,'O-'),(8,'AB-'),(9,'N/A');

/*Table structure for table `tbl_contact` */

DROP TABLE IF EXISTS `tbl_contact`;

CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(600) DEFAULT NULL,
  `message` text,
  `email` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `subject` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_contact` */

insert  into `tbl_contact`(`id`,`name`,`message`,`email`,`date`,`phone`,`subject`) values (1,'School','fhfhfh','rakibul@gmail.com','2021-08-12',NULL,'A error found'),(2,'ds','sd','sd@a.f','2021-08-14',NULL,'sd');

/*Table structure for table `tbl_contact_info` */

DROP TABLE IF EXISTS `tbl_contact_info`;

CREATE TABLE `tbl_contact_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(500) NOT NULL,
  `tin_number` varchar(200) DEFAULT NULL,
  `address` text,
  `email` varchar(500) DEFAULT NULL,
  `mobile` varchar(500) DEFAULT NULL,
  `facebook_address` varchar(500) DEFAULT NULL,
  `picture` varchar(500) DEFAULT NULL,
  `web_address` varchar(100) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `google_plus` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `linkedin` varchar(200) DEFAULT NULL,
  `whatsapp` varchar(200) DEFAULT NULL,
  `login_page_image_added` int(11) DEFAULT '0',
  `open_talk` int(11) DEFAULT NULL,
  `webinar` int(11) DEFAULT NULL,
  `workshop` int(11) DEFAULT NULL,
  `course` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_contact_info` */

insert  into `tbl_contact_info`(`id`,`school_name`,`tin_number`,`address`,`email`,`mobile`,`facebook_address`,`picture`,`web_address`,`twitter`,`google_plus`,`youtube`,`linkedin`,`whatsapp`,`login_page_image_added`,`open_talk`,`webinar`,`workshop`,`course`) values (1,'M Rahman','Muhammad M Rahman','235 Space Science &amp; Technology,&nbsp;Rice University, Houston, TX 77005 ','mr64@rice.edu','(334) 444-9202','<p>I am working as a Research <span xss=removed>Scientist in the Department of Materials Science and NanoEngineering at Rice University, USA</span></p>','orga_1628873101.mp4','https://bca.com.bd/','https://twitter.com/bca_bd','#','https://www.youtube.com/channel/UCi6bf2kB2BPDk6mdWE-3q0Q','https://www.linkedin.com/in/bcabd/','#',0,2,3,1,3);

/*Table structure for table `tbl_content_details` */

DROP TABLE IF EXISTS `tbl_content_details`;

CREATE TABLE `tbl_content_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `message` text,
  `photo_location` varchar(200) DEFAULT NULL,
  `content_type` enum('L','P','Y','E','I') NOT NULL,
  `youtube_link` text CHARACTER SET latin1,
  `menu_id` int(11) DEFAULT NULL,
  `order_no` int(11) DEFAULT '0',
  `is_active` int(1) DEFAULT '1',
  `employee_id` int(11) DEFAULT NULL,
  `photo_possition` enum('L','R','U','D') DEFAULT NULL,
  `instructor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=334 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_content_details` */

insert  into `tbl_content_details`(`id`,`title`,`message`,`photo_location`,`content_type`,`youtube_link`,`menu_id`,`order_no`,`is_active`,`employee_id`,`photo_possition`,`instructor_id`) values (330,'Inspiring Ways to Wear Dresses in the Winter','<span xss=removed>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model setence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span><br>','P_1628757986.png','P','',53,1,1,NULL,'D',NULL),(331,'ENTREPRENEURSHIP PROGRAM','<p><span xss=removed>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model setence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span><br></p>','','P','',53,2,1,NULL,'R',NULL),(332,'ENTREPRENEURSHIP PROGRAM','<p><span xss=removed>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model setence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span><br></p>','P_1628766249.pdf','P','',53,3,1,NULL,'U',NULL),(333,'ENTREPRENEURSHIP PROGRAM','<p><span xss=removed>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model setence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words et</span><br></p>','','Y','<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/bvliTokR5u8\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',44,1,1,NULL,'R',NULL);

/*Table structure for table `tbl_documents` */

DROP TABLE IF EXISTS `tbl_documents`;

CREATE TABLE `tbl_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `slogan` varchar(300) DEFAULT NULL,
  `type` enum('S','D') DEFAULT NULL,
  `order_no` int(11) NOT NULL DEFAULT '0',
  `photo_location` varchar(300) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `description` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_documents` */

insert  into `tbl_documents`(`id`,`title`,`slogan`,`type`,`order_no`,`photo_location`,`is_active`,`description`) values (8,'Making Bangladesh Ready for Industry 4.0','Knowledge | Expertise | Excellence','S',1,'Slider_1624708016.jpg',1,'A Journey To Develop Expert Human Capital, To Bridge, The Academia And Industry And, To Make A Knowledge-Based Bangladesh. ');

/*Table structure for table `tbl_faq` */

DROP TABLE IF EXISTS `tbl_faq`;

CREATE TABLE `tbl_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category_id` int(11) NOT NULL,
  `order_no` int(11) DEFAULT '0',
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_faq` */

insert  into `tbl_faq`(`id`,`faq_category_id`,`order_no`,`message`) values (1,1,2,'<p>cvv</p>');

/*Table structure for table `tbl_faq_category` */

DROP TABLE IF EXISTS `tbl_faq_category`;

CREATE TABLE `tbl_faq_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `order_no` int(11) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_faq_category` */

insert  into `tbl_faq_category`(`id`,`name`,`order_no`,`is_active`) values (1,'School',2,1);

/*Table structure for table `tbl_gallery` */

DROP TABLE IF EXISTS `tbl_gallery`;

CREATE TABLE `tbl_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text,
  `file_path` varchar(200) DEFAULT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_gallery` */

insert  into `tbl_gallery`(`id`,`title`,`description`,`file_path`,`reference_id`,`date`) values (28,'Collaboration Meeting with Daffodil International University.','','File_8_0_1627914953.JPG',8,'2021-08-02'),(29,'Collaboration Meeting with Daffodil International University.','','File_8_1_1627914953.JPG',8,'2021-08-02'),(30,'Collaboration Meeting with Daffodil International University.','','File_8_2_1627914953.JPG',8,'2021-08-02'),(31,'Seminar on Machine Learning at Bangladesh University','','File_1_0_1627915444.jpg',1,'2019-03-02'),(32,'Seminar on Machine Learning at Bangladesh University','','File_1_1_1627915444.jpg',1,'2019-03-02'),(33,'Webinar on Robotics at Asian University of Bangladesh','','File_6_0_1627915701.jpg',6,'2021-06-25'),(34,'Webinar on Robotics at Asian University of Bangladesh','','File_6_1_1627915701.jpg',6,'2021-06-25'),(35,'Webinar on Robotics at Asian University of Bangladesh','','File_6_2_1627915701.JPG',6,'2021-06-25'),(36,'Webinar on Data Science & Machine Learning at NOAMI','','File_5_0_1627915959.jpg',5,'2021-07-02'),(37,'Webinar on Data Science & Machine Learning at NOAMI','','File_5_1_1627915959.jpg',5,'2021-07-02'),(38,'Webinar on Data Science & Machine Learning at NOAMI','','File_5_2_1627915959.JPG',5,'2021-07-02'),(39,'Collaboration Meeting with Asian University of Bangladesh','','File_4_0_1627916551.jpg',4,'2021-05-07'),(40,'Collaboration Meeting with Asian University of Bangladesh','','File_4_1_1627916551.jpg',4,'2021-05-07'),(41,'Collaboration Meeting with Asian University of Bangladesh','','File_4_2_1627916551.jpg',4,'2021-05-07'),(42,'Collaboration Meeting with National Oceanography And Maritime Institute (NOAMI)]','','File_3_0_1627916888.jpg',3,'2021-04-02'),(43,'Collaboration Meeting with National Oceanography And Maritime Institute (NOAMI)]','','File_3_1_1627916888.jpg',3,'2021-04-02'),(44,'Collaboration Meeting with National Oceanography And Maritime Institute (NOAMI)]','','File_3_2_1627916888.jpg',3,'2021-04-02'),(45,'Webinar on Data Science and Machine Learning at European University of Bangladesh','','File_2_0_1627917103.png',2,'2021-06-04'),(46,'Webinar on Data Science and Machine Learning at European University of Bangladesh','','File_2_1_1627917103.png',2,'2021-06-04'),(47,'Webinar on Data Science and Machine Learning at European University of Bangladesh','','File_2_2_1627917103.png',2,'2021-06-04'),(48,'Certificate Course on Data Science & Machine Learning with Python','','File_7_0_1627917312.JPG',7,'2021-07-27'),(49,'Certificate Course on Data Science & Machine Learning with Python','','File_7_1_1627917312.JPG',7,'2021-07-27'),(50,'Certificate Course on Data Science & Machine Learning with Python','','File_7_2_1627917312.JPG',7,'2021-07-27'),(51,'Certificate Course on Data Science & Machine Learning with Python','','File_7_3_1627917312.JPG',7,'2021-07-27'),(52,'Certificate Course on Data Science & Machine Learning with Python','','File_7_4_1627917312.JPG',7,'2021-07-27'),(53,'Certificate Course on Data Science & Machine Learning with Python','','File_7_5_1627917312.JPG',7,'2021-07-27');

/*Table structure for table `tbl_gender` */

DROP TABLE IF EXISTS `tbl_gender`;

CREATE TABLE `tbl_gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_gender` */

insert  into `tbl_gender`(`id`,`name`) values (28,'Male'),(29,'Female');

/*Table structure for table `tbl_menu` */

DROP TABLE IF EXISTS `tbl_menu`;

CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `order` int(11) DEFAULT '0',
  `link_able` int(1) DEFAULT '0',
  `is_active` int(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `child_of` varchar(100) DEFAULT NULL,
  `is_url` int(1) DEFAULT '0',
  `url_link` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_menu` */

insert  into `tbl_menu`(`id`,`name`,`order`,`link_able`,`is_active`,`parent_id`,`child_of`,`is_url`,`url_link`) values (26,'Contact Us',7,0,1,0,'-0-',1,'http://localhost/mrahman/homes/contactus'),(31,'News',2,1,1,0,'-0-',1,'http://localhost/mrahman/homes/news'),(36,'Research',3,1,1,0,'-0-',1,'http://localhost/mrahman/homes/research'),(40,'Publication',4,1,1,0,'-0-',1,'http://localhost/mrahman/homes/publication'),(44,'Teaching',5,1,1,0,'-0-',1,'http://localhost/mrahman/homes/teachings'),(52,'About Me',1,1,1,0,'-0-',1,'http://localhost/mrahman/homes/about_me'),(53,'Outreach',6,1,1,0,'-0-',0,'');

/*Table structure for table `tbl_news_and_blog` */

DROP TABLE IF EXISTS `tbl_news_and_blog`;

CREATE TABLE `tbl_news_and_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(300) NOT NULL,
  `date` date DEFAULT NULL,
  `type` enum('B','E','N') NOT NULL,
  `photo_location` varchar(300) DEFAULT NULL,
  `youtube_link` text,
  `description` text,
  `is_active` int(11) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `google_map` text,
  `venue` varchar(300) DEFAULT NULL,
  `time` varchar(300) DEFAULT NULL,
  `short_description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_news_and_blog` */

insert  into `tbl_news_and_blog`(`id`,`category_id`,`title`,`date`,`type`,`photo_location`,`youtube_link`,`description`,`is_active`,`created_by`,`google_map`,`venue`,`time`,`short_description`) values (5,2,'Cornell University (2016, September 22)','2021-07-30','N','N_1628874831.png',NULL,'<div class=\"ecm0bbzt hv4rvrfc ihqw7lf3 dati1w0a\" data-ad-comet-preview=\"message\" data-ad-preview=\"message\" id=\"jsc_c_az\"><div class=\"j83agx80 cbu4d94t ew0dbk1b irj2b8pg\"><div class=\"qzhwtbm6 knvmm38d\"><span class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v knj5qynh oo9gr5id hzawbc8m\" dir=\"auto\" lang=\"en\"><div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\"><div dir=\"auto\" xss=\"removed\">Invited talk in the department of Fiber Science, Ithaca, NY<br></div></div></span></div></div></div>',1,27,'','','','Invited talk in the department of Fiber Science, Ithaca, NY'),(6,3,'Materials today (2017, January)','2020-07-17','N','N_1628874990.jpg',NULL,'<p class=\"MsoNormal\" xss=removed><a href=\"http://www.materialstoday.com/composites/news/ordering-fibers-in-a-nanocomposite-is-a-stretch\"><span xss=removed>http://www.materialstoday.com/composites/news/ordering-fibers-in-a-nanocomposite-is-a-stretch</span></a><span xss=removed><o></o></span></p>',1,38,'','','','Ordering fibers in green nano-composites is a stretch'),(8,1,'Ordering fibers in green nanocomposites is a stretch','2005-12-20','N','N_1628874737.jpg',NULL,'<h1 id=\"screen-reader-main-title\" class=\"Head u-font-serif u-h2 u-margin-s-ver\" xss=\"removed\"><span xss=removed><a href=\"http://www.sciencedirect.com/science/article/pii/S1748013216305114\"><span xss=removed>http://www.sciencedirect.com/science/article/pii/S1748013216305114</span></a></span><br></h1>',1,38,'','','','Ordering fibers in green nanocomposites is a stretch');

/*Table structure for table `tbl_news_and_blog_category` */

DROP TABLE IF EXISTS `tbl_news_and_blog_category`;

CREATE TABLE `tbl_news_and_blog_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `order_no` int(11) DEFAULT NULL,
  `is_active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_news_and_blog_category` */

insert  into `tbl_news_and_blog_category`(`id`,`name`,`order_no`,`is_active`) values (1,'OpenTalk',1,1),(2,'Webinar/Seminar',2,1),(3,'Workshop/Bootcamp',3,1),(4,'Course',4,1);

/*Table structure for table `tbl_publication_category` */

DROP TABLE IF EXISTS `tbl_publication_category`;

CREATE TABLE `tbl_publication_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `order_no` int(11) DEFAULT '0',
  `is_active` int(1) DEFAULT '1',
  `file_location_1` varchar(300) DEFAULT NULL,
  `file_location_2` varchar(300) DEFAULT NULL,
  `file_location_3` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_publication_category` */

insert  into `tbl_publication_category`(`id`,`name`,`order_no`,`is_active`,`file_location_1`,`file_location_2`,`file_location_3`) values (7,'Patents',1,1,'publications_img1__1628878322.jpg','',''),(8,'Journal Articles',2,1,'','',''),(9,'Book Chapters',3,1,'','',''),(10,'Manuscript under Review or Revision',4,1,'','','');

/*Table structure for table `tbl_publications` */

DROP TABLE IF EXISTS `tbl_publications`;

CREATE TABLE `tbl_publications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_category_id` int(11) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `title` text,
  `order_no` int(11) DEFAULT '0',
  `is_active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_publications` */

insert  into `tbl_publications`(`id`,`publication_category_id`,`year`,`title`,`order_no`,`is_active`) values (1,7,2020,'<p>Rahman, M. M.; Sajadi, S. M.; Kumar, A.; Boul, P. J.; Thaemlitz, C.; Ajayan, P. M. Cementbased direct ink for 3D-printing of complex architected structures. US Patent Application\r\nNo.: 16/596, 396, 2020.<br></p>',1,1),(2,8,2021,'<p>Zhu, Y.; Zhu, D.; Gao, G.; Yan, Q.; Xu, J.; Liu, Y.; Alahakoon, S.; Rahman, M. M.; Ajayan,\r\nP. M.; Egap, E.; Verduzco, R. Metal oxide catalysts for the synthesis of COFs and one-step\r\npreparation of COF composites. Accepted in Chemistry of Materials.\r\n<br></p>',1,1),(3,8,2021,'<p>Zhu, D.; Hu, Z.; Rogers, T.; Barnes, M.; Tseng, C.; Mei, H.; Sassi, L.; Zhang, Z.; Rahman,\r\nM. M.; Ajayan, P. M.; Verduzco, R. Patterning, Transfer, and Tensile Testing of Covalent\r\nOrganic Frameworks Films with Nanoscale Thickness. Accepted in Chemistry of Materials.<br></p>',2,1),(4,8,2021,'<p>Zhu, D.; Zhu, Y.; Yan, Q.; Liu, F.; Yu, P.; Tseng, C.; Tjahjono, N.; Huang, P.; Rahman, M.\r\nM.; Egap, E.; Ajayan, P. M.; Verduzco, R. Pure Crystalline Covalent Organic Framework\r\nAerogels. Accepted in Chemistry of Materials 2021, 33 (11), 4216-4224.<br></p>',3,1),(5,8,2021,'<p>Zhu, D.; Xu, G.; Barnes, M.; Zhang, Z.; Zhang, J.; Li, Y.; Khalil, S.; Rahman, M. M.*;\r\nVerduzco, R.; Ajayan, P. M. Covalent Organic Frameworks for Batteries. Accepted in\r\nAdvanced Functional Materials 2021.<br></p>',4,1),(6,8,2021,'<p>Khan, M.A.; Al-Attas, Tareq; Roy, S.; Rahman, M. M.; Ghaffour, N.; Thangadurai, V.;\r\nLarter, S.; Hu, J.; Ajayan, P.M.; Kibria, M.G. Seawater Splitting for Hydrogen Production:\r\nA Solution Looking for a Problem? Accepted in Energy & Environmental Science 2021.<br></p>',5,1),(7,8,2021,'<p>Zhu, D.; Zhang, Z.; Li, Y.; Barnes, M.; Khalil, S.; Rahman, M. M.; Ajayan, P. M.; Verduzco,\r\nR. Rapid, ambient temperature synthesis of imine covalent organic frameworks catalyzed by\r\ntransition metal nitrates. Chemistry of Materials 2021, 33 (9), 3394-3400.<br></p>',6,1),(8,8,2021,'<p>Yuan, F.; Salpekar, D.; Baburaj, A.; Hasan, S.; Putirath, A. B.; Saadi, M.A.S.R.; Robles,\r\nF.C.; Robetzazi, H.; Roy, S.; Sun, D.; Kotov, N.A.; Rahman, M. M.*; Ajayan, P. M. Fiber\r\nreinforced Monolithic Supercapacitor with Interdigitated Interfaces. Journal of Materials\r\nChemistry A. 2021, 9, 11033-11041.<br></p>',7,1),(9,9,2015,'<p>Rahman, M. M.; Netravali, A. N. ‘Green’ Resins from Plant Sources and Strengthening\r\nMechanisms. Book Chapter in Advanced Green Composites published by Scrivener\r\nPublishing<br></p>',1,1),(10,10,2020,'<p>Susarla, S.; Chilkoor, G.; Cui, Y.; Arif, T.; Tsafack, T.; Puthirath, A.B.; Sudeep, P. M.;\r\nKalimuthu, J. R.; Hassan, A.; Castro-Pardo, S.; Barnes, M.; Verduzco, R.; Filleter, T.;\r\nKoratkar, N.; Gadhamshetty, V.; Rahman, M. M.*; Ajayan, P. M. Corrosion Resistance of\r\nSulfur-Selenium Alloy Coatings. Under Review in Advanced Materials.<br></p>',1,1),(11,10,2020,'<p>Khan, M.A.; Nabil, S.K.; Al-Attas, T.A.; Roy, S.; Rahman, M.M.; Ajayan, P.M.; Hu, J.;\r\nKibria, M.G. Electrochemical reduction of CO2 to ethylene with 80% reduction in cost via\r\ncoproduction of glycolic acid. Under Review in Nature Communications.</p>',2,1),(12,10,2020,'<p>Adnan, M. A.; Khan, M. A.; Rahman, M. M.; Ajayan, P. M.; Hu, J.; Kibria, M. G.; Prospects\r\nof sunlight driven Air-to-Methanol synthesis via CO2 electrolysis. Under Review in Cell\r\nReports Physical Science.<br></p>',3,1);

/*Table structure for table `tbl_research` */

DROP TABLE IF EXISTS `tbl_research`;

CREATE TABLE `tbl_research` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `order` int(11) DEFAULT NULL,
  `is_active` int(1) DEFAULT '1',
  `file_location` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_research` */

insert  into `tbl_research`(`id`,`title`,`description`,`order`,`is_active`,`file_location`) values (2,'Research Goal 1: Bio-inspired Design via Additive Manufacturing','<p xss=\"removed\">Learning from complex hierarchical architected structures found in nature, multiscale geometric design plays a crucial role in terms of mechanical and functional response while using a limited number of building blocks. The key to the distinguished performance of natural and biological structures is the topology and spatial arrangement of the constituent blocks. Additive manufacturing technologies are opening new possibilities for the potential implementation of this design paradigm at the macroscale. A fundamental challenge that will be addressed in my research is the design and development of lightweight mechanical metamaterial structures with hierarchical geometry and architectures that are simultaneously strong and energy-efficient using additive manufacturing. My wide research experience on 3D printing has equipped me with expansive knowledge and varied skillset that certifies me to be a significant and efficient contributor in this upsurging scientific territory. Following a holistic and interdisciplinary approach, I have completed numerous research projects successfully developing and analyzing 3D printed architected structures using polymers, composites, ceramics, cement, and metals, particularly emphasizing their mechanical properties. My future works propose the design and fabrication of 3D printable structures in different architectures having optimized topologies, and densities in the form of metamaterials, which can have unprecedented mechanical, thermal, optical, and/or electrical properties.<br></p>',2,1,'Research_Statment_1628874150.jpg'),(3,'Research Goal 2: Multifunctional Fibers and Composites','<p xss=\"removed\">In my quest of comprehending and optimizing the multifunctional properties of composite materials, I investigated the dispersion of carbon-based nanoparticles such as carbon nanotubes (CNTs) and carbon nanofibers (CNFs) inside various matrix phases. My academic passion and vigor for composite materials brought me to astounding findings. I successfully demonstrated that even at a low loading level, uniformly dispersed CNTs in the composites can effectively combine high strength with high ductility, which resulted in higher absorption energy of the projectile without considerable deformation or damage during low and high-velocity impact studies. Additionally, I have analyzed the thermo-mechanical and thermal degradation behavior of these composite materials. Along the same line, various low dimensional nanomaterials have attracted my attention as reinforcement materials for multi-functional nanocomposites owing to its exceptionally low density, high strength, stiffness, hardness, toughness, and outstanding functional properties including electrical, optical, and thermal properties. My future goals include innovative designing and engineering of next-generation structural nanocomposites by implanting smart fibers, low dimensional, and nanomaterials inside the matrix systematically and orderly with self- healing and damage-sensing mechanisms while evaluating their use in real-life application.<br></p>',3,1,'Research_Statment_1628874575.jpg'),(6,'Research Goal 4: Digital Food Safety and Security','<p>World hunger and chronic undernourishment is an exigent global issue, impacting over 800 million people. A third of the food produced is wasted every year largely due to the high perishability of food. Hence, multi-faceted solutions focusing on food security, safety, and waste management/reduction will be one of my research emphases in the future. While there have been successes with the development of genetically modified products, chemical additives, and inedible coatings, these solutions introduce other detrimental effects and concerns related to human health and the environment. I am interested in the development of cost-effective and efficient, yet greener materials by the proper utilization of food wastes while ensuring food safety and security. Very recently, I achieved a breakthrough by developing a coating for perishable foods, from egg wastes, to enhance the shelf life of fruits. I am conscious, however, that the mitigation of food-related issues is not limited to traditional solutions, such as edible coatings. In fact, I would like to apply additive manufacturing techniques to realize unconventional food materials and structures, as well as the future exciting possibilities of personalized data-driven nutrition and food.</p>',4,1,'Research_Statment_1628874614.png'),(7,'Research Goal 3: Waste Utilization into Value-added Composites ','<p>Slanting towards environmental and economic concerns, I aspire to develop green composites/materials from waste sources intended for multi-functional applications ranging from economic packaging, housing or transportation, and sports to food, textile, and biomedical applications. Aiming to replace synthetic polymers for ensuring a greener environment, my research will encompass the optimization of multifunctional properties of inexpensive, biodegradable polymers such as protein, cellulose, and other carbohydrates etc. A significant portion of my research efforts will be invested in the synthesis of inorganic and polymeric Page 3 of 3 nanomaterials from various agro- and animal wastes, to reinforce and strengthen these biopolymers. Critical control of the size, shape, and order of these synthesized nanomaterials has prodded my quest and I am interested to explore in this avenue. Additionally, my goal is to investigate the chemical and microbial synthesis of biopolymers from methane-based wastes. Extraction and utilization of micro-plastics have piqued my interest as well and my motivation to explore in this arena is aligned with the growing challenges that our environment and climate have been facing for the past few decades.</p>',5,1,'Research_Statment_1628874404.png'),(8,'Research Goal 5: Living Composite Materials','<p>Drawing inspiration from the natural world has led to the development of engineered living materials, where living organisms are incorporated into material systems to generate structurally complex materials in situ, which would be otherwise extremely difficult to achieve by physical and chemical means. My research will aim at developing such materials, investigating their stimuli responsivity, longevity, biocompatibility, and self-healing properties, and finally, evaluating their use in a myriad of applications. I would like to develop living composites that involve the utilization of microorganisms, e.g. cellulose producing bacteria, as the self-strengthening and self- healing agent in enclosed three-dimensional bio-polymeric systems. Embedding microorganisms directly into the polymer matrix will ensure the three-dimensional in situ production of living nanocomposite with enhanced structural performances while reducing potential environmental impact. In contrast to the traditional methods of nanocomposite development, I want to advance this field, hatched at the interface of biology and materials science, by growing complex structures with enhanced structural and functional performances leveraging the power of living systems. My focus will be attaining sophisticated synthetic control, energy efficiency, and tunability in this process that is unachievable by any of the conventional approaches that are prevalent. The span of possibilities that can be achieved with my research expertise and approach is endless. The overarching goal of the proposed research approaches will be realized through quantitative measurements and optimization of the interplay of hierarchical components of the hybrid systems that control macroscopic properties and functionality. In a broader sense, through my research efforts, I seek toward achieving genuine sustainability in the design of materials, for the simultaneous benefit of the environment, economy, and society.<br></p>',6,1,'Research_Statment_1628874649.jpg'),(9,'Research Statement','<p>My primary academic interest lies in bridging the research gap pertaining to the interdisciplinary field wherein materials science and mechanical engineering are brought together with chemical and biological sciences to study and formulate structural and functional structures. The goal of my research is to develop advanced multifunctional structures with an emphasis on sustainable design, fabrication, and materials for a variety of applications, including load-bearing structures, food, textiles, energy, and environment. The quest for the simultaneous full-scale attainment of mutually exclusive material properties such as strength and toughness in structural materials has traditionally been a negotiation or compromise. Nature, on the other hand, overcomes such limitations by developing biological composites through multiple length-scale internalized designs where the optimized composition of the hard phase (provides high strength) is packaged with soft organic phases (provides high toughness) in a complex architecture. Despite several strenuous efforts for the concurrent realization of mutually exclusive material properties, such synthetic composite optimization has not been yet possible due to the lack of intelligent material design and assembly. Upon successful optimization, synthetic composite materials will be the preferred material class for a long list of structural and functional applications such as military, aerospace, automotive, marine, infrastructure, and industrial, where strength, toughness, resistance to corrosion and fatigue, and long-term endurance are critical requirements. Hence, part of my research objective will be aimed at maximizing the combination of fundamental material properties, thereby, achieving high- performance multifunctional structures through hierarchical geometric design, using advanced intelligent fabrication techniques such as- additive manufacturing. On a different note, increased concerns associated with environmental deterioration, climate change, waste disposal difficulties, and cost, have fueled my interest in the development of eco- friendly and biodegradable materials. These materials offer copious advantages and open up new possibilities, such as- innovations in the development of structural and functional bio- based structures, reductions in the use of limited fossil fuel-based materials, complete biological degradability coupled with decreased landfill use, and application possibilities of agricultural resources for the production of green materials. Consequently, part of my research efforts will be evaluated in the context of current growing interest in the development of green materials and the utilization of environmental wastes for multi- functional applications. Along the same line, development of futuristic biomaterials that will have advanced tunability and adaptability, over conventional materials, to different environmental conditions, self-repair and healing mechanism, and response to external stimuli, will be part of my research endeavors. I am highly interested to venture into this fertile area of exploration in order to develop responsive living composites for structural applications by integrating materials science and biology with bio-based scaffolds and living cells. Page 2 of 3 Edging towards sustainable solutions to global problems from the materials perspective, my research efforts will predominantly revolve around five key areas:<br></p>',1,1,'Research_Statment_1628875567.png');

/*Table structure for table `tbl_skills` */

DROP TABLE IF EXISTS `tbl_skills`;

CREATE TABLE `tbl_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `experience_percentage` int(11) DEFAULT NULL,
  `order_no` int(11) DEFAULT NULL,
  `is_active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tbl_skills` */

/*Table structure for table `tbl_teachings` */

DROP TABLE IF EXISTS `tbl_teachings`;

CREATE TABLE `tbl_teachings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `file_location` varchar(300) DEFAULT NULL,
  `description` text,
  `type` enum('OC','NC') NOT NULL,
  `order_no` int(11) DEFAULT '0',
  `is_active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_teachings` */

insert  into `tbl_teachings`(`id`,`title`,`file_location`,`description`,`type`,`order_no`,`is_active`) values (1,'DONATE AND PARTICIPATE WITH US','Teachings_1628783637.jpg','<p><span xss=removed>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas. Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas. Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas. Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas. Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</span><br></p>','NC',0,1),(2,'Inspiring Ways to Wear Dresses in the Winter','Teachings_1628782873.pdf','<p><span xss=removed>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas. Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas. Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint </span><br></p>','OC',0,1),(3,'Inspiring Ways to Wea','Teachings_1628782902.doc','<p><span xss=removed>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas. Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum</span><br></p>','OC',0,1);

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `user_name` varchar(250) NOT NULL,
  `user_password` varchar(250) NOT NULL,
  `user_role` int(11) NOT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `photo_location` varchar(400) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `is_locked` int(1) DEFAULT '0',
  `employee_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`id`,`name`,`user_name`,`user_password`,`user_role`,`mobile`,`photo_location`,`is_active`,`is_locked`,`employee_id`) values (27,'Admin','naiem360','487c078f38e8e6942a657e3a5682e878',1,'11111111111',NULL,1,0,34),(37,NULL,'afrin','c543746243732797ace907b64586f44c',1,NULL,NULL,1,0,35),(38,NULL,'admin','e10adc3949ba59abbe56e057f20f883e',1,NULL,NULL,1,0,33),(39,NULL,'admin23','86757050220b5edf09e49307b9603549',8,NULL,NULL,1,0,NULL);

/*Table structure for table `tbl_user_pass_history` */

DROP TABLE IF EXISTS `tbl_user_pass_history`;

CREATE TABLE `tbl_user_pass_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `old_pass` varchar(300) DEFAULT NULL,
  `new_pass` varchar(300) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_pass_history` */

insert  into `tbl_user_pass_history`(`id`,`user_id`,`old_pass`,`new_pass`,`date`) values (3,27,'e10adc3949ba59abbe56e057f20f883e','fcea920f7412b5da7be0cf42b8c93759','2021-03-09 01:44:08'),(4,27,'fcea920f7412b5da7be0cf42b8c93759','e10adc3949ba59abbe56e057f20f883e','2021-03-09 01:44:54'),(5,27,'e10adc3949ba59abbe56e057f20f883e','0752bd1a356de93804923f167ad15c7f','2021-06-26 17:58:30'),(6,27,'e10adc3949ba59abbe56e057f20f883e','0752bd1a356de93804923f167ad15c7f','2021-06-26 18:04:27');

/*Table structure for table `tbl_website_config` */

DROP TABLE IF EXISTS `tbl_website_config`;

CREATE TABLE `tbl_website_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_language` enum('B','E') DEFAULT 'E',
  `website_main_color` varchar(100) DEFAULT '#772b91',
  `facebook_page_embed` text,
  `google_map_embed` text,
  `show_female_student_mobile_address` int(1) DEFAULT '0',
  `show_female_student_photo` int(1) DEFAULT '0',
  `show_teacher_info` int(1) DEFAULT '0',
  `take_collect_payment` int(1) DEFAULT '0',
  `show_student_info` int(1) DEFAULT '0',
  `current_admission_year` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_website_config` */

insert  into `tbl_website_config`(`id`,`default_language`,`website_main_color`,`facebook_page_embed`,`google_map_embed`,`show_female_student_mobile_address`,`show_female_student_photo`,`show_teacher_info`,`take_collect_payment`,`show_student_info`,`current_admission_year`) values (1,'E','#1296cc','<iframe src=\"https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/sch360&tabs=timeline&width=300&height=250&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=144558516147261\" width=\"300\" height=\"250\" style=\"border:none;overflow:hidden\" scrolling=\"no\" frameborder=\"0\" allowTransparency=\"true\" allow=\"encrypted-media\"></iframe>','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.3383887374453!2d90.35031761463523!3d23.80656318456172!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c0e112c2434f%3A0x898436d8bc20d96f!2z4Kai4Ka-4KaV4Ka-IOCmleCmruCmvuCmsOCnjeCmuCDgppXgprLgp4fgppw!5e0!3m2!1sen!2sbd!4v1604725780270!5m2!1sen!2sbd\" width=\"580\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>',0,0,0,1,0,2021);

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User Role identification number',
  `role_name` varchar(200) NOT NULL,
  `role_description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_user_roles_role_Name` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `user_roles` */

insert  into `user_roles`(`id`,`role_name`,`role_description`) values (1,'Super Admin','Super Admin'),(8,'Admin','Admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
