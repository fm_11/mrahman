<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class About_me extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $if_exist = $this->db->get('tbl_about_me')->result_array();

      if(COUNT($if_exist) == 0){
        $data = array();
        $data['id'] ='1';
        $this->db->insert('tbl_about_me',$data);
        redirect("about_me/index");
      }
      $about_me_info=  $this->Admin_login->get_about_me();
        if ($_POST) {
             $id=$about_me_info->id;
              $old_photo=  $this->input->post('old_photo_location', true);
              $about_me_img=  $old_photo;
               if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
                 $about_me_img = $this->my_file_upload('txt_photo_location', 'about_me');
                 if ($about_me_img=='0') {
                     $sdata['exception'] = "Photo doesn't upload." . $this->upload->display_errors();
                     $this->session->set_userdata($sdata);
                     redirect("about_me/index");
                 }
                 $this->my_file_remove($old_photo,  $about_me_img);
              }

            $allowedTags = '<style><iframe><p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
            $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a><b>';
            $about_1 = strip_tags(stripslashes($_POST['about_1']), $allowedTags);

            $allowedTags = '<style><iframe><p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
            $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a><b>';
            $about_2 = strip_tags(stripslashes($_POST['about_2']), $allowedTags);
           $data = array();
           $data['id'] =$id;
           $data['title'] = $this->input->post('title', true);
           $data['about_1'] = $about_1;
           $data['about_2'] = $about_2;
           $data['file_location'] = $about_me_img;

           if ($this->Admin_login->edit_about_me($data,$id)) {
               $sdata['message'] = "Data successfully updated";
               $this->session->set_userdata($sdata);
           } else {
               $sdata['exception'] = "Data cannot be Updated";
               $this->session->set_userdata($sdata);
           }
           redirect("about_me/index");
        }
      $data = array();
      $data['title'] = 'Update About Me';
      $data['heading_msg'] = 'Update About Me';
      $data['row'] =$about_me_info;
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('about_me/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/dist/img/orga/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                'max_size' => "99999999999",
                'max_height' => "3000",
                'max_width' => "3000",
                'file_name' => $new_file_name
            );
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/dist/img/orga/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
}
