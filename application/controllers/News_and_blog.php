<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class News_and_blog extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('News_and_event','Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'News Informations';
      $data['heading_msg'] = 'News Informations';
      $data['is_show_button'] = "add";
      $data['content'] = $this->News_and_event->get_news_and_blog_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('news_and_blog/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

         $user_info = $this->session->userdata('user_info');

          $content_file="";
         //  $content_type=$this->input->post('type', true);
         //  if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
         //   $type=$this->get_full_content_type($content_type);
         //    $content_file = $this->my_file_upload('txt_photo_location', $type);
         //    if ($content_file=='0') {
         //        $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors();
         //        $this->session->set_userdata($sdata);
         //        redirect("news_and_blog/add");
         //    }
         // }

          $data = array();
          $data['title'] = $this->input->post('title', true);
          $data['date'] = $this->input->post('date', true);
          $data['type'] = $this->input->post('type', true);
          $data['youtube_link'] = $this->input->post('youtube_link', true);
          $data['category_id'] = $this->input->post('category_id', true);
          $data['description'] = $this->input->post('description', true);
          $data['google_map'] = $this->input->post('google_map', true);
          $data['venue'] = $this->input->post('venue', true);
          $data['time'] = $this->input->post('time', true);
          $data['short_description'] = $this->input->post('short_description', true);
          $data['is_link_url'] = $this->input->post('is_link_url', true);
          $data['url'] = $this->input->post('url', true);
          $data['embedded_youtube'] = $this->input->post('embedded_youtube', true);
          $data['year'] = $this->input->post('year', true);
          $data['photo_location'] = $content_file;
          $data['created_by']=$user_info[0]->id;
          if ($this->News_and_event->add_news_and_blog($data)) {
              $sdata['message'] = "Data successfully added";
              $this->session->set_userdata($sdata);
              redirect("news_and_blog/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("news_and_blog/add");
          }
      }
        $data = array();
        $data['title'] = 'Add News';
        $data['heading_msg'] =  'Add News';
        $data['is_show_button'] = "index";
        $data['category'] = $this->News_and_event->get_news_and_blog_category_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('news_and_blog/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {
          $id=  $this->input->post('id', true);

         // $old_file=  $this->input->post('old_photo_location', true);
         $content_file="";
         // $content_type=$this->input->post('type', true);
         // $content_file=  $old_file;
         //  if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
         //    $content_file = $this->my_file_upload('txt_photo_location', $content_type);
         //    if ($content_file=='0') {
         //        $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors();
         //        $this->session->set_userdata($sdata);
         //        redirect("news_and_blog/edit/".$id);
         //    }
         //    $this->my_file_remove($old_file,  $content_file);
         // }
          $data = array();
          $data['id'] =$id;
          $data['title'] = $this->input->post('title', true);
          $data['date'] = $this->input->post('date', true);
          $data['type'] = $this->input->post('type', true);
          $data['youtube_link'] = $this->input->post('youtube_link', true);
          $data['category_id'] = $this->input->post('category_id', true);
          $data['description'] = $this->input->post('description', true);
          $data['google_map'] = $this->input->post('google_map', true);
          $data['venue'] = $this->input->post('venue', true);
          $data['time'] = $this->input->post('time', true);
          $data['short_description'] = $this->input->post('short_description', true);
          $data['is_link_url'] = $this->input->post('is_link_url', true);
          $data['url'] = $this->input->post('url', true);
          $data['embedded_youtube'] = $this->input->post('embedded_youtube', true);
          $data['year'] = $this->input->post('year', true);
          $data['photo_location'] = $content_file;
          if ($this->News_and_event->edit_news_and_blog($data,$id)) {
              $sdata['message'] = "Data successfully updated";
              $this->session->set_userdata($sdata);
              redirect("news_and_blog/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("news_and_blog/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update News';
        $data['heading_msg'] =  'Update News';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->News_and_event->read_news_and_blog($id);
        $data['category'] = $this->News_and_event->get_news_and_blog_category_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('news_and_blog/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
       $data= $this->News_and_event->read_news_and_blog($id);
       if(empty($data))
       {
         $sdata['exception'] = "Invalid Id!Please try again";
         $this->session->set_userdata($sdata);
         redirect("news_and_blog/index");
       }
       $photo= $data->photo_location;
        if($this->News_and_event->delete_news_and_blog($id))
        {
          $this->my_file_remove($photo, $photo);
          $sdata['message'] = "delete";
        }else{
          $sdata['exception'] = "Data cannot be Deleted";
        }
        $this->session->set_userdata($sdata);
        redirect("news_and_blog/index");
    }
    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/dist/img/newsblog/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx|gif",
                'max_size' => "99999999999",
                'max_height' => "90000",
                'max_width' => "90000",
                'file_name' => $new_file_name
            );
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/dist/img/newsblog/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
    private function get_full_content_type($type)
    {
      $full_type="";
      if($type=='E')
      {
        $full_type="Events";
      }elseif ($type=='B') {
        $full_type="Blog";
      }
      return $full_type;
    }
    function updateMsgContentStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->News_and_event->edit_news_and_blog($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-xs">Inactive</button></a>';
        }
    }

}
