<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home_message extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $messaege=$this->Admin_login->get_chairman_message();
      // print_r($messaege);
      // die;
      if(empty($messaege))
      {
          redirect("home_message/add");
      }
      if ($_POST) {
        $id= $messaege->id;
         $old_photo=  $this->input->post('old_photo_location', true);
         $employee_img=  $old_photo;
          if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
            $employee_img = $this->my_file_upload('txt_photo_location', 'chairmen');
            if ($employee_img=='0') {
                $sdata['exception'] = "Photo doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("home_message/index");
            }
            $this->my_file_remove($old_photo,  $employee_img);
         }
          $data = array();
          $data['id'] =$id;
          $data['title'] = $this->input->post('title', true);
          $data['short_message'] = $this->input->post('short_message', true);
          $data['long_message'] = $this->input->post('long_message', true);
          $data['name'] = $this->input->post('name', true);
          $data['designation'] = $this->input->post('designation', true);
          $data['photo_location'] = $employee_img;
          if ($this->Admin_login->edit_chairman_message($data,$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("home_message/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("home_message/index");
          }
      }

      $data = array();
      $data['title'] = 'Update Home Message';
      $data['heading_msg'] = 'Home Message';
    //  $data['is_show_button'] = "add";
      $data['row'] =$messaege;
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('home_message/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
          $employee_img="";
          if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
            $employee_img = $this->my_file_upload('txt_photo_location', 'chairmen');
            if ($employee_img=='0') {
                $sdata['exception'] = "Photo doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("home_message/add");
            }
         }

          $data = array();
          $data['title'] = $this->input->post('title', true);
          $data['short_message'] = $this->input->post('short_message', true);
          $data['long_message'] = $this->input->post('long_message', true);
          $data['name'] = $this->input->post('name', true);
          $data['designation'] = $this->input->post('designation', true);
          $data['photo_location'] = $employee_img;
          if ($this->Admin_login->add_chairman_message($data)) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("home_message/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("home_message/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Home Message';
        $data['heading_msg'] =  'Add Home Message';
        //$data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('home_message/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/dist/img/employee/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                'max_size' => "99999999999",
                'max_height' => "6000",
                'max_width' => "6000",
                'file_name' => $new_file_name
            );
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/dist/img/employee/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
}
