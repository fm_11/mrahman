<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Homes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Website','Admin_login','Content','Publication','Research'));
        $this->load->library('session','Language_number_convert');
    		date_default_timezone_set('Asia/Dhaka');
    		$website_lang = $this->session->userdata('site_lang');
    		if($website_lang == ''){
    			$website_config = $this->Admin_login->getWebsiteConfig();
    			$default_language = $website_config->default_language;
    			$language = 'english';
    			if ($default_language != 'E') {
    				$language = 'bangla';
    			}
    			$this->session->set_userdata('site_lang', $language);
    		}
    }

    public function index() {
          //return redirect("homes/about_me");
          $data = array();
        //  $data['title'] = 'M Rahman';
          $data['org_info'] =$this->Website->getInstituteInfo();
          $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();
          $data['about'] = $this->Admin_login->get_about_me();
          $data['title'] = $data['about']->title;
          $data['main_content'] = $this->load->view('homes/sources/about_me', $data, true);
        //  $data['main_content'] = $this->load->view('homes/sources/main_content', $data, true);
          $this->load->view('homes/index', $data);
    }
    function news(){
      $data = array();
      $data['org_info'] =$this->Website->getInstituteInfo();
      $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();
      // $data['latest_blog_info'] =$this->Website->get_latest_news_and_blog('N');
      // $data['news_category'] =$this->Website->get_news_and_blog_category_list('N');
      // $this->load->library('Text_preview');
      $cond = array();
      $cond['type']='N';
      $cond['is_active']='1';
      if(isset($_GET['cat_id']))
      {
        $cond['category_id']=$_GET['cat_id'];
      }
      // $this->load->library('pagination');
      // $config['base_url'] = site_url('homes/news/');
      // $per_page=9;
      // $config['per_page'] = $per_page;
      // $config['total_rows'] = count($this->Website->get_news_and_blog_list(0, 0, $cond));
      // $config['uri_segment']  = 3;
      // $this->pagination->initialize($config);
      // $data['news_list'] = $this->Website->get_news_and_blog_list($per_page, (int) $this->uri->segment(3), $cond);
      // $data['counter'] = (int) $this->uri->segment(3);
      //
      // echo "<pre>";
      //     print_r($cond);
      // print_r($data['news_list']);
      // die;
      $data['news_and_blog_list'] = $this->Website->get_all_news_and_blog_by_type('N');
      $data['image_list']=$this->Website->get_image_list_by_type('N');
      $data['title'] ='News';
      $data['show_footer']=1;
      $data['main_content'] = $this->load->view('homes/sources/news', $data, true);
      $this->load->view('homes/index', $data);
    }
    public function news_details($id) {
          $data = array();
          $data['org_info'] =$this->Website->getInstituteInfo();
          $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();
          $data['latest_news_info'] =$this->Website->get_latest_news_and_blog('N');
          $data['news_category'] =$this->Website->get_news_and_blog_category_list('N');
          $cond = array();
          $cond['type']='N';
          $cond['is_active']='1';
          $cond['id']=$id;
          $details=$this->Website->get_news_and_blog_list(0, 0, $cond);
          if(empty($details))
          {
            return redirect("homes/news");
          }
          // echo "<pre>";
          // print_r($details);
          // die;
          $data['news_details']=$details;
          $data['title'] =$details[0]['title'];
          $data['main_content'] = $this->load->view('homes/sources/news_details', $data, true);
          $this->load->view('homes/index', $data);
    }
    public function about_me() {
          $data = array();
          $data['org_info'] =$this->Website->getInstituteInfo();
          $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();
          $data['about'] = $this->Admin_login->get_about_me();
          $data['title'] = $data['about']->title;
          $data['main_content'] = $this->load->view('homes/sources/about_me', $data, true);
          $this->load->view('homes/index', $data);
    }
    public function publication() {
          $data = array();
          $data['org_info'] =$this->Website->getInstituteInfo();
          $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();
          $data['publication_list'] = $this->Publication->get_all_publications_by_details();
          $data['image_list']=$this->Website->get_image_list_by_type('P');
          $data['title'] = 'Publications';
          $data['show_footer']=1;
          $data['main_content'] = $this->load->view('homes/sources/publication', $data, true);
          $this->load->view('homes/index', $data);
    }
    public function research() {
          $data = array();
          $data['org_info'] =$this->Website->getInstituteInfo();
          $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();
          $data['research_list'] = $this->Research->get_research_list_for_home();
          $data['title'] = 'Research';
          $data['main_content'] = $this->load->view('homes/sources/research', $data, true);
          $this->load->view('homes/index', $data);
    }
    public function research_details($id) {
        $data = array();
        $data['org_info'] =$this->Website->getInstituteInfo();
        $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();

        $content=$this->Research->read_researchs($id);
        if(empty($content))
        {
          redirect("homes/empty_content");
        }
        $data['content']=$content;
        $data['title'] = $content->title;
        $data['main_content'] = $this->load->view('homes/sources/research_details', $data, true);
        $this->load->view('homes/index', $data);
    }
    public function contactus() {
          $data = array();
          if ($_POST) {
            $data = array();
            $data['email'] =$this->input->post('email', true);
            $data['name'] =$this->input->post('name', true);
            $data['subject'] =$this->input->post('subject', true);
            $data['message'] =$this->input->post('message', true);
            $data['date'] =date('Y-m-d');
            $this->db->insert('tbl_contact', $data);
            $result['status']='1';
            $result['message']='Thank you for getting in touch!';
            echo json_encode($result);
          }else{
            $data['org_info'] =$this->Website->getInstituteInfo();
            $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();
            $data['research_list'] = $this->Research->get_research_list_for_home();
            // echo "<pre>";
            // print_r($data['research_list']);
            // die;
            $data['title'] = 'Contact';
            $data['main_content'] = $this->load->view('homes/sources/contactus', $data, true);
            $this->load->view('homes/index', $data);
          }

    }
    public function content_details($id) {
        $data = array();
        $data['org_info'] =$this->Website->getInstituteInfo();
        $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();

        $content_type_list=$this->Website->get_content_type_by_menu_id($id);
        if(empty($content_type_list))
        {
          redirect("homes/empty_content");
        }
        $content = array();
        $i=0;
        foreach ($content_type_list as  $value) {
          if($value['content_type']=='P')
          {
            $details['paragraph_content']= $this->Website->get_content_details_by_menu_and_type($value['menu_id'],$value['content_type']);
            $content[$i++] = $this->load->view('homes/sources/paragraph_content', $details, true);

         }elseif ($value['content_type']=='Y') {
           $details['youtube_content']= $this->Website->get_content_details_by_menu_and_type($value['menu_id'],$value['content_type']);
           $content[$i++] = $this->load->view('homes/sources/youtube_content', $details, true);
         }
        }
        // echo "<pre>";
        // print_r($content_type_list);
        // die;
        $data['content']=$content;
        $data['title'] = $content_type_list[0]['menu_name'];
        $data['main_content'] = $this->load->view('homes/sources/content_details', $data, true);
        $this->load->view('homes/index', $data);
    }

    public function empty_content() {
          $data = array();
          $data['org_info'] =$this->Website->getInstituteInfo();
          $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();

          $data['title'] ='Comming Soon';
          $data['main_content'] = $this->load->view('homes/sources/empty_content', $data, true);
          $this->load->view('homes/index', $data);
    }
    public function teachings() {
          $data = array();
          $data['org_info'] =$this->Website->getInstituteInfo();
          $data['dynamic_menu']=$this->Website->get_dynamic_menu_list();
          $data['new_course_list']=$this->Website->get_teachings_list_by_type('NC');
          $data['old_course_list']=$this->Website->get_teachings_list_by_type('OC');
          $data['title'] ='Teaching Innovation';
          $data['main_content'] = $this->load->view('homes/sources/teachings', $data, true);
          $this->load->view('homes/index', $data);
    }


}
