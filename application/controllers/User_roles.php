<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_roles extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('User_role'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'User role';
      $data['heading_msg'] = 'User role';
      $data['is_show_button'] = "add";
      $data['user_roles'] = $this->User_role->get_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('user_roles/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

          if($this->User_role->checkifexist_role_name($this->input->post('role_name', true)))
          {
            $sdata['exception'] = "This role name '".$this->input->post('role_name', true)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("user_roles/add");
          }
          if ($this->User_role->add($this->input->post())) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("user_roles/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("user_roles/add");
          }
      }
        $data = array();
        $data['title'] = 'Add User Role';
        $data['heading_msg'] =  'Add User Role';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('user_roles/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {

          if($this->User_role->checkifexist_update_role_name($this->input->post('role_name', true),$id))
          {
            $sdata['exception'] = "This role name '".$this->input->post('role_name', true)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("user_roles/edit/".$id);
          }
          if ($this->User_role->edit($this->input->post(),$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("user_roles/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("user_roles/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update User Role';
        $data['heading_msg'] =  'Update User Role';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->User_role->read($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('user_roles/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
        $this->User_role->delete($id);
        $sdata['message'] = "delete";
        $this->session->set_userdata($sdata);
        redirect("user_roles/index");
    }
}
