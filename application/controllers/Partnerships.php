<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Partnerships extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Content','Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Partnerships';
      $data['heading_msg'] = 'Partnerships';
      $data['is_show_button'] = "add";
      $data['content'] = $this->Content->get_dentity_of_mother_concern_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('partnerships/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
          $content_file="";
          $content_type=$this->input->post('type', true);
          if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
           $type=$this->get_full_content_type($content_type);
            $content_file = $this->my_file_upload('txt_photo_location', $type);
            if ($content_file=='0') {
                $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("partnerships/add");
            }
         }

          $data = array();
          $data['title'] = $this->input->post('title', true);
          $data['order_no'] = $this->input->post('order_no', true);
          $data['type'] = $this->input->post('type', true);
          $data['description'] = $this->input->post('description', true);
          $data['link'] = $this->input->post('link', true);
          $data['photo_location'] = $content_file;
          if ($this->Content->add_dentity_of_mother_concern($data)) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("partnerships/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("partnerships/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Partnerships';
        $data['heading_msg'] =  'Add Partnerships';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('partnerships/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {
          $id=  $this->input->post('id', true);

         $old_file=  $this->input->post('old_photo_location', true);
         $content_file="";
         $content_type=$this->input->post('type', true);
         $content_file=  $old_file;
          if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
            $content_file = $this->my_file_upload('txt_photo_location', $content_type);
            if ($content_file=='0') {
                $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("partnerships/edit/".$id);
            }
            $this->my_file_remove($old_file,  $content_file);
         }
          $data = array();
          $data['id'] =$id;
          $data['title'] = $this->input->post('title', true);
          $data['order_no'] = $this->input->post('order_no', true);
          $data['type'] = $this->input->post('type', true);
          $data['description'] = $this->input->post('description', true);
          $data['link'] = $this->input->post('link', true);
          $data['photo_location'] = $content_file;
          if ($this->Content->edit_dentity_of_mother_concern($data,$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("partnerships/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("partnerships/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update Partnerships';
        $data['heading_msg'] =  'Update Partnerships';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->Content->read_dentity_of_mother_concern($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('partnerships/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
       $data= $this->Content->read_dentity_of_mother_concern($id);
       if(empty($data))
       {
         $sdata['exception'] = "Invalid Id!Please try again";
         $this->session->set_userdata($sdata);
         redirect("partnerships/index");
       }
       $photo= $data->photo_location;
        if($this->Content->delete_dentity_of_mother_concern($id))
        {
          $this->my_file_remove($photo, $photo);
          $sdata['message'] = "delete";
        }else{
          $sdata['exception'] = "Data cannot be Deleted";
        }
        $this->session->set_userdata($sdata);
        redirect("partnerships/index");
    }
    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/dist/img/mother_concern/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                'max_size' => "99999999999",
                'max_height' => "90000",
                'max_width' => "90000",
                'file_name' => $new_file_name
            );
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/dist/img/mother_concern/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
    private function get_full_content_type($type)
    {
      $full_type="";
      if($type=='AP')
      {
        $full_type="Academic";
      }elseif ($type=='RDP') {
        $full_type="Research";
      }
      return $full_type;
    }


}
