<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Menus extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('User_role','Menu'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Menu';
      $data['heading_msg'] = 'Menu';
      $data['is_show_button'] = "add";
      $data['menu_list'] = $this->Menu->get_menu_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('menus/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
          $parent_id=$this->input->post('menu_id', true);
          $name=$this->input->post("name", true);
          $order=$this->input->post("order", true);
          $link_able=$this->input->post("link_able", true);
          $is_active=1;
          if($this->Menu->checkifexist_menu($name))
          {
            $sdata['exception'] = "This menu '".$name."' already exists";
            $this->session->set_userdata($sdata);
            redirect("menus/add");
          }
          $data = array();
          $data['parent_id']= $parent_id;
          if($parent_id=='P')
          {
            $data['child_of']='-0-';
          } else {
            $menu_id=$_POST['menu_id'];
            $child = $this->Menu->get_child_by_parent_id($menu_id);
            // print_r($child); echo die;
            $tags = explode('-' , $child);
            $data['child_of']=$child.$menu_id.'-';
            if(count($tags)>4)
            {
              $sdata['exception'] = "Data cannot be Saved as Menu is more than 3";
              $this->session->set_userdata($sdata);
              redirect("menus/add");
            }
          }
          $data['name']=$name;
          $data['order']= $order;
          $data['is_active']=$is_active;
          $data['is_url']=$_POST['is_url'];
          $data['url_link']= $_POST['url_link'];
          if(count($tags)==4)
          {
              $data['link_able'] = 1;
          }
          else {
            $data['link_able']=$link_able;
          }
          if ($this->Menu->add_menu($data)) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("menus/add");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("menus/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Menu';
        $data['heading_msg'] =  'Add Menu';
        $data['is_show_button'] = "index";
        $data['menus'] = $this->Menu->get_menu_dropdown_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('menus/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {
        // print_r($_POST);
        // die;
          $id=$this->input->post('id', true);
          $is_active=$this->input->post('is_active', true);
          //$parent_id=$this->input->post('menu_id', true);
          $name=$this->input->post("name", true);
          $order=$this->input->post("order", true);
          $link_able=$this->input->post("link_able", true);
          if($this->Menu->checkifexist_update_menu_name($name,$id))
          {
            $sdata['exception'] = "This menu name '".$name."' already exist";
            $this->session->set_userdata($sdata);
            redirect("menus/edit/".$id);
          }
          if($this->Menu->checkifexist_child_by_id($id))
          {
            if($link_able=='1')
            {
              $sdata['exception'] = "The menu cannot be linkable because there is a sub-menu under this menu.";
              $this->session->set_userdata($sdata);
              redirect("menus/edit/".$id);
            }
          }
          // if($this->Menu->checkifexist_update_menu_child($id))
          // {
          //   $sdata['exception'] = "This menu name '".$name."' already exist";
          //   $this->session->set_userdata($sdata);
          //   redirect("menu/edit/".$id);
          // }
          $data = array();
          $data['id']=$id;
          $data['name']=$name;
          $data['order']= $order;
          $data['is_active']=$is_active;
        //  $data['parent_id']=$parent_id;
          $data['link_able']= $link_able;
          $data['is_url']=$_POST['is_url'];
          $data['url_link']= $_POST['url_link'];
          if ($this->Menu->edit_menu($data,$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("menus/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("menus/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update Menu';
        $data['heading_msg'] =  'Update Menu';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['menus'] = $this->Menu->get_menu_dropdown_list();
        $data['row'] = $this->Menu->read_menu($id);
        // $data['employees'] = $this->Employee->get_employee_dropdown_list();
        // $data['user_roles'] = $this->User_role->get_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('menus/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function updateMsgStatusMenusStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->Menu->edit_menu($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button></a>';
        }
    }

    public function getContenMaxOrder()
    {
      $menu_id=$_GET['menu_id'];
      $max_order = $this->Menu->get_content_max_order_by_menu_id($menu_id);
      // if(empty($max_order)){
      //   return false;
      // }
      echo json_encode($max_order);
    }

    public function delete($id)
    {
       $errorMsg=$this->Menu->checkifexist_menu_for_any_dependency_by_menu_id($id);
        if(empty($errorMsg))
        {
          $this->Menu->delete_menu($id);
          $sdata['message'] = "delete";
        }else{
            $sdata['exception'] =$errorMsg;
        }
        $this->session->set_userdata($sdata);
        redirect("menus/index");
    }

    function updateMsgStatusEmployeeStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->User_role->edit_user($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button></a>';
        }
    }
    public function getEmployeeCodeByEmployeeId()
    {
      $employee_id=$_GET['employee_id'];
      $employee_code = $this->Employee->get_employee_code_by_id($employee_id);
      if(empty($employee_code)){
        return false;
      }
      echo json_encode($employee_code);
    }
}
