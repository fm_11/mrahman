<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Faq_category extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'FAQ Category';
      $data['heading_msg'] = 'FAQ Category';
      $data['is_show_button'] = "add";
      $data['faq_list'] = $this->Admin_login->get_faq_category_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('faq_category/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

          if($this->Admin_login->checkifexist_faq_category_name($this->input->post('name', true)))
          {
            $sdata['exception'] = "This faq category name '".$this->input->post('name', true)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("faq_category/add");
          }
          if ($this->Admin_login->add_faq_category($this->input->post())) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("faq_category/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("faq_category/add");
          }
      }
        $data = array();
        $data['title'] = 'Add FAQ Category';
        $data['heading_msg'] =  'Add FAQ Category';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('faq_category/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {

          if($this->Admin_login->checkifexist_update_faq_category_name($this->input->post('name', true),$id))
          {
            $sdata['exception'] = "This faq category name '".$this->input->post('name', true)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("faq_category/edit/".$id);
          }
          if ($this->Admin_login->edit_faq_category($this->input->post(),$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("faq_category/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("faq_category/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update FAQ Category';
        $data['heading_msg'] =  'Update FAQ Category';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->Admin_login->read_faq_category($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('faq_category/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
       $errorMsg=$this->Admin_login->checkifexist_dependency_for_faq_category($id);
       if(empty($errorMsg))
       {
         $this->Admin_login->delete_faq_category($id);
         $sdata['message'] = "delete";
       }else{
           $sdata['exception'] =$errorMsg;
       }
        $this->session->set_userdata($sdata);
        redirect("faq_category/index");
    }
}
