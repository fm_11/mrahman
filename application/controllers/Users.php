<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('User_role'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $user_info = $this->session->userdata('user_info');

      $data = array();
      $data['current_user_id']=$user_info[0]->id;
      $data['title'] = 'Users';
      $data['heading_msg'] = 'Users';
      $data['is_show_button'] = "add";
      $data['user_list'] = $this->User_role->get_user_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('users/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
          $user_name=$this->input->post('user_name', true);
          $employee_id=$this->input->post("employee_id");
          if($this->User_role->checkifexist_user_name($user_name))
          {
            $sdata['exception'] = "This user name '".$user_name."' already exist";
            $this->session->set_userdata($sdata);
            redirect("users/add");
          }
          if($this->User_role->checkifexist_employee_id($employee_id))
          {
            $sdata['exception'] = "This employee '".$this->Employee->get_employee_name_by_id($employee_id)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("users/add");
          }
          $data = array();
          $password=$this->input->post("user_password");
          $data['user_password']= md5($password);
          $data['user_name']= $user_name;
          // $data['employee_id']=$employee_id;
          $data['user_role']=$this->input->post("user_role");
          if ($this->User_role->add_user($data)) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("users/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("user_roles/add");
          }
      }
        $data = array();
        $data['title'] = 'Add User';
        $data['heading_msg'] =  'Add User';
        $data['is_show_button'] = "index";
        // $data['employees'] = $this->Employee->get_employee_dropdown_without_existing_user_list();
        $data['user_roles'] = $this->User_role->get_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('users/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {
        // print_r($_POST);
        // die;
          $id=$this->input->post('id', true);
          $user_name=$this->input->post('user_name', true);
          $employee_id=$this->input->post("employee_id");
          if($this->User_role->checkifexist_update_user_name($user_name,$id))
          {
            $sdata['exception'] = "This user name '".$user_name."' already exist";
            $this->session->set_userdata($sdata);
            redirect("users/edit/".$id);
          }
          if($this->User_role->checkifexist_update_employee_id($employee_id,$id))
          {
            $sdata['exception'] = "This employee '".$this->Employee->get_employee_name_by_id($employee_id)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("users/edit/".$id);
          }
          $data = array();
          $password=$this->input->post("user_password");
          if($password!='expert')
          {
              $data['user_password']= md5($password);
          }
          $data['user_name']= $user_name;
          $data['employee_id']=$employee_id;
          $data['user_role']=$this->input->post("user_role");


          if ($this->User_role->edit_user($data,$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("users/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("users/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update User';
        $data['heading_msg'] =  'Update User';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->User_role->read_user($id);
        $data['user_roles'] = $this->User_role->get_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('users/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
       $user_info = $this->session->userdata('user_info');
       if($user_info[0]->id==$id)
       {
         $sdata['exception'] ="You cannot delete your own information. Please contact your administrator";
         $this->session->set_userdata($sdata);
         redirect("users/index");
       }
       $errorMsg=$this->User_role->checkifexist_user_for_any_dependency_by_user_id($id);
        if(empty($errorMsg))
        {
          $this->User_role->delete_user($id);
          $sdata['message'] = "delete";
        }else{
            $sdata['exception'] =$errorMsg;
        }
        $this->session->set_userdata($sdata);
        redirect("users/index");
    }
    public function change_password(){
       $user_info = $this->session->userdata('user_info');
       $current_user_id=$user_info[0]->id;
    		if($_POST){
           $password = $this->input->post("current_pass");
           $password = md5($password);
    		   $is_authenticated =$this->User_role->compare_current_password_by_user_id($current_user_id, $password);
    		   if ($is_authenticated > 0) {
                   $data = array();
                   $new_password= md5($this->input->post("new_pass"));
          			   $data['user_password'] =$new_password;
          			   $data['id'] = $current_user_id;
          			   if($this->User_role->edit_user($data,$current_user_id))
                   {
                     $history = array();
                     $history['user_id']= $current_user_id;
                     $history['old_pass']= $password;
                     $history['new_pass']=$new_password;
                     $history['date']=date("Y-m-d H:i:s");
                     $this->db->insert('tbl_user_pass_history', $history);
                     $sdata['message'] = "You are Successfully Password Changes.";
                   }else{
                     $sdata['exception'] = "Sorry password cannot be Updated.Please try again";
                   }
                   $this->session->set_userdata($sdata);
                   redirect("users/change_password/".$user_id);
    			}else {
    				$sdata['exception'] = "Sorry current password does not match.Please try again";
    				$this->session->set_userdata($sdata);
    				redirect("users/change_password");
    			}
    		}else{
			      $data['title'] = 'Change Password';
            $data['heading_msg'] = "Change Password";
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('users/change_password', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    		}
    	}

    function updateMsgStatusEmployeeStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->User_role->edit_user($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button></a>';
        }
    }
    public function getEmployeeCodeByEmployeeId()
    {
      $employee_id=$_GET['employee_id'];
      $employee_code = $this->Employee->get_employee_code_by_id($employee_id);
      if(empty($employee_code)){
        return false;
      }
      echo json_encode($employee_code);
    }
}
