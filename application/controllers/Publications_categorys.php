<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Publications_categorys extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Publication'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Publications Category';
      $data['heading_msg'] = 'Publications Category';
      $data['is_show_button'] = "add";
      $data['publication_list'] = $this->Publication->get_publications_category_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('publications_categorys/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

          $content_file1="";
          $content_file2="";
          $content_file3="";
          if (isset($_FILES['txt_photo_location1']) && $_FILES['txt_photo_location1']['name'] != '') {
            $content_file1 = $this->my_file_upload('txt_photo_location1', 'publications_img1_');
            if ($content_file1=='0') {
                $sdata['exception'] = "Image 1 doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("publications_categorys/add");
            }
         }
         if (isset($_FILES['txt_photo_location2']) && $_FILES['txt_photo_location2']['name'] != '') {
           $content_file2 = $this->my_file_upload('txt_photo_location2', 'publications_img2_');
           if ($content_file2=='0') {
               $sdata['exception'] = "Image 2 doesn't upload." . $this->upload->display_errors();
               $this->session->set_userdata($sdata);
               redirect("publications_categorys/add");
           }
        }
        if (isset($_FILES['txt_photo_location3']) && $_FILES['txt_photo_location3']['name'] != '') {
          $content_file3 = $this->my_file_upload('txt_photo_location1', 'publications_img3_');
          if ($content_file3=='0') {
              $sdata['exception'] = "Image 3 doesn't upload." . $this->upload->display_errors();
              $this->session->set_userdata($sdata);
              redirect("publications_categorys/add");
          }
       }

          $data = array();
          $data['name'] = $this->input->post('name', true);
          $data['order_no'] = $this->input->post('order_no', true);
          $data['file_location_1'] = $content_file1;
          $data['file_location_2'] = $content_file2;
          $data['file_location_3'] = $content_file3;
          if ($this->Publication->add_publications_category($data)) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("publications_categorys/add");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("publications_categorys/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Publications Category';
        $data['heading_msg'] =  'Add Publications Category';
        $data['is_show_button'] = "index";
        $data['category'] = $this->Publication->get_publications_category_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('publications_categorys/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id){

      if ($_POST) {
         $id=  $this->input->post('id', true);

         $old_file1=  $this->input->post('old_photo_location_1', true);
         $old_file2=  $this->input->post('old_photo_location_2', true);
         $old_file3=  $this->input->post('old_photo_location_3', true);

         $content_file1=""; $content_file2=""; $content_file3="";
         $content_file1=  $old_file1; $content_file2=  $old_file2; $content_file3=  $old_file3;

          if (isset($_FILES['txt_photo_location1']) && $_FILES['txt_photo_location1']['name'] != '') {
            $content_file1 = $this->my_file_upload('txt_photo_location1', 'publications_img1_');
            if ($content_file1=='0') {
                $sdata['exception'] = "Image 1 doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("publications_categorys/edit/".$id);
            }
            $this->my_file_remove($old_file1,  $content_file1);
         }

         if (isset($_FILES['txt_photo_location2']) && $_FILES['txt_photo_location2']['name'] != '') {
           $content_file2 = $this->my_file_upload('txt_photo_location2', 'publications_img2_');
           if ($content_file1=='0') {
               $sdata['exception'] = "Image 2 doesn't upload." . $this->upload->display_errors();
               $this->session->set_userdata($sdata);
               redirect("publications_categorys/edit/".$id);
           }
           $this->my_file_remove($old_file2,  $content_file2);
        }

        if (isset($_FILES['txt_photo_location3']) && $_FILES['txt_photo_location3']['name'] != '') {
          $content_file3 = $this->my_file_upload('txt_photo_location3', 'publications_img3_');
          if ($content_file3=='0') {
              $sdata['exception'] = "Image 3 doesn't upload." . $this->upload->display_errors();
              $this->session->set_userdata($sdata);
              redirect("publications_categorys/edit/".$id);
          }
          $this->my_file_remove($old_file3,  $content_file3);
       }

          $data = array();
          $data['id'] =$id;
          $data['name'] = $this->input->post('name', true);
          $data['order_no'] = $this->input->post('order_no', true);
          $data['file_location_1'] = $content_file1;
          $data['file_location_2'] = $content_file2;
          $data['file_location_3'] = $content_file3;
          if ($this->Publication->edit_publications_category($data,$id)) {
              $sdata['message'] = "Data successfully updated";
              $this->session->set_userdata($sdata);
              redirect("publications_categorys/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("publications_categorys/edit/".$id);
          }
      }

      $data = array();
      $data['title'] = 'Update Publication Categorys';
      $data['heading_msg'] =  'Update Publication Categorys';
      $data['is_show_button'] = "index";
      $data['action'] = 'edit/' . $id;
      $data['row'] = $this->Publication->read_publications_category($id);
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('publications_categorys/edit', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
  }

    public function delete($id)
    {
       $errorMsg=$this->Publication->checkifexist_category_for_any_dependency_by_id($id);
       if(empty($errorMsg))
       {
         $data= $this->Publication->read_publications_category($id);
         $photo1= $data->file_location_1;
         $photo2= $data->file_location_2;
         $photo3= $data->file_location_3;
         if($this->Publication->delete_publications_category($id)){
           if(!empty($photo1)){$this->my_file_remove($photo1, $photo1);}
           if(!empty($photo2)){$this->my_file_remove($photo2, $photo2);}
           if(!empty($photo3)){$this->my_file_remove($photo3, $photo3);}
           $sdata['message'] = "Data successfully deleted";
         }else{
             $sdata['exception'] =$errorMsg;
       }
       }else{
           $sdata['exception'] =$errorMsg;
            }
        $this->session->set_userdata($sdata);
        redirect("publications_categorys/index");
    }

    public function updateMsgContentStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->Publication->edit_publications_category($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button></a>';
        }
    }

    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/publication/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx|gif",
                'max_size' => "99999999999",
                'max_height' => "90000",
                'max_width' => "90000",
                'file_name' => $new_file_name
            );
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }

     public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/publication/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
}
