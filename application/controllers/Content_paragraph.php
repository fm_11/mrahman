<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Content_paragraph extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Content','Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Content Paragraph';
      $data['heading_msg'] = 'Content Paragraph';
      $data['is_show_button'] = "add";
      $data['content'] = $this->Content->get_content_details_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('content_paragraph/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
          $content_file="";
          $content_type=$this->input->post('content_type', true);
          if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
           $type=$this->get_full_content_type($content_type);
            $content_file = $this->my_file_upload('txt_photo_location', $type);
            if ($content_file=='0') {
                $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("content_paragraph/add");
            }
         }
         $allowedTags = '<style><iframe><p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
         $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a><b>';
        // $youtube_link = strip_tags(stripslashes($_POST['youtube_link']), $allowedTags);
         $message = strip_tags(stripslashes($this->input->post('message', true)), $allowedTags);

          $data = array();
          $data['title'] = $this->input->post('title', true);
          $data['message'] = $message;
          $data['content_type'] = $this->input->post('content_type', true);
          $data['youtube_link'] = $this->input->post('youtube_link', true);
          $data['menu_id'] = $this->input->post('menu_id', true);
          $data['order_no'] = $this->input->post('order_no', true);
          $data['employee_id'] = $this->input->post('employee_id', true);
          $data['photo_possition'] = $this->input->post('photo_possition', true);
          $data['instructor_id'] = $this->input->post('instructor_id', true);
          $data['width'] = $this->input->post('width', true);
          $data['height'] = $this->input->post('height', true);
          $data['photo_location'] = $content_file;
          if ($this->Content->add_content_details($data)) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("content_paragraph/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("content_paragraph/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Content Paragraph';
        $data['heading_msg'] =  'Add Content Paragraph';
        $data['is_show_button'] = "index";
        $data['menus'] = $this->Content->get_link_able_menu();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('content_paragraph/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {
          $id=  $this->input->post('id', true);

         $old_file=  $this->input->post('old_photo_location', true);
         $content_file="";
         $content_type=$this->input->post('content_type', true);
         $content_file=  $old_file;
          if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
            $content_file = $this->my_file_upload('txt_photo_location', $content_type);
            if ($content_file=='0') {
                $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("content_paragraph/edit/".$id);
            }
            $this->my_file_remove($old_file,  $content_file);
         }

         $allowedTags = '<iframe><p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
         $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a><b>';
         //$youtube_link = strip_tags(stripslashes($_POST['youtube_link']), $allowedTags);
         $message = strip_tags(stripslashes($this->input->post('message', true)), $allowedTags);
          $data = array();
          $data['id'] =$id;
          $data['title'] = $this->input->post('title', true);
          $data['message'] = $message;
          $data['content_type'] = $this->input->post('content_type', true);
          $data['youtube_link'] =  $this->input->post('youtube_link', true);
          $data['menu_id'] = $this->input->post('menu_id', true);
          $data['order_no'] = $this->input->post('order_no', true);
          $data['employee_id'] = $this->input->post('employee_id', true);
          $data['photo_possition'] = $this->input->post('photo_possition', true);
          $data['instructor_id'] = $this->input->post('instructor_id', true);
          $data['width'] = $this->input->post('width', true);
          $data['height'] = $this->input->post('height', true);
          $data['photo_location'] = $content_file;
          if ($this->Content->edit_content_details($data,$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("content_paragraph/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("content_paragraph/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update Content Paragraph';
        $data['heading_msg'] =  'Update Content Paragraph';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->Content->read_content_details($id);
        $data['menus'] = $this->Content->get_link_able_menu();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('content_paragraph/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
       $data= $this->Content->read_content_details($id);
       if(empty($data))
       {
         $sdata['exception'] = "Invalid Id!Please try again";
         $this->session->set_userdata($sdata);
         redirect("content_paragraph/index");
       }
       $photo= $data->photo_location;
        if($this->Content->delete_content_details($id))
        {
          $this->my_file_remove($photo, $photo);
          $sdata['message'] = "delete";
        }else{
          $sdata['exception'] = "Data cannot be Deleted";
        }
        $this->session->set_userdata($sdata);
        redirect("content_paragraph/index");
    }
    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/dist/img/content/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx|gif",
                'max_size' => "99999999999",
                'max_height' => "90000",
                'max_width' => "90000",
                'file_name' => $new_file_name
            );
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/dist/img/content/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
    private function get_full_content_type($type)
    {
      $full_type="";
      if($type=='L')
      {
        $full_type="List";
      }elseif ($type=='P') {
        $full_type="Paragraph";
      }
      return $full_type;
    }
    function updateMsgContentStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->Content->edit_content_details($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-xs">Inactive</button></a>';
        }
    }
    public function getContenMaxOrder()
    {
      $menu_id=$_GET['menu_id'];
      $max_order = $this->Content->get_content_max_order_by_menu_id($menu_id);
      // if(empty($max_order)){
      //   return false;
      // }
      echo json_encode($max_order);
    }
}
