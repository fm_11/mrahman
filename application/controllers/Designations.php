<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Designations extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Expertise';
      $data['heading_msg'] = 'Expertise';
      $data['is_show_button'] = "add";
      $data['designations_list'] = $this->Admin_login->get_designation_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('designations/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

          if($this->Admin_login->checkifexist_designation_name($this->input->post('name', true)))
          {
            $sdata['exception'] = "This  name '".$this->input->post('name', true)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("designations/add");
          }
          if ($this->Admin_login->add_designation($this->input->post())) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("designations/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("designations/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Expertise';
        $data['heading_msg'] =  'Add Expertise';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('designations/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {

          if($this->Admin_login->checkifexist_update_designation_name($this->input->post('name', true),$id))
          {
            $sdata['exception'] = "This name '".$this->input->post('name', true)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("designations/edit/".$id);
          }
          if ($this->Admin_login->edit_designation($this->input->post(),$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("designations/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("designations/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update Expertise';
        $data['heading_msg'] =  'Update Expertise';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->Admin_login->read_designation($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('designations/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
        $errorMsg=$this->Admin_login->checkifexist_designation_dependency_for_delete($id);
        if(empty($errorMsg))
        {
          $this->Admin_login->delete_designation($id);
          $sdata['message'] = "delete";
        }else{
            $sdata['exception'] =$errorMsg;
        }
        $this->session->set_userdata($sdata);
        redirect("designations/index");
    }
}
