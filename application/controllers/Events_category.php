<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Events_category extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('News_and_event'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'News Category';
      $data['heading_msg'] = 'News Category';
      $data['is_show_button'] = "add";
      $data['events_list'] = $this->News_and_event->get_news_and_blog_category_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('events_category/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

          if($this->News_and_event->checkifexist_news_and_blog_name($this->input->post('name', true)))
          {
            $sdata['exception'] = "This events category name '".$this->input->post('name', true)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("events_category/add");
          }
          if ($this->News_and_event->add_news_and_blog_category($this->input->post())) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("events_category/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("events_category/add");
          }
      }
        $data = array();
        $data['title'] = 'Add News Category';
        $data['heading_msg'] =  'Add News Category';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('events_category/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {

          if($this->News_and_event->checkifexist_update_news_and_blog_name($this->input->post('name', true),$id))
          {
            $sdata['exception'] = "This events category name '".$this->input->post('name', true)."' already exist";
            $this->session->set_userdata($sdata);
            redirect("events_category/edit/".$id);
          }
          if ($this->News_and_event->edit_news_and_blog_category($this->input->post(),$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("events_category/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("events_category/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update News Category';
        $data['heading_msg'] =  'Update News Category';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->News_and_event->read_news_and_blog_category($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('events_category/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
       $errorMsg=$this->News_and_event->checkifexist_category_for_any_dependency_by_id($id);
       if(empty($errorMsg))
       {
         $this->News_and_event->delete_news_and_blog_category($id);
         $sdata['message'] = "delete";
       }else{
           $sdata['exception'] =$errorMsg;
       }
        $this->session->set_userdata($sdata);
        redirect("events_category/index");
    }
}
