<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Course_categorys extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Course_category'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User!";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Course Category';
      $data['heading_msg'] = 'Course Category';
      $data['is_show_button'] = "add";
      $data['course_category_list'] = $this->Course_category->get_course_category_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('course_categorys/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

          if($this->Course_category->checkifexist_course_category($this->input->post('name', true)))
          {
            $sdata['exception'] = "This course category name '".$this->input->post('name', true)."' already exists";
            $this->session->set_userdata($sdata);
            redirect("course_categorys/add");
          }
          if ($this->Course_category->add_course_category($this->input->post())) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("course_categorys/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("course_categorys/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Course Category';
        $data['heading_msg'] =  'Add Course Category';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('course_categorys/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {

          if($this->Course_category->checkifexist_update_course_category($this->input->post('name', true),$id))
          {
            $sdata['exception'] = "This course category name '".$this->input->post('name', true)."' already existw";
            $this->session->set_userdata($sdata);
            redirect("course_categorys/edit/".$id);
          }
          if ($this->Course_category->edit_course_category($this->input->post(),$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("course_categorys/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("course_categorys/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update Course Category';
        $data['heading_msg'] =  'Update Course Category';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->Course_category->read_course_category($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('course_categorys/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
       $errorMsg=$this->Course_category->checkifexist_category_for_any_dependency_by_id($id);
       if(empty($errorMsg))
       {
         $this->Course_category->delete_course_category($id);
         $sdata['message'] = "delete";
       }else{
           $sdata['exception'] =$errorMsg;
       }
        $this->session->set_userdata($sdata);
        redirect("course_categorys/index");
    }

    function updateMsgContentStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->Course_category->edit_course_category($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button></a>';
        }
    }
}
