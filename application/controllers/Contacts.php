<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Contacts extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Content','Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Contacts List';
      $data['heading_msg'] = 'Contacts List';
      //$data['is_show_button'] = "add";
      $data['contacts'] = $this->Content->get_contacts_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('contacts/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
      if($this->Content->delete_contacts($id))
      {
        $sdata['message'] = "delete";
      }else{
        $sdata['exception'] = "Data cannot be Deleted";
      }
      $this->session->set_userdata($sdata);
      redirect("contacts/index");
    }

}
