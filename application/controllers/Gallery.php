<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Gallery extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('News_and_event','Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Gallery';
      $data['heading_msg'] = 'Gallery';
      $data['is_show_button'] = "add";
      $cond = array();
      $data['gallery_list'] = $this->Admin_login->get_all_galley_list($cond);
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('gallery/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
        // echo "<pre>";
        // print_r($_POST);
        // die;

         $file_data = array();
         $file_loop_time = $this->input->post("file_upload_num_of_row");
         $file_label_name = $this->input->post("title");
         $date = $this->input->post("date");
         $reference_id=$this->Admin_login->get_max_reference_no();
         $j = 0;
         while ($j < $file_loop_time) {
           if (isset($_FILES['txt_photo_location_' . $j]) && $_FILES['txt_photo_location_' . $j]['name'] != '') {
                   $image_name = 'txt_photo_location_' . $j;
                   $new_file_name = $this->my_file_upload($image_name, "File_".$reference_id."_".$j);
                  if ($new_file_name == '0') {
                       $sdata['exception'] = "Sorry! this file '".$this->input->post("description_". $j)."'  doesn't upload." . $this->upload->display_errors();
                       $this->session->set_userdata($sdata);
                       redirect("gallery/add/");
                   }
                   $file_data[$j]['title']=$file_label_name;
                   $file_data[$j]['date']=$date;
                   $file_data[$j]['description']=$this->input->post("description_". $j);
                   $file_data[$j]['file_path']=$new_file_name;
                   $file_data[$j]['reference_id']=$reference_id;
           }
           $j++;
         }
         // print_r($file_data);
         // die;

          if (count($file_data)>0) {
              $this->db->insert_batch('tbl_gallery', $file_data);
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("gallery/add");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("gallery/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Gallery';
        $data['heading_msg'] =  'Add Gallery';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('gallery/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {

        $file_data = array();
          $file_loop_time = $this->input->post("file_upload_num_of_row");
          $file_label_name = $this->input->post("title");
          $reference_id= $this->input->post("reference_id");
          $date = $this->input->post("date");

          $j = 0;
          while ($j < $file_loop_time) {
            if (isset($_FILES['txt_photo_location_' . $j]) && $_FILES['txt_photo_location_' . $j]['name'] != '') {
                    $image_name = 'txt_photo_location_' . $j;
                    $new_file_name = $this->my_file_upload($image_name, "File_".$reference_id."_".$j);
                   if ($new_file_name == '0') {
                        $sdata['exception'] = "Sorry! this file '".$this->input->post("description_". $j)."'  doesn't upload." . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("gallery/edit/".$reference_id);
                    }
                    $file_data[$j]['title']=$file_label_name;
                    $file_data[$j]['date']=$date;
                    $file_data[$j]['description']=$this->input->post("description_". $j);
                    $file_data[$j]['file_path']=$new_file_name;
                    $file_data[$j]['reference_id']=$reference_id;
            }
            $j++;
          }
          if(count($file_data)>0)
          {
            $this->db->insert_batch('tbl_gallery', $file_data);
          }
          $master_data=array();
          $master_data['title']=$file_label_name;
          $master_data['date']=$date;
          if($this->Admin_login->edit_gallery_by_referenceId($master_data,$reference_id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("gallery/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("gallery/edit/".$reference_id);
          }
      }
        $data = array();
        $data['title'] = 'Update Gallery';
        $data['heading_msg'] =  'Update Gallery';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $gallery_info = $this->Admin_login->read_gallery_by_referenceId($id);
        if(empty($gallery_info))
        {
          $sdata['exception'] = "Invalid Id";
          $this->session->set_userdata($sdata);
          redirect("gallery/index");
        }
        $data['row'] = $gallery_info;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('gallery/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
      $gallery_info = $this->Admin_login->read_gallery_by_referenceId($id);
       if(empty($gallery_info))
       {
         $sdata['exception'] = "Invalid Id!Please try again";
         $this->session->set_userdata($sdata);
         redirect("gallery/index");
       }
       $i=0;
       foreach ($gallery_info as $row) {
         $file = $row['file_path'];
         if (!empty($file)) {
           $i++;
             $this->my_file_remove($file,"new");
         }
       }
        if($i==count($gallery_info))
        {
          $this->Admin_login->delete_gallery_by_referenceId($id);
          $sdata['message'] = "delete";
        }else{
          $sdata['exception'] = "Data cannot be Deleted";
        }
        $this->session->set_userdata($sdata);
        redirect("gallery/index");
    }
    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/dist/img/gallery/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                'max_size' => "99999999999",
                'max_height' => "90000",
                'max_width' => "90000",
                'file_name' => $new_file_name
            );
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/dist/img/gallery/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
  public function delete_file()
  {
      $id = $this->input->get('id', true);
      $file=$this->Admin_login->read_gallery($id);
      $old_file=$file->file_path;
      $this->Admin_login->delete_gallery($id);
      $this->my_file_remove($old_file,"delete");
  }
      function view($id)
      {
          $gallery_info = $this->Admin_login->read_gallery_by_referenceId($id);

          if(empty($gallery_info))
          {
            $sdata['exception'] = "Invalid";
            $this->session->set_userdata($sdata);
            redirect("gallery/index");
          }

          $data = array();
          $data['title'] = 'View Gallery';
          $data['heading_msg'] = "View Gallery";
          $data['is_show_button'] = "index";
          $data['row'] = $gallery_info;
          $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
          $data['maincontent'] = $this->load->view('gallery/view', $data, true);
          $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
      }
}
