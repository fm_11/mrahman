<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Teachings extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Teachings List';
      $data['heading_msg'] = 'Teachings List';
      $data['is_show_button'] = "add";
      $data['teachings_list'] = $this->Admin_login->get_teachings_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('teachings/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

        $content_file="";
        if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
          $content_file = $this->my_file_upload('txt_photo_location', 'Teachings');
          if ($content_file=='0') {
              $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors();
              $this->session->set_userdata($sdata);
              redirect("teachings/add");
          }
       }

        $data = array();
        $data['title'] = $this->input->post('title', true);
        $data['description'] = $this->input->post('description', true);
        $data['type'] = $this->input->post('type', true);
        $data['order_no'] = $this->input->post('order_no', true);
        $data['head_title'] = $this->input->post('head_title', true);
        $data['file_location'] = $content_file;
          if ($this->Admin_login->add_teachings($data)) {
              $sdata['message'] = "Data successfully added";
              $this->session->set_userdata($sdata);
              redirect("teachings/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("teachings/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Teachings';
        $data['heading_msg'] =  'Add Teachings';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('teachings/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {
          $data = array();
          $id=$this->input->post('id', true);

           $old_file=  $this->input->post('old_photo_location', true);
           $file="";
           $content_file=  $old_file;
            if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
              $content_file = $this->my_file_upload('txt_photo_location', 'Teachings');
              if ($content_file=='0') {
                  $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors();
                  $this->session->set_userdata($sdata);
                  redirect("teachings/edit/".$id);
              }
              $this->my_file_remove($old_file,  $content_file);
           }
          $data['id'] = $id;
          $data['title'] = $this->input->post('title', true);
          $data['description'] = $this->input->post('description', true);
          $data['type'] = $this->input->post('type', true);
          $data['order_no'] = $this->input->post('order_no', true);
          $data['head_title'] = $this->input->post('head_title', true);
          $data['file_location'] = $content_file;

          if ($this->Admin_login->edit_teachings($data,$id)) {
              $sdata['message'] = "Data successfully updated";
              $this->session->set_userdata($sdata);
              redirect("teachings/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("teachings/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update Teachings';
        $data['heading_msg'] =  'Update Teachings';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->Admin_login->read_teachings($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('teachings/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
      $data= $this->Admin_login->read_teachings($id);
      if(empty($data))
      {
        $sdata['exception'] = "Invalid Id! Please try again";
        $this->session->set_userdata($sdata);
        redirect("teachings/index");
      }
      $photo= $data->file_location;
       if($this->Admin_login->delete_teachings($id))
       {
         $this->my_file_remove($photo, $photo);
         $sdata['message'] = "Data successfully deleted";
       }else{
         $sdata['exception'] = "Data cannot be Deleted";
       }
       $this->session->set_userdata($sdata);
       redirect("teachings/index");
    }

    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/dist/img/documents/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx|gif",
                'max_size' => "99999999999",
                'max_height' => "9000000",
                'max_width' => "9000000",
                'file_name' => $new_file_name
            );
        if($type=="Slider")
        {
          $config = array(
                  'upload_path' => "core_media/adminpanel/dist/img/documents/",
                  'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|gif",
                  'max_size' => "99999999999",
                  'max_height' => "90000",
                  'max_width' => "90000",
                  'file_name' => $new_file_name
              );
        }
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }

    public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/dist/img/documents/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }

    function updateMsgContentStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->Admin_login->edit_teachings($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button></a>';
        }
    }

    public function getContenMaxOrder()
    {
      $type=$_GET['type'];
      $max_order = $this->Admin_login->get_teachings_max_order_by_menu_id($type);
      echo json_encode($max_order);
    }
}
