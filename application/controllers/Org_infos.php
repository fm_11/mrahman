<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Org_infos extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {

      $orga_info=  $this->Admin_login->get_organization_info();
        if ($_POST) {
             $id=$orga_info->id;
              $old_photo=  $this->input->post('old_photo_location', true);
              $org_img=  $old_photo;
               if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
                 $org_img = $this->my_file_upload('txt_photo_location', 'orga');
                 if ($org_img=='0') {
                     $sdata['exception'] = "Photo doesn't upload." . $this->upload->display_errors();
                     $this->session->set_userdata($sdata);
                     redirect("org_infos/index");
                 }
                 $this->my_file_remove($old_photo,  $org_img);
              }

          // echo "<pre>";
          // print_r($this->input->post('google_map', true));
          // die;
          //
          // $allowedTags = '<iframe><p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
          // $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
          // $google_map = strip_tags(stripslashes($_POST['google_map']), $allowedTags);
          //$address = strip_tags(stripslashes($_POST['address']), $allowedTags);

           $data = array();
           $data['id'] =$id;
           $data['school_name'] = $this->input->post('school_name', true);
           $data['tin_number'] = $this->input->post('tin_number', true);
           $data['address'] = $this->input->post('address', true);
           $data['email'] = $this->input->post('email', true);
           $data['mobile'] = $this->input->post('mobile', true);
           $data['facebook_address'] = $this->input->post('facebook_address', true);
           $data['web_address'] = $this->input->post('web_address', true);
           $data['twitter'] = $this->input->post('twitter', true);
           $data['google_plus'] = $this->input->post('google_plus', true);
           $data['google_map'] =  $this->input->post('google_map', true);
           $data['linkedin'] = $this->input->post('linkedin', true);
           // $data['whatsapp'] = $this->input->post('whatsapp', true);
           $data['width'] = $this->input->post('width', true);
           $data['height'] = $this->input->post('height', true);
           // $data['workshop'] = $this->input->post('workshop', true);
           // $data['course'] = $this->input->post('course', true);
           $data['picture'] = $org_img;

           if ($this->Admin_login->edit_organization_info($data,$id)) {
               $sdata['message'] = "update";
               $this->session->set_userdata($sdata);
           } else {
               $sdata['exception'] = "Data cannot be Updated";
               $this->session->set_userdata($sdata);
           }
           redirect("org_infos/index");
        }
      $data = array();
      $data['title'] = 'Update Basic Informations';
      $data['heading_msg'] = 'Update Basic Informations';
      $data['row'] =$orga_info;
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('org_infos/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/dist/img/orga/",
                'allowed_types' => "GIF|gif|avi|mp4|3gp|mpeg|mpg|mov|mp3|flv|wmv",
                'max_size' => "99999999999",
                'max_height' => "3000",
                'max_width' => "3000",
                'file_name' => $new_file_name
            );
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/dist/img/orga/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
}
