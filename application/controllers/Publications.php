<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Publications extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Publication'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Publications List';
      $data['heading_msg'] = 'Publications List';
      $data['is_show_button'] = "add";
      $data['publication_list'] = $this->Publication->get_publications_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('publications/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

        $data = array();
        $data['publication_category_id'] = $this->input->post('publication_category_id', true);
        $data['year'] = $this->input->post('year', true);
        $data['order_no'] = $this->input->post('order_no', true);
        $data['title'] = $this->input->post('title', true);
          if ($this->Publication->add_publications($data)) {
              $sdata['message'] = "Data successfully saved";
              $this->session->set_userdata($sdata);
              redirect("publications/add");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("publications/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Publications';
        $data['heading_msg'] =  'Add Publications';
        $data['is_show_button'] = "index";
        $data['publications_category'] = $this->Publication->get_publications_category_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('publications/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {
          $data = array();
          $id=$this->input->post('id', true);
          $data['id'] = $id;
          $data['publication_category_id'] = $this->input->post('publication_category_id', true);
          $data['year'] = $this->input->post('year', true);
          $data['order_no'] = $this->input->post('order_no', true);
          $data['title'] = $this->input->post('title', true);

          if ($this->Publication->edit_publications($data,$id)) {
              $sdata['message'] = "Data successfully updated";
              $this->session->set_userdata($sdata);
              redirect("publications/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("publications/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update Publications';
        $data['heading_msg'] =  'Update Publications';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->Publication->read_publications($id);
        $data['publications_category'] = $this->Publication->get_publications_category_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('publications/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
        $this->Publication->delete_publications($id);
        $sdata['message'] = "Data successfully deleted";
        $this->session->set_userdata($sdata);
        redirect("publications/index");
    }
    public function updateMsgContentStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->Publication->edit_publications($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button></a>';
        }
    }

    public function get_pub_max_order_by_pub_category_id()
    {
      $category_id=$_GET['publication_category_id'];
      $max_order = $this->Publication->get_pub_max_order_by_pub_category_id($category_id);
      echo json_encode($max_order);
    }
}
