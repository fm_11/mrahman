<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Faq extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'FAQ';
      $data['heading_msg'] = 'FAQ';
      $data['is_show_button'] = "add";
      $data['faq_list'] = $this->Admin_login->get_faq_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('faq/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {

        $data = array();
        $data['faq_category_id'] = $this->input->post('faq_category_id', true);
        $data['order_no'] = $this->input->post('order_no', true);
        $data['message'] = $this->input->post('message', true);
          if ($this->Admin_login->add_faq($data)) {
              $sdata['message'] = "save";
              $this->session->set_userdata($sdata);
              redirect("faq/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("faq/add");
          }
      }
        $data = array();
        $data['title'] = 'Add FAQ';
        $data['heading_msg'] =  'Add FAQ';
        $data['is_show_button'] = "index";
        $data['faq_category'] = $this->Admin_login->get_faq_category_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('faq/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {
          $data = array();
          $id=$this->input->post('id', true);
          $data['id'] = $id;
          $data['faq_category_id'] = $this->input->post('faq_category_id', true);
          $data['order_no'] = $this->input->post('order_no', true);
          $data['message'] = $this->input->post('message', true);
          if ($this->Admin_login->edit_faq($data,$id)) {
              $sdata['message'] = "update";
              $this->session->set_userdata($sdata);
              redirect("faq/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("faq/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update FAQ';
        $data['heading_msg'] =  'Update FAQ';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->Admin_login->read_faq($id);
        $data['faq_category'] = $this->Admin_login->get_faq_category_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('faq/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
       $this->Admin_login->delete_faq($id);
        $sdata['message'] = "delete";
        $this->session->set_userdata($sdata);
        redirect("faq/index");
    }
    public function get_faq_max_order_by_faq_category_id()
    {
      $category_id=$_GET['category_id'];
      $max_order = $this->Admin_login->get_faq_max_order_by_faq_category_id($category_id);
      echo json_encode($max_order);
    }
}
