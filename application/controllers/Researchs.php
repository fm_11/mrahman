<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Researchs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Research'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Research Statement & Goals';
      $data['heading_msg'] = 'Research Statement & Goals';
      $data['is_show_button'] = "add";
      $data['content'] = $this->Research->get_research_list();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('researchs/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
          $content_file="";
          $content_file2="";
          $content_file3="";
          if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
            $content_file = $this->my_file_upload('txt_photo_location', 'Research_Statment');
            if ($content_file=='0') {
                $sdata['exception'] = "Image 1 doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("researchs/add");
            }
         }
         if (isset($_FILES['txt_photo_location2']) && $_FILES['txt_photo_location2']['name'] != '') {
           $content_file2 = $this->my_file_upload('txt_photo_location2', 'Research_Details');
           if ($content_file2=='0') {
               $sdata['exception'] = "Image 2 doesn't upload." . $this->upload->display_errors();
               $this->session->set_userdata($sdata);
               redirect("researchs/add");
           }
        }
        if (isset($_FILES['txt_photo_location3']) && $_FILES['txt_photo_location3']['name'] != '') {
          $content_file3 = $this->my_file_upload('txt_photo_location3', 'Research_Details_3');
          if ($content_file2=='0') {
              $sdata['exception'] = "Image 3 doesn't upload." . $this->upload->display_errors();
              $this->session->set_userdata($sdata);
              redirect("researchs/add");
          }
       }
        $allowedTags = '<style><iframe><p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
        $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a><b>';
       // $youtube_link = strip_tags(stripslashes($_POST['youtube_link']), $allowedTags);
        $description = strip_tags(stripslashes($this->input->post('description', true)), $allowedTags);

          $data = array();
          $data['title'] = $this->input->post('title', true);
          $data['description'] = $description;
          $data['order'] = $this->input->post('order', true);
          $data['show_feature_image'] = $this->input->post('show_feature_image', true);
          $data['text_row'] = $this->input->post('text_row', true);
          $data['image_row'] = $this->input->post('image_row', true);
          $data['file_location'] = $content_file;
          $data['file_location2'] = $content_file2;
          $data['file_location3'] = $content_file3;
          if ($this->Research->add_researchs($data)) {
              $sdata['message'] = "Data successfully saved";
              $this->session->set_userdata($sdata);
              redirect("researchs/index");
          } else {
              $sdata['exception'] = "Data cannot be Saved.";
              $this->session->set_userdata($sdata);
              redirect("researchs/add");
          }
      }
        $data = array();
        $data['title'] = 'Add Research Statement & Goals';
        $data['heading_msg'] =  'Add Research Statement & Goals';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('researchs/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id)
    {
      if ($_POST) {
         $id=  $this->input->post('id', true);

         $old_file=  $this->input->post('old_photo_location', true);
         $file="";
         $content_file=  $old_file;

         if (isset($_FILES['txt_photo_location']) && $_FILES['txt_photo_location']['name'] != '') {
           $content_file = $this->my_file_upload('txt_photo_location', 'Research_Statment');
           if ($content_file=='0') {
               $sdata['exception'] = "Image 1 doesn't upload." . $this->upload->display_errors();
               $this->session->set_userdata($sdata);
               redirect("researchs/edit/".$id);
           }
           $this->my_file_remove($old_file,  $content_file);
        }

         $old_file2=  $this->input->post('old_photo_location2', true);
         $file2="";
         $content_file2=  $old_file2;

          if (isset($_FILES['txt_photo_location2']) && $_FILES['txt_photo_location2']['name'] != '') {
            $content_file2 = $this->my_file_upload('txt_photo_location2', 'Research_Details');
            if ($content_file2=='0') {
                $sdata['exception'] = "Image 2 doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("researchs/edit/".$id);
            }
            $this->my_file_remove($old_file2,  $content_file2);
         }

         $old_file3=  $this->input->post('old_photo_location3', true);
         $file3="";
         $content_file3=  $old_file3;

          if (isset($_FILES['txt_photo_location3']) && $_FILES['txt_photo_location3']['name'] != '') {
            $content_file3 = $this->my_file_upload('txt_photo_location3', 'Research_Details_3');
            if ($content_file2=='0') {
                $sdata['exception'] = "Image 3 doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("researchs/edit/".$id);
            }
            $this->my_file_remove($old_file3,  $content_file3);
         }

         $allowedTags = '<iframe><p><strong><em><u><h1><h2><h3><h4><h5><h6><img><style>';
         $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a><b>';

        $description = strip_tags(stripslashes($this->input->post('description', true)), $allowedTags);

         //$description = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->input->post('description', true));
         //$description=$this->input->post('description', true);

         //$description = preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $description);
        //$description = preg_replace( '/style=(["\'])[^\1]*?\1/i', '', $description, -1 );
        // echo "<pre>";
         //print_r($_POST);
          //print_r($description);
        // die;
          $data = array();
          $data['id'] =$id;
          $data['title'] = $this->input->post('title', true);
          $data['description'] = $description;
          $data['show_feature_image'] = $this->input->post('show_feature_image', true);
          $data['order'] = $this->input->post('order', true);
          $data['text_row'] = $this->input->post('text_row', true);
          $data['image_row'] = $this->input->post('image_row', true);
          $data['file_location'] = $content_file;
          $data['file_location2'] = $content_file2;
          $data['file_location3'] = $content_file3;
          if ($this->Research->edit_researchs($data,$id)) {
              $sdata['message'] = "Data successfully updated";
              $this->session->set_userdata($sdata);
              redirect("researchs/index");
          } else {
              $sdata['exception'] = "Data cannot be Updated";
              $this->session->set_userdata($sdata);
              redirect("researchs/edit/".$id);
          }
      }
        $data = array();
        $data['title'] = 'Update Research Statement & Goals';
        $data['heading_msg'] =  'Update Research Statement & Goals';
        $data['is_show_button'] = "index";
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->Research->read_researchs($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('researchs/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function delete($id)
    {
       $data= $this->Research->read_researchs($id);
       if(empty($data))
       {
         $sdata['exception'] = "Invalid Id!Please try again";
         $this->session->set_userdata($sdata);
         redirect("researchs/index");
       }
       $photo= $data->file_location;
       $photo2= $data->file_location2;
       $photo3= $data->file_location3;
        if($this->Research->delete_researchs($id))
        {
          $this->my_file_remove($photo, $photo);
          $this->my_file_remove($photo2, $photo2);
          $this->my_file_remove($photo3, $photo3);
          $sdata['message'] = "Data successfully deleted";
        }else{
          $sdata['exception'] = "Data cannot be Deleted";
        }
        $this->session->set_userdata($sdata);
        redirect("researchs/index");
    }
    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name =  $type.'_'. time().'.' .end($ext);
        // print_r(rand());
        // die();
        $this->load->library('upload');
        $config = array(
                'upload_path' => "core_media/adminpanel/dist/img/content/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx|gif",
                'max_size' => "99999999999",
                'max_height' => "9000000",
                'max_width' => "9000000",
                'file_name' => $new_file_name
            );
        // if($type=="Slider")
        // {
        //   $config = array(
        //           'upload_path' => "core_media/adminpanel/dist/img/content/",
        //           'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|gif",
        //           'max_size' => "99999999999",
        //           'max_height' => "90000",
        //           'max_width' => "90000",
        //           'file_name' => $new_file_name
        //       );
        // }
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function my_file_remove($old_file, $new_file)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH."core_media/adminpanel/dist/img/content/".$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
    function updateMsgContentStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->Research->edit_researchs($data,$id);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-block btn-success btn-sm">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-block btn-danger btn-xs">Inactive</button></a>';
        }
    }
}
