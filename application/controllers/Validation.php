<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Validation extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
      $data = array();
      $cond = array();
      if($_POST){
        $reference_number = $this->input->post("reference_number");
        $registration_type = $this->input->post("registration_type");
        $status = $this->input->post("status");
        $name = $this->input->post("cname");

        $cond['reference_number'] = $reference_number;
        $cond['registration_type'] = $registration_type;
        $cond['status'] = $status;
        $cond['cname'] = $name;

        $sdata['reference_number'] = $reference_number;
        $sdata['registration_type'] = $registration_type;
        $sdata['status'] = $status;
        $sdata['cname'] = $name;
        $this->session->set_userdata($sdata);
      }else{
        $reference_number = $this->session->userdata('reference_number');
        $registration_type = $this->session->userdata('registration_type');
        $status = $this->session->userdata('status');
        $name = $this->session->userdata('cname');

        $cond['reference_number'] = $reference_number;
        $cond['registration_type'] = $registration_type;
        $cond['status'] = $status;
        $cond['cname'] = $name;
      }
      $data['title'] = 'Registration';
      $data['heading_msg'] = 'Registration';
    //  $this->load->library('pagination');
      $config['base_url'] = site_url('Registration_Form/index/');
      $config['per_page'] =20;
      $data['is_show_button'] = "add";
  //    $config['total_rows'] = count($this->Admin_login->get_all_registration_index_list(0, 0, $cond));
    //  $this->pagination->initialize($config);
    //  $data['registration_list'] = $this->Admin_login->get_all_registration_index_list(20, (int)$this->uri->segment(3), $cond);
    //  $data['counter'] = (int)$this->uri->segment(3);
    //  $data['category_list']=$this->Website->get_website_content_category();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_left', '', true);
      $data['maincontent'] = $this->load->view('validation/index', $data, true);
      $this->load->view('admin_logins/index_left', $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
          $msg="";
          if($this->Admin_login->checkifexist_spot_ticket_number($_POST['txt_ticktet_number']))
          {
            $msg="Ticket number '".$_POST['txt_ticktet_number']."' already exists.";
          }

         if(isset($_POST['rd_guest']))
         {
           for ($i=1; $i <=$_POST['txt_guest_number']; $i++) {
                 $data['guest_ticket_'.$i]=$_POST['txt_guest_ticket_'.$i];
                 if($this->Admin_login->checkifexist_spot_ticket_number($_POST['txt_guest_ticket_'.$i]))
                 {
                   $msg .="Guest Ticket number ".$i." '".$_POST['txt_guest_ticket_'.$i]."' already exists.";
                 }
           }
         }

        if(isset($_POST['rd_baby']))
         {
           for ($i=1; $i <=$_POST['txt_baby_number']; $i++) {
                 $data['baby_ticket_'.$i]=$_POST['txt_baby_ticket_'.$i];
                 if($this->Admin_login->checkifexist_spot_ticket_number($_POST['txt_baby_ticket_'.$i]))
                 {
                   $msg .="Baby Ticket number ".$i." '".$_POST['txt_baby_ticket_'.$i]."' already exists.";
                 }
           }
         }
          if($msg=="")
          {

            $data['name'] = $this->input->post('txt_name', true);
            $data['wpforms_0608'] = $this->input->post('rd_wpforms', true);
            $data['ticket_number'] = $this->input->post('txt_ticktet_number', true);
            $data['guest'] = $this->input->post('rd_guest', true);
            if(isset($_POST['rd_guest']))
            {
              $data['guest_number'] = $this->input->post('txt_guest_number', true);
            }

            $data['baby'] = $this->input->post('rd_baby', true);
            if(isset($_POST['rd_baby']))
            {
              $data['baby_number'] = $this->input->post('txt_baby_number', true);
            }
            $data['mobile'] = $this->input->post('txt_mobile', true);
            $data['size'] = $this->input->post('rd_size', true);
            $data['blood_group_id'] = $this->input->post('blood_group', true);
            $data['reference_number'] = $this->input->post('txt_reference', true);
            $data['payment_method'] = $this->input->post('rd_payment_mode', true);
            $data['payment_number'] = $this->input->post('txt_payment_mobile', true);
            $data['present_address'] = $this->input->post('txt_present_address', true);
            $data['permanent_address'] = $this->input->post('txt_permanent_address', true);
            $data['payment_person'] = $this->input->post('txt_payment_person', true);
            $data['registration_type'] = $this->input->post('rd_registration_type', true);
            $data['date'] = $this->input->post('txt_date', true);
            $data['submission_date'] = date("Y-m-d H:i:s");
            $data['status'] = 'A';
            // echo "<pre>";
            // print_r($data);
            // die;
            $this->db->insert('tbl_registration_from', $data);


            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
          }else{

               $sdata['exception'] = $msg;
               $this->session->set_userdata($sdata);
          }
          redirect('validation/add');
        }
        $data = array();
        $data['title'] = 'Add Registration';
        $data['heading_msg'] =  'Add Registration';
        $data['is_show_button'] = "index";
        $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('validation/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
          $p_id=$_POST['id'];
          $reg=  $this->db->query("SELECT * FROM `tbl_registration_from` WHERE id = '$p_id'")->row();
          $msg="";

          if($msg=="")
          {
            $data = array();
            $data['id'] = $_POST['id'];

            if($_POST['status']=='A')
            {
              $max_length=$this->get_max_number($reg->registration_type);
             $ticket_number=$this->generate_ticket_number($max_length,1);
             $max_length=$max_length+1;
             $data['ticket_number'] = $ticket_number;
             $total_guest=0;
             if($reg->guest=='1')
             {
               for ($i=1; $i <=$reg->guest_number ; $i++) {
                     $data['guest_ticket_'.$i]=$this->generate_ticket_number($max_length,$i);
               }
               $max_length=$max_length+$reg->guest_number;
             }

             if($reg->baby=='1')
             {
               for ($i=1; $i <=$reg->baby_number ; $i++) {
                     $data['baby_ticket_'.$i]=$this->generate_ticket_number($max_length,$i);
               }
               $max_length=$max_length+$reg->baby_number;
             }
             $data['self_max_number'] = $max_length;


            }
            // echo "<pre>";
            // print_r($data);
            // die;
            $data['status'] = $_POST['status'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_registration_from', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('Registration_Form/index');
          }else{
               $sdata['exception'] = $msg;
               $this->session->set_userdata($sdata);
               redirect('Registration_Form/edit/'. $_POST['id']);
          }
        }
        $data = array();
        $data['title'] = 'Update Registration From';
        $data['heading_msg'] = 'Update Registration From';
        $data['is_show_button'] = "index";
        $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
        $data['registration'] = $this->db->query("SELECT * FROM `tbl_registration_from` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('registration_form/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    function generate_ticket_number($max_length,$possition){
          $code = "06080";
          if($max_length<10)
          {
              $code .= "00";
          }elseif ($max_length<100) {
              $code .= "0";
          }
          $total=$max_length+$possition;
          $code .= $total;
          return $code;
    }
    function get_max_number($registration_type)
    {
        $count = $this->db->query("SELECT IFNULL(MAX(`self_max_number`),0) AS self_max_number FROM `tbl_registration_from` WHERE `registration_type`='$registration_type'")->row();
        if($registration_type=='Self')
        {
          if($count->self_max_number==0)
          {
            return 50;
          }
        }else{
          if($count->self_max_number==50)
          {
            return 350;
          }
        }

       return $count->self_max_number;
    }
    public function delete($id)
    {
        $this->db->delete('tbl_registration_from', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('Registration_Form/index');
    }

}
