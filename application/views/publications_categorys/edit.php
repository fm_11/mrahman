

<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>publications_categorys/edit/<?php echo $row->id; ?>" method="post" enctype="multipart/form-data">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Name&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="name" class="form-control nameValidate" id="name" placeholder="name..." value="<?php echo $row->name; ?>" maxlength="200">
         </div>
        </div>

		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Order No&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" placeholder="order no..." min="0" value="<?php echo $row->order_no; ?>">
         </div>
        </div>
		 </div>
     <div class="row" id="div_file_1">
       <div class="col-lg-4 col-md-4 col-sm-4">
         <span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/publication/<?php echo $row->file_location_1; ?>" width="50" height="50"></span>
       </div>
       <div class="col-lg-8 col-md-8 col-sm-8">
         <div class="form-group">
         <label for="customFile">Image 1</label>
           <div class="custom-file">
             <input type="file" class="custom-file-input" name="txt_photo_location1" id="customFile">
             <label class="custom-file-label" for="customFile">Choose file</label>
           </div>
         </div>
        </div>
     </div>

     <div class="row" id="div_file_2">
       <div class="col-lg-4 col-md-4 col-sm-4">
         <span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/publication/<?php echo $row->file_location_2; ?>" width="50" height="50"></span>
       </div>
       <div class="col-lg-8 col-md-8 col-sm-8">
         <div class="form-group">
         <label for="customFile">Image 2</label>
           <div class="custom-file">
             <input type="file" class="custom-file-input" name="txt_photo_location2" id="customFile">
             <label class="custom-file-label" for="customFile">Choose file</label>
           </div>
         </div>
        </div>
     </div>

     <div class="row" id="div_file_3">
       <div class="col-lg-4 col-md-4 col-sm-4">
         <span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/publication/<?php echo $row->file_location_3; ?>"width="50" height="50"></span>
       </div>
       <div class="col-lg-8 col-md-8 col-sm-8">
         <div class="form-group">
         <label for="customFile">Image 3</label>
           <div class="custom-file">
             <input type="file" class="custom-file-input" name="txt_photo_location3" id="customFile">
             <label class="custom-file-label" for="customFile">Choose file</label>
           </div>
         </div>
        </div>
     </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <input type="hidden" name="old_photo_location_1" value="<?php echo $row->file_location_1; ?>" />
      <input type="hidden" name="old_photo_location_2" value="<?php echo $row->file_location_2; ?>" />
      <input type="hidden" name="old_photo_location_3" value="<?php echo $row->file_location_3; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>publications_categorys">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
	<script>
	$(function () {
		$('#quickForm').validate({
			rules: {
				role_name: {
					maxlength: 200
				},
				role_description: {
					maxlength: 500
				},
			},
			messages: {
				role_name: {
					maxlength: "Maximum length should be 200 characters."
				},
				role_description: {
					maxlength: "Maximum length should be 500 characters."
				}
			},
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
	</script>
