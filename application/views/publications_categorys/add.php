

<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>publications_categorys/add" method="post" enctype="multipart/form-data">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Name&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="name" class="form-control nameValidate" id="name" placeholder="name..." maxlength="200">
         </div>
        </div>

		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Order No&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" placeholder="order no..." min="0">
         </div>
        </div>
		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
         <label for="customFile">Image 1&nbsp;</label>
           <div class="custom-file">
             <input type="file" class="custom-file-input" name="txt_photo_location1" id="customFile1">
             <label class="custom-file-label" for="customFile">Choose file</label>
           </div>
         </div>
        </div>
     </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
         <label for="customFile">Image 2&nbsp;</label>
           <div class="custom-file">
             <input type="file" class="custom-file-input" name="txt_photo_location2" id="customFile2">
             <label class="custom-file-label" for="customFile">Choose file</label>
           </div>
         </div>
        </div>
     </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
         <label for="customFile">Image 3&nbsp;</label>
           <div class="custom-file">
             <input type="file" class="custom-file-input" name="txt_photo_location3" id="customFile3">
             <label class="custom-file-label" for="customFile">Choose file</label>
           </div>
         </div>
        </div>
     </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      <a href="<?php echo base_url(); ?>events_category">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>
    </div>
  </form>
