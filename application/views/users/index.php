<script type="text/javascript">
    function msgStatusUpdate(id, status) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>users/updateMsgStatusEmployeeStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }

</script>


            <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th class="">SL.</th>
                <th class="">User Name</th>
                <!-- <th class="">Employee</th>
                <th class="">Employee ID</th>
                <th class="">Branch</th> -->
                <th class="">Role</th>
                <th class="">Status</th>
                <th class="text-center">Action</th>
              </tr>
              </thead>
              <tbody>
              <?php  $i = 0;
                    foreach ($user_list as $row):
                    $i++;?>
                    <tr>
                      <td class=""><?php echo $i; ?></td>
                      <td class=""><?php echo $row['user_name']; ?></td>
                      <!-- <td class=""><?php echo $row['employee_name']; ?></td>
                      <td class=""><?php echo $row['employee_code']; ?></td>
                      <td class=""><?php echo $row['branch']; ?></td> -->
                      <td class=""><?php echo $row['role_name']; ?></td>

                      <td id="status_sction_<?php echo $row['id']; ?>">
                            <?php
                            if ($row['is_active'] == 1) {
                                ?>
                                  <?php if($row['id']==$current_user_id) {
                                    echo '<span  class="badge badge-success">Active</span>';
                                  }else{?>
                                    <a title="Active" href="#"
                                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)">
                                       <button type="button" class="btn btn-block btn-success btn-sm">Active</button>
                                     </a>
                                     <?php
                                     }
                                     ?>
                            <?php
                            } else {
                                ?>
                                <?php if($row['id']==$current_user_id) {
                                  echo '<span class="badge badge-danger">Inactive</span>';
                                }else{?>
                                <a title="Inactive" href="#"
                                   onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)">
                                   <button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button>
                                 </a>
                                 <?php
                                 }
                                 ?>
                            <?php
                            }
                            ?>

                        </td>

                      <td>
                        <div class="input-group-prepend" style="margin: auto;width: 50%;">
                          <button type="button" style="padding: 0.20rem .80rem;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-edit"></i>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>users/edit/<?php echo $row['id']; ?>" title="edit">Edit</a>
                              <?php if($row['id']!=$current_user_id) {?>
                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>users/delete/<?php echo $row['id']; ?>" title="delete">Delete</a>
                            <?php
                            }
                            ?>
                          </div>
                        </div>
                      </td>
                    </tr>
                 <?php endforeach; ?>
              </tbody>
              <!-- <tfoot>
              <tr>
                <th>SL.</th>
                <th>Role Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </tfoot> -->
            </table>
