<script type="text/javascript">

	function getEmployeeCodeByEmployeeId(employee_id) {
		if(employee_id != ''){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != false){
						var employee_code = JSON.parse((xmlhttp.responseText));

						document.getElementById('user_name').value = employee_code;
					}else{
						document.getElementById('user_name').value = "";
					}
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>users/getEmployeeCodeByEmployeeId?employee_id=" + employee_id, true);
			xmlhttp.send();
		}else{
			document.getElementById('user_name').value = "";
		}
	}
  </script>

<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>users/add" method="post">
    <div class="card-body">
      <div class="row">
        <!-- <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1"> Employee&nbsp;<span class="required_label">*</span></label>
            <select class="form-control select2 ddlErrorMsg" name="employee_id" onchange="getEmployeeCodeByEmployeeId(this.value);" style="width:100%;">
              <option value="">-Please Select--</option>
              <?php
                 if (count($employees)) {
                     foreach ($employees as $list) {
                         ?>
                         <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                     <?php
                     }
                 }
                 ?>
            </select>
          </div>
         </div> -->
      </div>
     <div class="row">
		 <div class="col-lg-4 col-md-4 col-sm-12">
			 <div class="form-group">
				 <label for="exampleInputEmail1">Role&nbsp;<span class="required_label">*</span></label>
				 <select class="form-control select2 ddlErrorMsg" name="user_role" style="width:100%;">
					 <option value="">-Please Select--</option>
					 <?php
							if (count($user_roles)) {
									foreach ($user_roles as $list) {
											?>
											<option value="<?php echo $list['id']; ?>"><?php echo $list['role_name']; ?></option>
									<?php
									}
							}
							?>
				 </select>
				</div>
			</div>
       <div class="col-lg-4 col-md-4 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">User Name&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="user_name" class="form-control" id="user_name" placeholder="username..." maxlength="100">
         </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1">Password&nbsp;<span class="required_label">*</span></label>
            <input type="password" name="user_password" class="form-control" id="user_password" placeholder="password..." maxlength="50">
          </div>
         </div>
		 </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      <a href="<?php echo base_url(); ?>users">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
	<script>
	$(function () {
      $('#quickForm').validate({
        rules: {
          user_name: {
            maxlength: 50,
            minlength:3,
            required: true
          },
          user_password: {
           required: true,
           minlength: 6,
           maxlength: 20
         },
        },
        messages: {
          user_name: {
            maxlength: "Maximum length should be 50 characters.",
            minlength: "Minimum length should be 3 characters.",
            required: "Please enter a username"
          },
          user_password: {
            maxlength: "Maximum length should be 20 characters.",
            minlength: "Minimum length should be 6 characters.",
            required: "Please provide a password"
          }
        },
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });
	});
	</script>
