

<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>users/change_password" method="post">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Current Password&nbsp;<span class="required_label">*</span></label>
           <input type="password" name="current_pass" class="form-control" id="current_pass" placeholder="current password...">
         </div>
        </div>

		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">New Password&nbsp;<span class="required_label">*</span></label>
           <input type="password" name="new_pass" class="form-control" id="new_pass" placeholder="new password...">

         </div>
        </div>
		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Confirm Password&nbsp;<span class="required_label">*</span></label>
           <input type="password" name="confirm_pass" class="form-control" id="confirm_pass" placeholder="confirm password...">
         </div>
        </div>
		 </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Change Password</button>
      <a href="<?php echo base_url(); ?>users">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
	<script>
  $(function () {
      $('#quickForm').validate({
        rules: {
          current_pass: {
            maxlength: 20,
            minlength:6,
            required: true,

          },
          new_pass: {
           required: true,
           minlength: 6,
           maxlength: 20,
           //equalTo: "#current_pass"
         },
         confirm_pass: {
          required: true,
          minlength: 6,
          maxlength: 20,
          equalTo: "#new_pass"
        }
        },
        messages: {
          current_pass: {
            maxlength: "Maximum length should be 20 characters.",
            minlength: "Minimum length should be 6 characters.",
            required: "Please provide a current password"

          },
          new_pass: {
            maxlength: "Maximum length should be 20 characters.",
            minlength: "Minimum length should be 6 characters.",
            required: "Please provide a new password",
            //equalTo: "Sorry, the current password and a new password cannot be the same."
          },
          confirm_pass: {
            maxlength: "Maximum length should be 20 characters.",
            minlength: "Minimum length should be 6 characters.",
            required: "Please provide a confirm password",
            equalTo: "Sorry confirm password does not match"
          }
        },
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });
	});
	</script>
