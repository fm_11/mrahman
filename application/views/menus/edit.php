
     <script type="text/javascript">

     	function getEmployeeCodeByEmployeeId(employee_id) {
     		if(employee_id != ''){
     			if (window.XMLHttpRequest)
     			{
     				xmlhttp = new XMLHttpRequest();
     			}
     			else
     			{
     				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
     			}
     			xmlhttp.onreadystatechange = function()
     			{
     				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
     				{
     					if(xmlhttp.responseText != false){
     						var employee_code = JSON.parse((xmlhttp.responseText));

     						document.getElementById('user_name').value = employee_code;
     					}else{
     						document.getElementById('user_name').value = "";
     					}
     				}
     			}
     			xmlhttp.open("GET", "<?php echo base_url(); ?>users/getEmployeeCodeByEmployeeId?employee_id=" + employee_id, true);
     			xmlhttp.send();
     		}else{
     			document.getElementById('user_name').value = "";
     		}
     	}

      // if(document.getElementById("link_able").checked) {
      //     document.getElementById('link_able_Hidden').disabled = true;
      // }
       </script>

     <!-- form start id="quickForm"-->

       <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>menus/edit/<?php echo $row->id; ?>" method="post">
         <div class="card-body">
           <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12">
               <div class="form-group">
                 <label for="exampleInputEmail1"> Parent&nbsp;<span class="required_label">*</span></label>
                 <select class="form-control select2 ddlErrorMsg" name="menu_id" onchange="getContenMaxOrder(this.value);" disabled>
                   <option value="P">Parent Menu</option>
                   <?php
                      if (count($menus)) {
                          foreach ($menus as $list) {
                              ?>
                              <option value="<?php echo $list['id']; ?>" <?php if($list['id']==$row->parent_id){echo "selected";} ?>><?php echo $list['name']; ?></option>
                          <?php
                          }
                      }
                      ?>
                 </select>
               </div>
              </div>
           </div>
           <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12">
               <div class="form-group">
                 <label for="exampleInputEmail1">Name<span class="required_label">*</span></label>
                 <input type="text" name="name" class="form-control nameValidate" id="name" value="<?php echo $row->name ?>" placeholder="Menu name...">
               </div>
              </div>
      		 </div>
           <div class="row">
             <div class="col-lg-6 col-md-6 col-sm-12">
               <div class="form-group">
                 <label for="exampleInputEmail1">Order<span class="required_label">*</span></label>
                 <label style="float: right;color: red;" id="order_label"> </label>
                 <input type="number" name="order" class="form-control numberfieldErrorMsg" id="order" value="<?php echo $row->order ?>" placeholder="Order...">
               </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group">
                  <label for="exampleInputEmail1"> Is Linkable&nbsp;<span class="required_label">*</span></label>
                  <select class="form-control select2" name="link_able" id="link_able" style="width:100%;">
                    <option value="0" <?php if('0'==$row->link_able){echo "selected";} ?>>No</option>
                    <option value="1" <?php if('1'==$row->link_able){echo "selected";} ?>>Yes</option>

                  </select>
                </div>

               </div>
      		 </div>

           <div class="row" >
             <div class="col-lg-6 col-md-6 col-sm-12">
               <div class="form-group">
                 <label for="exampleInputEmail1"> Is URL&nbsp;<span class="required_label">*</span></label>
                 <select class="form-control select2" name="is_url" id="is_url" style="width:100%;">
                   <option value="0" <?php if('0'==$row->is_url){echo "selected";} ?>>No</option>
                   <option value="1" <?php if('1'==$row->is_url){echo "selected";} ?>>Yes</option>

                 </select>
               </div>
              </div>
             <div class="col-lg-6 col-md-6 col-sm-12" id="div_url_link">
                <div class="form-group">
                  <label for="exampleInputEmail1">URL&nbsp;<span class="required_label">*</span></label>
                  <input type="text" name="url_link" class="form-control" id="url_link" value="<?php echo $row->url_link ?>" placeholder="url..." maxlength="300">
                </div>
               </div>
           </div>
         </div>


         <!-- /.card-body -->
         <div class="card-footer">
           <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
           <input type="hidden" name="is_active" value="<?php echo $row->is_active; ?>" />
           <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
           <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
           <a href="<?php echo base_url(); ?>users">
           <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
          </a>

         </div>
       </form>

     	<!-- Page specific script -->
     	<script>
      $(document).ready(function(){
        div_hide_show();
        $("#is_url").change(function(){
           div_hide_show();
         });
      });

      function div_hide_show()
      {
        var content_type=$('#is_url').val();
        if(content_type=='1')
        {
          $('#div_url_link').show();
          $('#url_link').addClass("nameValidate");
        }else{
           $('#div_url_link').hide();
           $('#url_link').removeClass("nameValidate");
           $('#url_link').val('');

        }
      }

     	$(function () {
           $('#quickForm').validate({
             rules: {
               user_name: {
                 maxlength: 50,
                 minlength:3,
                 required: true
               },
               user_password: {
                required: true,
                minlength: 6,
                maxlength: 20
              },
             },
             messages: {
               user_name: {
                 maxlength: "Maximum length should be 50 characters.",
                 minlength: "Minimum length should be 3 characters.",
                 required: "Please enter a username"
               },
               user_password: {
                 maxlength: "Maximum length should be 20 characters.",
                 minlength: "Minimum length should be 6 characters.",
                 required: "Please provide a password"
               }
             },
             errorPlacement: function (error, element) {
               error.addClass('invalid-feedback');
               element.closest('.form-group').append(error);
             },
             highlight: function (element, errorClass, validClass) {
               $(element).addClass('is-invalid');
             },
             unhighlight: function (element, errorClass, validClass) {
               $(element).removeClass('is-invalid');
             }
           });
     	});
     	</script>
