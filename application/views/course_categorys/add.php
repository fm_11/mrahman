

<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>course_categorys/add" method="post">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Name&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="name" class="form-control nameValidate" id="name" placeholder="name..." maxlength="200">
         </div>
        </div>

		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Order No&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" placeholder="order no..." min="0">
         </div>
        </div>
		 </div>
     <input type="hidden" name="is_active" value="1" />
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      <a href="<?php echo base_url(); ?>events_category">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>
    </div>
  </form>
