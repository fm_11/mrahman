<script type="text/javascript">
    function msgStatusUpdate(id, status) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>teachings/updateMsgContentStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }

</script>
            <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th class="">SL.</th>
                <th class="">Head Title</th>
                <th class="">Title</th>
                <!-- <th class="">Description</th> -->
                <th class="">Type</th>
                <th class="">Order</th>
                <th class="">Status</th>
                <th class="text-center">Action</th>
              </tr>
              </thead>
              <tbody>
              <?php  $i = 0;
                    foreach ($teachings_list as $row):
                    $i++;?>
                    <tr>
                      <td class=""><?php echo $i; ?></td>
                      <td class=""><?php echo $row['head_title']; ?></td>
                      <td class=""><?php echo $row['title']; ?></td>
                      <!-- <td class=""><?php echo $row['description']; ?></td> -->
                      <td class=""><?php echo ($row['type'] == 'NC')? 'New Course' : 'Old Course'; ?></td>
                      <td class=""><?php echo $row['order_no']; ?></td>
                      <td class="" id="status_sction_<?php echo $row['id']; ?>">
                        <?php
                        if ($row['is_active'] == 1) {?>
                            <a title="Active" href="#"
                               onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)">
                               <button type="button" class="btn btn-block btn-success btn-sm">Active</button>
                             </a>
                        <?php } else {  ?>
                            <a title="Inactive" href="#"
                               onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)">
                               <button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button>
                             </a>
                        <?php  } ?>
                        </td>
                      <td>
                        <div class="input-group-prepend" style="margin: auto;width: 50%;">
                          <button type="button" style="padding: 0.20rem .80rem;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-edit"></i>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>teachings/edit/<?php echo $row['id']; ?>" title="edit">Edit</a>
                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>teachings/delete/<?php echo $row['id']; ?>" title="delete">Delete</a>
                          </div>
                        </div>
                      </td>
                    </tr>
                 <?php endforeach; ?>
              </tbody>
              <!-- <tfoot>
              <tr>
                <th>SL.</th>
                <th>Role Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </tfoot> -->
            </table>
