
<script type="text/javascript">

	function getContenMaxOrder(menu_id) {
		if(menu_id != ''){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
          debugger;
					if(xmlhttp.responseText != false){
            debugger;
						var max_order = JSON.parse((xmlhttp.responseText));

						document.getElementById('order_label').innerHTML ="Max Order No. "+ max_order;
					}else{
						document.getElementById('order_label').innerHTML = "";
					}
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>content_paragraph/getContenMaxOrder?menu_id=" + menu_id, true);
			xmlhttp.send();
		}else{
			document.getElementById('order_label').innerHTML = "";
		}
	}


  </script>
<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>content_paragraph/add" method="post"  enctype="multipart/form-data">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1"> Menu&nbsp;<span class="required_label">*</span></label>
            <select class="form-control select2 ddlErrorMsg" name="menu_id" onchange="getContenMaxOrder(this.value);" style="width:100%;">
              <option value="">-Please Select--</option>
              <?php
                 if (count($menus)) {
                     foreach ($menus as $list) {
                         ?>
                         <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                     <?php
                     }
                 }
                 ?>
            </select>
          </div>
         </div>
 		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Title</label>
           <input type="text" name="title" class="form-control" id="title" placeholder="title..." maxlength="200">
         </div>
        </div>
		 </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Order No&nbsp;<span class="required_label">*</span></label>

          <label style="float: right;color: red;" id="order_label"> </label>
          <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" placeholder="order no..." min="0" max="100">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Content Type&nbsp;<span class="required_label">*</span></label>
           <select class="form-control select2 ddlErrorMsg" name="content_type" id="content_type" style="width:100%;">
             <option value="">-Please Select--</option>
             <!-- <option value="L">List</option> -->
             <option value="P">Paragraph</option>
             <option value="Y">Youtube Link</option>
           </select>
         </div>
        </div>
    </div>

    <div class="row" id="div_file">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
        <label for="customFile">File</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
					<span class="required_label">Height: 667px, Width: 1000px</span>
        </div>
       </div>
			 <div class="col-lg-6 col-md-6 col-sm-12">
				 <div class="form-group">
					 <label for="exampleInputEmail1"> Image Possition&nbsp;<span class="required_label">*</span></label>
					 <div class="custom-control custom-radio">
             <input class="custom-control-input" type="radio" id="customRadio1" value="L" name="photo_possition">
             <label for="customRadio1" class="custom-control-label">Left</label>
           </div>
					 <div class="custom-control custom-radio">
             <input class="custom-control-input" type="radio" id="customRadio2" value="R" name="photo_possition" checked>
             <label for="customRadio2" class="custom-control-label">Right</label>
           </div>
					 <div class="custom-control custom-radio">
             <input class="custom-control-input" type="radio" id="customRadio3" value="U" name="photo_possition" >
             <label for="customRadio3" class="custom-control-label">Above</label>
           </div>
					 <div class="custom-control custom-radio">
             <input class="custom-control-input" type="radio" id="customRadio4" value="D" name="photo_possition">
             <label for="customRadio4" class="custom-control-label">Below</label>
           </div>
				 </div>
				</div>
    </div>
    <div class="row" id="div_youtube">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Youtube Code&nbsp;<span class="required_label">*</span></label>
					<input type="text" name="youtube_link" class="form-control" id="youtube_link"  value="" placeholder="youtube code...">

        </div>
       </div>

			 <div class="col-lg-6 col-md-6 col-sm-12">
				 <div class="form-group">
					 <label for="exampleInputEmail1">Youtube Width&nbsp;<span class="required_label">*</span></label>
					 <input type="number" name="width" class="form-control" id="width" min="0" value="" placeholder="width...">
				 </div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="form-group">
						<label for="exampleInputEmail1">Youtube Height&nbsp;<span class="required_label">*</span></label>
						<input type="number" name="height" class="form-control" id="height"  min="0"  value="" placeholder="height...">
					</div>
				 </div>
    </div>

    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Message&nbsp;<span class="required_label">*</span></label>
           <textarea  name="message"  id="summernote" placeholder="message ..." ></textarea>
         </div>
        </div>
    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      <a href="<?php echo base_url(); ?>content_paragraph">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>
	<!-- Page specific script -->
	<script>

  $(document).ready(function(){
    div_hide_show();
    $("#content_type").change(function(){
       div_hide_show();
     });
  });

  function div_hide_show()
  {
    var content_type=$('#content_type').val();
    if(content_type=='Y')
    {
      $('#customFile').val('');
      $('#div_file').hide();

      $('#div_youtube').show();
      $('#youtube_link').addClass("embadedNameValidate");
			$('#width').addClass("numberfieldErrorMsg");
			$('#height').addClass("numberfieldErrorMsg");
    }
		else{
       $('#youtube_link').removeClass("embadedNameValidate");
			 $('#width').removeClass("numberfieldErrorMsg");
 		  	$('#height').removeClass("numberfieldErrorMsg");
       $('#youtube_link').val('');
			 $('#div_youtube').hide();

        $('#div_file').show();

    }
  }
	</script>
