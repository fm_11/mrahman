<script type="text/javascript">
	function getContenMaxOrder(menu_id) {
		if(menu_id != ''){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
          debugger;
					if(xmlhttp.responseText != false){
            debugger;
						var max_order = JSON.parse((xmlhttp.responseText));

						document.getElementById('order_label').innerHTML ="Max Order No. "+ max_order;
					}else{
						document.getElementById('order_label').innerHTML = "";
					}
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>content_paragraph/getContenMaxOrder?menu_id=" + menu_id, true);
			xmlhttp.send();
		}else{
			document.getElementById('order_label').innerHTML = "";
		}
	}
  </script>
<style>
.mailbox-attachments li {
    border: 1px solid #eee;
    /* float: left; */
    margin-bottom: 10px;
    margin-right: 10px;
    /* width: 200px; */
    width: 30%;
    margin: auto;
}
</style>


<!-- /.card-body -->
   <?php if(!empty($row->photo_location) &&  $row->photo_location!=''){?>
          <?php
              $ext = explode('.',$row->photo_location);
              $end_ext=end($ext);
           ?>
            <div class="card-footer bg-white">
              <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
							 <?php if($end_ext=='pdf'){?>
                <li>
                  <span class="mailbox-attachment-icon"><i class="far fa-file-pdf"></i></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"> <?php echo $row->photo_location; ?></a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.00 MB</span>
                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>
							<?php }elseif ($end_ext=='csv' || $end_ext=='doc' || $end_ext=='docx') { ?>
                <li>
                  <span class="mailbox-attachment-icon"><i class="far fa-file-word"></i></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fas fa-paperclip"></i> <?php echo $row->photo_location; ?></a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.00 MB</span>
                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>
								<?php }else { ?>
                <li>
                  <span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $row->photo_location; ?>" alt="Attachment"></span>

                  <div class="mailbox-attachment-info">
                    <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $row->photo_location; ?>" class="mailbox-attachment-name"> <?php echo $row->photo_location; ?></a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.00 MB</span>
                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>
                 <?php } ?>
              </ul>
            </div>
					  <?php } ?>
            <!-- /.card-footer -->
  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>content_paragraph/edit/<?php echo $row->id; ?>" method="post"  enctype="multipart/form-data">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1"> Menu&nbsp;<span class="required_label">*</span></label>
            <select class="form-control select2 ddlErrorMsg" name="menu_id" onchange="getContenMaxOrder(this.value);" style="width:100%;">
              <option value="">-Please Select--</option>
              <?php
                 if (count($menus)) {
                     foreach ($menus as $list) {
                         ?>
                         <option value="<?php echo $list['id']; ?>" <?php if($list['id']==$row->menu_id){echo "selected";} ?>><?php echo $list['name']; ?></option>
                     <?php
                     }
                 }
                 ?>
            </select>
          </div>
         </div>
 		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Title</label>
           <input type="text" name="title" class="form-control" id="title" placeholder="title..." value="<?php echo $row->title; ?>" maxlength="200">
         </div>
        </div>
		 </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Order No&nbsp;<span class="required_label">*</span></label>

          <label style="float: right;color: red;" id="order_label"> </label>
          <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" value="<?php echo $row->order_no; ?>" placeholder="order no..." min="0" max="100">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Content Type&nbsp;<span class="required_label">*</span></label>
           <select class="form-control select2 ddlErrorMsg" name="content_type" id="content_type" style="width:100%;">
             <option value="">-Please Select--</option>
             <!-- <option value="L" <?php if('L'==$row->content_type){echo "selected";} ?>>List</option> -->
             <option value="P" <?php if('P'==$row->content_type){echo "selected";} ?>>Paragraph</option>
             <option value="Y" <?php if('Y'==$row->content_type){echo "selected";} ?>>Youtube Link</option>
           </select>
         </div>
        </div>
    </div>


    <div class="row" id="div_file">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
        <label for="customFile">File</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
        </div>
					<span class="required_label">Height: 667px, Width: 1000px</span>
       </div>
			 <div class="col-lg-6 col-md-6 col-sm-12">
				 <div class="form-group">
					 <label for="exampleInputEmail1"> Image Possition&nbsp;<span class="required_label">*</span></label>
					 <div class="custom-control custom-radio">
             <input class="custom-control-input" type="radio" id="customRadio1" value="L" name="photo_possition" <?php if($row->photo_possition=='L'){echo "checked";} ?>>
             <label for="customRadio1" class="custom-control-label">Left</label>
           </div>
					 <div class="custom-control custom-radio">
             <input class="custom-control-input" type="radio" id="customRadio2" value="R" name="photo_possition" <?php if($row->photo_possition=='R'){echo "checked";} ?> >
             <label for="customRadio2" class="custom-control-label">Right</label>
           </div>
					 <div class="custom-control custom-radio">
             <input class="custom-control-input" type="radio" id="customRadio3" value="U" name="photo_possition" <?php if($row->photo_possition=='U'){echo "checked";} ?>>
             <label for="customRadio3" class="custom-control-label">Above</label>
           </div>
					 <div class="custom-control custom-radio">
             <input class="custom-control-input" type="radio" id="customRadio4" value="D" name="photo_possition" <?php if($row->photo_possition=='D'){echo "checked";} ?>>
             <label for="customRadio4" class="custom-control-label">Below</label>
           </div>
				 </div>
				</div>
    </div>
    <div class="row" id="div_youtube">
  
			 <div class="col-lg-12 col-md-12 col-sm-12">
			 	<div class="form-group">
			 		<label for="exampleInputEmail1"> Youtube Code&nbsp;<span class="required_label">*</span></label>
			 		<input type="text" name="youtube_link" class="form-control" id="youtube_link"  value="<?php echo $row->youtube_link; ?>" placeholder="youtube code...">

			 	</div>
			  </div>
			 <div class="col-lg-6 col-md-6 col-sm-12">
				 <div class="form-group">
					 <label for="exampleInputEmail1">Youtube Width&nbsp;<span class="required_label">*</span></label>
					 <input type="number" name="width" class="form-control" id="width" min="0" value="<?php echo $row->width; ?>" placeholder="width...">
				 </div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="form-group">
						<label for="exampleInputEmail1">Youtube Height&nbsp;<span class="required_label">*</span></label>
						<input type="number" name="height" class="form-control" id="height"  min="0"  value="<?php echo $row->height; ?>" placeholder="height...">
					</div>
				 </div>
    </div>
    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Message&nbsp;<span class="required_label">*</span></label>
           <textarea  name="message"  id="summernote" placeholder="message ..." ><?php echo $row->message; ?></textarea>
         </div>
        </div>
    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="old_photo_location" value="<?php echo $row->photo_location; ?>" />
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>content_paragraph">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
  <script>

  $(document).ready(function(){
    div_hide_show();
    $("#content_type").change(function(){
       div_hide_show();
     });
  });

	function div_hide_show()
  {
    var content_type=$('#content_type').val();
		if(content_type=='Y')
		{
			$('#customFile').val('');
			$('#div_file').hide();

			$('#div_youtube').show();
			$('#youtube_link').addClass("embadedNameValidate");
			$('#width').addClass("numberfieldErrorMsg");
			$('#height').addClass("numberfieldErrorMsg");
		}
		else{
			 $('#youtube_link').removeClass("embadedNameValidate");
			 $('#width').removeClass("numberfieldErrorMsg");
				$('#height').removeClass("numberfieldErrorMsg");
			 $('#youtube_link').val('');
			 $('#div_youtube').hide();

				$('#div_file').show();

		}
  }
  </script>
