
            <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th class="">SL.</th>
                <th class="">Name</th>
                <th class="">Mobile</th>
                <th class="">Email</th>
                <th class="">Subject : Message</th>
                <th class="">Date</th>
                <th class="text-center">Action</th>
              </tr>
              </thead>
              <tbody>
              <?php  $i = 0;
                    foreach ($contacts as $row):
                    $i++;?>
                    <tr>
                      <td class=""><?php echo $i; ?></td>
                      <td class=""><?php echo $row['name']; ?></td>
                      <td class=""><?php echo $row['phone']; ?></td>
                      <td class=""><?php echo $row['email']; ?></td>
                      <td class=""><b><?php echo $row['subject']; ?> : </b><?php echo $row['message']; ?></td>
                      <td class=""><?php echo $row['date']; ?></td>
                      <td>
                        <div class="input-group-prepend" style="margin: auto;width: 50%;">
                          <button type="button" style="padding: 0.20rem .80rem;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-edit"></i>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>contacts/delete/<?php echo $row['id']; ?>" title="delete">Delete</a>
                          </div>
                        </div>
                      </td>
                    </tr>
                 <?php endforeach; ?>
              </tbody>
            </table>
