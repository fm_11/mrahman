
<style>
.addnew {
        font-size: 20px;
        color: #3c8dbc;
        cursor: pointer;
    }
  .noBorder {
    border-left: 2px solid #ffffff !important;
    border-right: 2px solid #ffffff !important;
    border-bottom: 2px solid #ffffff !important;
}

</style>


  <div class="card-body table-responsive p-0" style="height: 400px;">
      <table class="table table-head-fixed text-nowrap">
        <thead>
          <tr>
            <th>SL.</th>
            <th>Photo</th>
            <th>Description</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        <?php $i=1; foreach ($row as $rows): ?>
         <tr  id="file_gallery_row_<?php echo $rows['id'];?>">
            <td> <?php echo $i++;?></td>
            <td>
              <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                  <div class="text-center">
                    <img style="width: 50% !important;" class="profile-user-img img-fluid"
                         src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/gallery/<?php echo $rows['file_path']; ?>"
                         alt="User profile picture">
                  </div>
                  <h3 class="profile-username text-center"><?php echo $rows['title']; ?></h3>
                </div>

              </div>
            </td>
            <td><span class="tag tag-success"><?php echo $rows['description']; ?></span></td>
            <td>
              <a  target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/gallery/<?php echo $rows['file_path']; ?>">
                <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>

	<!-- Page specific script -->
  <script>

  $(document).ready(function(){
    // ADD MORE
       $(".addnew").click(function () {
           var lastRow = $('#table_id tr:last').prev();
           var lastRowNo = lastRow.find('.hfLast').val();

           var finalthml = GET_NEW_ROW(lastRowNo);
           $('#table_id tr:last').prev().after(finalthml);
       });
  });
  $(document).on('click', '.delete', function () {
      var row = $(this).closest("tr");
      row.remove();
      var i = 0;
      $(".txtfile").each(function () {
          var row = $(this).closest("tr");
          row.find('.countRow').text(i + 1);
          row.find('.txtfile').attr('name', 'txt_photo_location_' + i);
          row.find('.txtDescription').attr('name', 'description_' + i);
          row.find('.hfLast').val(i);

          i = i + 1;
      });
     $('#file_upload_num_of_row').val(i);
  });
  function GET_NEW_ROW(lastRowNo) {

        lastRowNo++;
       $('#file_upload_num_of_row').val((Number(lastRowNo)+1));
        var rowNo_td = "<tr><td><span class='countRow'>" + (lastRowNo + 1) + "</span></td>";
        var file_td = "<td><div class='custom-file'><input type='file' class='custom-file-input txtfile' required name='txt_photo_location_" + lastRowNo + "'><label class='custom-file-label' for='customFile'>Choose Photo</label></div></td>";
        var txtDescription = "<td><div class='form-group'><input type='text' name='description_" + lastRowNo + "' class='form-control txtDescription'  placeholder='description...'' maxlength='300'></div></td>";
        var del_td = "<td class='col-md-2 rightBorder' style='width: 40px;'><input class='hfLast' type='hidden' value=" + lastRowNo + " /><i class='fa fa-trash delete' style='font-size: 20px;cursor:pointer'></i></td></tr>";
        var final = rowNo_td  + file_td + txtDescription + del_td;
        return final;

    }

    function delete_file_row(id) {
      var result = confirm("Are you sure to delete?");
        if (result) {
          if (window.XMLHttpRequest)
          {
              xmlhttp = new XMLHttpRequest();
          }
          else
          {
              xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange = function()
          {
              if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
              {
                  document.getElementById("file_gallery_row_" + id).outerHTML = "";
              }
          }
          xmlhttp.open("GET", "<?php echo base_url(); ?>gallery/delete_file?id=" + id, true);
          xmlhttp.send();
        }
    }
  </script>
