
<style>
.expandable-body>td {
    padding: 0!important;
    width: 0%;
}
</style>
            <table id="example2" class="table table-bordered table-striped table table-hover">
              <thead>
              <tr>
                <th class="">SL.</th>
                <th class="">Title</th>
                <th class="">Date</th>
                <th class="">Photo</th>
                <th class="text-center">Action</th>
              </tr>
              </thead>
              <tbody>
              <?php  $i = 0;
                    foreach ($gallery_list as $row):
                    $i++;?>
                    <tr class="expandable-body">
                      <td class=""><?php echo $i; ?></td>
                      <td class=""><?php echo $row['title']; ?></td>
                      <td class=""><?php echo $row['date']; ?></td>
                      <td>
                        <div class="p-0">
                          <table class="table table-hover">
                            <tbody>
                              <tr data-widget="expandable-table" aria-expanded="false">
                                <td>
                                  <i class="fas fa-caret-right fa-fw"></i>
                                  All Photo
                                </td>
                              </tr>
                              <tr class="expandable-body">
                                <td>
                                  <div class="p-0">
                                    <table class="table table-hover">
                                      <tbody>
                                    <?php $j=1; foreach ($row['file_list'] as  $value): ?>
                                       <?php if(isset($value['file_path']) && $value['file_path']!=''){?>
                                        <tr>
                                          <td><?php echo $j;?></td>
                                          <td><?php echo $value['description'];?></td>
                                          <td>
                                          <a  target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/gallery/<?php echo $value['file_path']; ?>">
                                            <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                                          </a></td>
                                        </tr>
                                      <?php }?>
                                    <?php $j++;?>
                                 <?php endforeach; ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </td>
                              </tr>

                            </tbody>
                          </table>
                        </div>
                      </td>
                      <td>
                        <div class="input-group-prepend" style="margin: auto;width: 50%;">
                          <button type="button" style="padding: 0.20rem .80rem;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-edit"></i>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>gallery/edit/<?php echo $row['reference_id']; ?>" title="edit">Edit</a>
                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>gallery/delete/<?php echo $row['reference_id']; ?>" title="delete">Delete</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>gallery/view/<?php echo $row['reference_id']; ?>" title="edit">View All</a>
                          </div>
                        </div>
                      </td>
                    </tr>
                 <?php endforeach; ?>
              </tbody>

            </table>
