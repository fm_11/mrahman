<style>
.addnew {
        font-size: 20px;
        color: #3c8dbc;
        cursor: pointer;
    }
  .noBorder {
    border-left: 2px solid #ffffff !important;
    border-right: 2px solid #ffffff !important;
    border-bottom: 2px solid #ffffff !important;
}

</style>
<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>gallery/add" method="post"  enctype="multipart/form-data">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
           <textarea name="title" class="form-control nameValidate" placeholder="title..."></textarea>
           <!-- <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." maxlength="300"> -->
         </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1">Date</label>
            <div class="input-group" id="reservationdate" data-target-input="nearest">
                  <input type="text" class="form-control datetimepicker-input" name="date" data-target="#reservationdate"/>
                  <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
              </div>
           </div>
         </div>
		 </div>

     <div class="row">
       <table class="table table-bordered" id="table_id">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Photo</th>
              <th>Description</th>
              <th style="width: 40px">Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td> <span class="countRow">1</span></td>
              <td>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input txtfile" required name="txt_photo_location_0">
                    <label class="custom-file-label" for="customFile">Choose Photo</label>
                  </div>
                  	<span class="required_label">Height: 638px, Width: 972px </span>
              </td>
              <td>
                <div class="form-group">
                  <input type="text" name="description_0" class="form-control txtDescription"  placeholder="description..." maxlength="300">
                </div>
              </td>
              <td class="col-md-2 rightBorder" style="width: 40px;">
                  <input class="hfLast" type="hidden" value="0" />

                      <!-- <i class="fa fa-trash delete" style="font-size: 20px; cursor:pointer"></i> -->

              </td>

            </tr>
            <tr style="">

               <td class="col-md-2 noBorder" style="width: 40px;"><i class="fa fa-plus-circle addnew" aria-hidden="true" title="ADD NEW ROW"></i></td>

               <td class="noBorder"></td>

           </tr>
          </tbody>
        </table>
     </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" id="file_upload_num_of_row" name="file_upload_num_of_row" value="1">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      <a href="<?php echo base_url(); ?>gallery">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>
	<!-- Page specific script -->
	<script>

  $(document).ready(function(){
    // ADD MORE
       $(".addnew").click(function () {
           var lastRow = $('#table_id tr:last').prev();
           var lastRowNo = lastRow.find('.hfLast').val();

           var finalthml = GET_NEW_ROW(lastRowNo);
           $('#table_id tr:last').prev().after(finalthml);
       });
  });
  $(document).on('click', '.delete', function () {
      var row = $(this).closest("tr");
      row.remove();
      var i = 0;
      $(".txtfile").each(function () {
          var row = $(this).closest("tr");
          row.find('.countRow').text(i + 1);
          row.find('.txtfile').attr('name', 'txt_photo_location_' + i);
          row.find('.txtDescription').attr('name', 'description_' + i);
          row.find('.hfLast').val(i);

          i = i + 1;
      });
     $('#file_upload_num_of_row').val(i);
  });
  function GET_NEW_ROW(lastRowNo) {

              lastRowNo++;
             $('#file_upload_num_of_row').val((Number(lastRowNo)+1));
              var rowNo_td = "<tr><td><span class='countRow'>" + (lastRowNo + 1) + "</span></td>";
              var file_td = "<td><div class='custom-file'><input type='file' class='custom-file-input txtfile' required name='txt_photo_location_" + lastRowNo + "'><label class='custom-file-label' for='customFile'>Choose Photo</label></div></td>";
              var txtDescription = "<td><div class='form-group'><input type='text' name='description_" + lastRowNo + "' class='form-control txtDescription'  placeholder='description...'' maxlength='300'></div></td>";
              var del_td = "<td class='col-md-2 rightBorder' style='width: 40px;'><input class='hfLast' type='hidden' value=" + lastRowNo + " /><i class='fa fa-trash delete' style='font-size: 20px;cursor:pointer'></i></td></tr>";
              var final = rowNo_td  + file_td + txtDescription + del_td;
              return final;

          }
	</script>
