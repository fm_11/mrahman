

<style>
.card.card-image {
    width: 100%;
    background-position: center;
    background-size: cover;
}
.card {
    font-weight: 400;
    border: 0;
    box-shadow: 0 2px 5px 0 rgb(0 0 0 / 16%), 0 2px 10px 0 rgb(0 0 0 / 12%);
}
.rgba-black-medium, .rgba-black-mediun:after {
    background-color: rgba(0,0,0,.45);
    height: 260px;
}
</style>
<!-- ======= Services Section ======= -->
   <section id="services" class="services">
     <div class="container" data-aos="fade-up">
       <div class="section-title">
         <h2>Research</h2>
       </div>

       <div class="row">
         <div class="offset-lg-3 offset-md-1 col-md-10 col-lg-8">
           <div class="row">

          <?php $i=100; foreach ($research_list as $key=>$value): ?>
            <?php if ($key==2): ?>
              <!-- <div class="col-md-3">

              </div>
              <div class="col-md-3">

              </div> -->
            <!-- <div style="background: #f1e9e9;padding-top: 10px;    margin-left: 10px;border-radius: 3px;" class="col-md-5 col-lg-11" data-aos="zoom-in" data-aos-delay="300">
              <div style="">
              <h4 style="font-size: 22px;">SUSTAINABLE MATERIALS AND MANUFACTURING</h4>
              <div class="text-center">
                <span class="research_h4" style="font-weight: bold;font-size: 20px;">Nature inspires us</span>
              </div>
            </div>
            </div> -->
            <div style="padding-top: 10px;padding-bottom: 10px;" class="<?php if($key%2==0){echo '';}?> col-md-12 col-lg-10 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="<?php echo $i=$i+100;;?>">
              <div class="card card-image" style="background: #f1e9e9;">
                  <div class="text-center align-items-center">
                    <h4 style="font-size: 22px;color: #4e4a4a;">SUSTAINABLE MATERIALS AND MANUFACTURING</h4>
                    <div class="text-center">
                      <span class="research_h4" style="font-weight: bold;font-size: 20px;">Nature inspires us</span>
                    </div>
                  </div>
              </div>
            </div>
            <?php endif; ?>
             <div style="padding-top: 10px;padding-bottom: 10px;" class="<?php if($key%2==0){echo '';}?> col-md-6 col-lg-5 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="<?php echo $i=$i+100;;?>">
               <div class="card card-image" style="background-image: url(<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo  $value['file_location']; ?>);">
                 <a href="<?php echo base_url(); ?>homes/research_details/<?php echo  $value['id']; ?>">
                   <div class="text-white text-center align-items-center rgba-black-medium py-5 px-4 rounded">
                        <h6 class="py-3 font-weight-bold" style="color: #fff;">
                            <strong><?php echo  $value['title']; ?></strong>

                        </h6>
                   </div>
                 </a>
               </div>
             </div>

         <?php endforeach; ?>
         </div>
       </div>
       <div class="col-md-4">
       </div>

       </div>
     </div>
   </section>
