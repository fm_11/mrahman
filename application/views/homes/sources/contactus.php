
<style>

.contact .ajax-form input, .contact .ajax-form textarea {
  border-radius: 4px;
  box-shadow: none;
  font-size: 14px;
}

.contact .ajax-form input:focus, .contact .ajax-form textarea:focus {
  border-color: #34b7a7;
}

.contact .ajax-form input {
  height: 44px;
}

.contact .ajax-form textarea {
  padding: 10px 12px;
}

.contact .ajax-form button[type="submit"] {
  background: #424242;
  border: 0;
  padding: 10px 30px 12px 30px;
  color: #fff;
  transition: 0.4s;
  border-radius: 50px;
}

.contact .ajax-form button[type="submit"]:hover {
  background: #6d6d6d;
}
.required_label{
  color: red;
}
</style>
<!-- ======= Contact Section ======= -->
   <section id="contact" class="contact">
     <div class="container" data-aos="fade-up">

       <div class="section-title">
         <h2>Contact</h2>

       </div>


       <div class="row mt-5">

         <div class="col-lg-6">
           <div class="info">
             <div class="address">
               <!-- <i class="fa fa-map-marker"></i>
               <h4>Location:</h4> -->
               <!-- <p></p> -->
               <?php echo $org_info->address; ?>
             </div>

             <!-- <div class="email">
               <i class="fa fa-envelope"></i>
               <h4>Email:</h4>
               <p><?php echo $org_info->email; ?></p>
             </div> -->

             <!-- <div class="phone">
               <i class="fa fa-phone"></i>
               <h4>Call:</h4>
               <p><?php echo $org_info->mobile; ?></p>
             </div> -->

           </div>

         </div>

         <div class="col-lg-6 mt-5 mt-lg-0">
           <iframe src="<?php echo $org_info->google_map; ?>"
           width="<?php if($org_info->width==100){ echo "100%";}echo $org_info->width.'px';?>" height="<?php echo $org_info->height; ?>" style="border:0;" allowfullscreen="" loading="lazy">
         </iframe>
           <!-- <?php echo $org_info->google_map; ?> -->
           <!-- <form action="<?php echo base_url(); ?>homes/contactus" method="post" class="row g-3 ajax-form">

             <div class="col-md-6">
               <label for="validationCustom01" class="form-label">Your Name<span class="required_label">*</span></label>
               <input type="text" class="form-control" name="name" id="validationCustom01" value="" maxlength="50" required>

             </div>
             <div class="col-md-6">
               <label for="validationCustom02" class="form-label">Your Email<span class="required_label">*</span></label>
               <input type="email" class="form-control" name="email" id="validationCustom02" value="" maxlength="50" required>

             </div>

             <div class="col-md-12">
               <label for="validationCustom03" class="form-label">Subject<span class="required_label">*</span></label>
               <input type="text" name="subject" class="form-control" id="validationCustom03" maxlength="200" required>

             </div>
             <div class="col-md-12">
               <label for="validationCustom04" class="form-label">Message<span class="required_label">*</span></label>
               <textarea class="form-control" name="message" id="validationTextarea"  maxlength="400" required></textarea>

             </div>

             <div class="col-md-12 ajax-message"></div>
             <div class="col-12">
               <button class="btn btn-primary" type="submit">Send Message</button>
             </div>
           </form> -->


            <!-- <form action="forms/contact.php" method="post" role="form" class="ajax-form">
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div>

              <div class="text-center"><button type="submit">Send Message</button></div>
            </form> -->
          </div>

       </div>

     </div>
   </section><!-- End Contact Section -->


   <script>
   // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
      'use strict'

      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.querySelectorAll('.needs-validation')

      // Loop over them and prevent submission
      Array.prototype.slice.call(forms)
        .forEach(function (form) {
          form.addEventListener('submit', function (event) {
            if (!form.checkValidity()) {
              event.preventDefault()
              event.stopPropagation()
            }

            form.classList.add('was-validated')
          }, false)
        })
    })();
   </script>
