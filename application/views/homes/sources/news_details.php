
  <section id="services" class="services">
        <div class="container" data-aos="fade-up">
          <div class="section-title">
            <h2><?php echo $title;?></h2>
          </div>

          <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
  						<div class="blog-details-post-wrapper">
  							<img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/newsblog/<?php echo $news_details[0]['photo_location']; ?>" alt="Image">
  							<div class="post-heading">
  								<h4><?php echo $news_details[0]['title']; ?></h4>
  								<span>Posted by <a href="#" class="tran3s p-color"><?php echo $news_details[0]['user_name']; ?></a>  at <?php echo date("F j, Y",strtotime($news_details[0]['date'])); ?></span>
  							</div> <!-- /.post-heading -->
  							<div style="text-align: justify">
                  <?php echo $news_details[0]['description']; ?>
                </div>


                <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="line-height: 32px;padding-top: 50px;">
                <h4>Share</h4>
                <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_twitter"></a>
                <a class="a2a_button_whatsapp"></a>
                <a class="a2a_button_copy_link"></a>
                <a class="a2a_button_email"></a>
                <a class="a2a_button_linkedin"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>

  							<!-- <div class="post-comment">
  								<h4>Post A Comment</h4>

  								<form action="#" method="post">
  									<div class="row">
  										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  											<span>Name*</span>
  											<div class="single-input">
  												<input type="text">
  											</div>
  										</div>
  										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  											<span>Email*</span>
  											<div class="single-input">
  												<input type="email">
  											</div>
  										</div>

  										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  											<span>Your Comment</span>
  											<div class="single-input">
  												<textarea></textarea>
  											</div>
  										</div>
  									</div>

  									<button class="tran3s p-color-bg">Post Comment</button>
  								</form>
  							</div>  -->
  						</div> <!-- /.blog-details-post-wrapper -->
					</div> <!-- /.col- -->


          <!-- ========================== Aside Bar ======================== -->
					<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
						<aside>
							<!-- <form action="#" class="sidebar-search-box">
								<input type="text" placeholder="Search...">
								<button class="p-color-bg"><i class="fa fa-search" aria-hidden="true"></i></button>
							</form> -->

							<div class="sidebar-recent-post">
								<h6>RECENT POST</h6>
             <?php foreach ($latest_news_info as $key): ?>
								<div class="recent-single-post clear-fix">
                  <div class="row">
                    <div class="col-md-4">
                      <a href="<?php echo base_url(); ?>homes/news_details/<?php echo $key['id']; ?>" class="tran3s">
                      <img style="" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/newsblog/<?php echo $key['photo_location']; ?>" alt="Image" class="float-left img-fluid img-thumbnail">
                      </a>
                    </div>
                    <div class="col-md-8">
                      <div class="post float-left" style="width:100% !important;padding-left: 0px !important;">
    										<a href="<?php echo base_url(); ?>homes/news_details/<?php echo $key['id']; ?>" class="tran3s"><?php echo $key['title']; ?></a>
    										<span><?php echo date("F j, Y",strtotime($key['date'])); ?></span>
    									</div>
                    </div>
                  </div>

									 <!-- /.post -->
								</div> <!-- /.recent-single-post -->
            <?php endforeach; ?>


							</div> <!-- /.sidebar-recent-post -->

							<div class="sidebar-tags">
								<h6>Tags</h6>
								<ul>
                  <?php foreach ($news_category as $key): ?>
                      <li><a href="<?php echo base_url(); ?>homes/news?cat_id=<?php echo $key['id']; ?>" class="tran3s"><?php echo $key['name']; ?></a></li>
                  <?php endforeach; ?>
								</ul>
							</div> <!-- /.sidebar-tags -->
						</aside>
					</div> <!-- /.col- -->


        </div>
      </div>
  </section>
