<style>
.hyper-link{
  color: #424242 !important;
}
.hyper-link:hover{
  color: #34b7a7 !important;
}
</style>
<!-- ======= Portfolio Section ======= -->
   <section id="portfolio" class="portfolio">
     <div class="container" data-aos="fade-up">

       <div class="section-title">
         <h2><?php echo $title;?></h2>
       </div>

       <div class="row" data-aos="fade-up" data-aos-delay="100">
         <div class="col-lg-12 d-flex justify-content-center">
           <ul id="portfolio-flters">
             <li data-filter="*" class="filter-active">All</li>
             <?php foreach ($news_and_blog_list as  $value): ?>
                  <li data-filter=".filter-app-<?php echo $value['year'];?>"><?php echo $value['year'];?></li>
             <?php endforeach; ?>

           </ul>
         </div>
       </div>

       <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
          <div class="row">
            <div class="col-md-8 ">
        <?php foreach ($news_and_blog_list as $row): ?>
             <div class="row">
                 <div class="col-md-8 portfolio-item filter-app-<?php echo $row['year'];?>">
                      <!-- <div class="col-md-12 text-left"> -->
                         <ul style="">
                                <h2><?php echo $row['year'];?></h2><br>
                                <?php  $i=count($row['news_list']); foreach ($row['news_list'] as  $value): ?>
                                      <li  style="padding-left: 30px;">
                                        <h6><?php echo $i--;?>.</h6>
                                        <div style="margin-top: -22px;padding-left: <?php if($i>8){echo '30';}else{ echo '20';}?>px;">
                                            <?php if ($value['is_link_url']=='1'){?>
                                               <a style="" class="hyper-link" target="_blank" href="<?php echo $value['url'];?>"><?php echo $value['title']; ?></a>
                                            <?php }else{?>
                                              <?php echo $value['title']; ?>
                                            <?php } ?>
                                        </div>
                                      </li><br>
                                <?php endforeach; ?>
                             <br><br>
        							</ul>
                    <!-- </div> -->
        					</div>
              </div>
        <?php endforeach; ?>
          </div>
        <div class="d-none d-sm-block d-sm-none d-md-block col-md-4">
          <?php foreach ($image_list as  $row) {?>
            <div style="padding-bottom: 5px;">
              <?php if($row['type']=='I'){?>
              <img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/images/<?php echo $row['url'];?>" alt="Image">
            <?php }else { ?>
               <object  data='https://www.youtube.com/embed/<?php echo $row['url'];?>?autoplay=0' width="100%"  height='275px'></object>
            <?php } ?>
            </div>
          <?php } ?>

        </div>
      </div>
       </div>
     </div>
   </section><!-- End Portfolio Section -->
