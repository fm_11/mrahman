<!-- ======= Hero Section ======= -->



<!-- <section class="d-flex align-items-center">
  <div class="container d-flex flex-column align-items-center" data-aos="zoom-in" data-aos-delay="100"> -->
<header class="header_section">

  <!-- This div is  intentionally blank. It creates the transparent black overlay over the video which you can modify in the CSS -->
  <div class="overlay"></div>

  <!-- The HTML5 video element that will create the background video on the header -->
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="<?php echo base_url();?>core_media/adminpanel/dist/img/orga/<?php echo $org_info->picture;?>" type="video/mp4">
        <!-- <source src="https://www.eng.yale.edu/faboratory/video/faboratory-bg-opt.mp4" type="video/mp4"> -->
  </video>

  <!-- The header content -->
  <!-- <div class="container h-100">
    <div class="d-flex h-100 text-center align-items-center">
      <div class="w-100 text-white">
        <h1 class="display-3">Video Header</h1>
        <p class="lead mb-0">Using HTML5 Video and Bootstrap</p>
      </div>
    </div>
  </div> -->
</header>
  <!-- </div>
</section> -->



<section id="hero" class="d-flex align-items-center">
  <div style="text-align: center;" class="container d-flex flex-column align-items-center" data-aos="zoom-in" data-aos-delay="100">
    <h1><?php echo $org_info->tin_number;?></h1>
    <h2><?php echo $org_info->facebook_address;?></h2>
    <a href="<?php echo base_url(); ?>homes/about_me" class="btn-about">About Me</a>
  </div>
</section>
