<style>
h4 {
    padding-bottom: 20px !important;
}
</style>

<article class="blog-details-page">
		<div class="container">
			<div class="offset-lg-2 col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div style="text-align: justify;" class="blog-details-post-wrapper">
        <?php foreach ($youtube_content as $value): ?>
          <div class="row">

            <div  style="padding-bottom: 20px !important;" class="col-md-12 left">
              <?php if(!empty($value['title'])){?>
							<h4 class=""><?php echo $value['title']; ?></h4>
              <?php } ?>
              <div class="text-justify">
                <?php echo $value['message']; ?>
              </div>
						</div>
						<div class="col-md-12 left text-center">
              <object  data='https://www.youtube.com/embed/<?php echo $value['youtube_link'];?>?autoplay=0' width="<?php if($value['width']==100){ echo "100%";}echo $value['width'].'px';?>"  height='<?php  echo $value['height']; ?>px'></object>
						</div>

          </div>
         <?php endforeach; ?>
        </div>
      </div>
    </div>
  </article>
