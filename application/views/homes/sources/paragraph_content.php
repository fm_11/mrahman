
<style>
h4 {
    padding-bottom: 20px !important;
}
</style>

<article class="blog-details-page">
				<div class="container">
					<div class="offset-lg-2 col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
						<div style="text-align: justify;" class="blog-details-post-wrapper">
  <?php foreach ($paragraph_content as $value): ?>
     <?php if(empty($value['photo_location'])){?>
              <div class="row">
            <?php if(!empty($value['title'])){?>
                <h4><?php echo $value['title']; ?></h4>
            <?php } ?>
                <div class="col-md-12 col-xs-12">
                  <p><?php echo $value['message']; ?></p>
                </div> <!-- /.col -->
              </div>

     <?php } elseif($value['photo_possition']=='U'){?>
       <div class="row">
         <?php if(!empty($value['title'])){?>
             <h4><?php echo $value['title']; ?></h4>
         <?php } ?>

           <?php if(!empty($value['photo_location'])){?>
           <?php
               $ext = explode('.',$value['photo_location']);
               $end_ext=end($ext);
            ?>
            <?php if($end_ext=='pdf'){?>
              <div style="text-align: center;" class="col-md-12 col-xs-12">
                <embed  style="width:100%;height: 700px;" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $value['photo_location']; ?>" type="application/pdf">
              </div> <!-- /.col- -->
            <?php }else{?>
              <div style="text-align: center;" class="offset-md-3 col-md-6 col-xs-12">
                   <img class="img-fluid img-thumbnail w-100 mb-4" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $value['photo_location']; ?>" alt="about image">
               </div> <!-- /.col- -->
            <?php } ?>
           <?php } ?>

         <div class="col-md-12 col-xs-12">
            <p><?php echo $value['message']; ?></p>
         </div> <!-- /.col -->

       </div>
 <?php } elseif($value['photo_possition']=='D'){?>
   <div class="row">
     <?php if(!empty($value['title'])){?>
         <h4><?php echo $value['title']; ?></h4>
     <?php } ?>

     <div class="col-md-12 col-xs-12">
        <p><?php echo $value['message']; ?></p>
     </div> <!-- /.col -->

       <?php if(!empty($value['photo_location'])){?>
       <?php
           $ext = explode('.',$value['photo_location']);
           $end_ext=end($ext);
        ?>
        <?php if($end_ext=='pdf'){?>
          <div style="text-align: center;" class="col-md-12 col-xs-12">
           <embed style="width:100%;height: 700px;" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $value['photo_location']; ?>" type="application/pdf"  >
          </div> <!-- /.col- -->
        <?php }else{?>
          <div style="text-align: center;" class="offset-md-3 col-md-6 col-xs-12">
               <img class="img-fluid img-thumbnail w-100 mb-4" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $value['photo_location']; ?>" alt="about image">
          </div> <!-- /.col- -->
        <?php } ?>
       <?php } ?>

   </div>
 <?php } elseif($value['photo_possition']=='R'){?>
      <div class="row">
        <?php if(!empty($value['title'])){?>
            <h4><?php echo $value['title']; ?></h4>
        <?php } ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <p><?php echo $value['message']; ?></p>
        </div>  <!-- /.col -->
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <?php if(!empty($value['photo_location'])){?>
          <?php
              $ext = explode('.',$value['photo_location']);
              $end_ext=end($ext);
           ?>
           <?php if($end_ext=='pdf'){?>
             <embed  style="width:100%;height: 400px;" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $value['photo_location']; ?>" type="application/pdf">
           <?php }else{?>

                  <img class="img-fluid img-thumbnail w-100 mb-4" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $value['photo_location']; ?>" alt="about image">

           <?php } ?>
          <?php } ?>
        </div> <!-- /.col- -->
      </div> <!-- /.row -->
<?php } else{?>
  <div class="row">
    <?php if(!empty($value['title'])){?>
        <h4><?php echo $value['title']; ?></h4>
    <?php } ?>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <?php if(!empty($value['photo_location'])){?>
      <?php
          $ext = explode('.',$value['photo_location']);
          $end_ext=end($ext);
       ?>
       <?php if($end_ext=='pdf'){?>
         <embed   style="width:100%;height: 400px;"  src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $value['photo_location']; ?>" type="application/pdf" >
       <?php }else{?>
              <img class="img-fluid img-thumbnail w-100 mb-4" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $value['photo_location']; ?>" alt="about image">
       <?php } ?>
      <?php } ?>
    </div> <!-- /.col- -->
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p><?php echo $value['message']; ?></p>
    </div> <!-- /.col -->

  </div>
<?php } ?>

   <?php endforeach; ?>




               <!-- /.row -->

						</div> <!-- /.blog-details-post-wrapper -->
					</div> <!-- /.col- -->

					<!-- ========================== Aside Bar ======================== -->

				</div> <!-- /.container -->
			</article>
