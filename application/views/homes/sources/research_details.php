
  <section id="services" class="services">
        <div class="container" data-aos="fade-up">
          <!-- <div class="section-title">
            <h2><?php echo $title;?></h2>
          </div> -->

          <div class="row">
            <div class="offset-lg-1 col-lg-10 col-md-12 p-fix">
  						<div class="blog-details-post-wrapper">
                <?php if ($content->show_feature_image=='1'): ?>
                  <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo  $content->file_location; ?>" alt="Image">
                <?php endif; ?>

  							<div class="post-heading">
  								<h4><?php echo $content->title; ?></h4>

  							</div> <!-- /.post-heading -->
  							<div style="text-align: justify">
                  <div class="row">

                      <?php if (!empty($content->file_location2)): ?>
                        <div class="col-md-<?php echo $content->text_row; ?>">
                          <?php echo $content->description; ?>
                        </div>
                        <div class="col-md-<?php echo $content->image_row; ?>">
                            <img class="img-fluid" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo  $content->file_location2; ?>" alt="Image">
                               <?php if (!empty($content->file_location3)): ?>
                                    <img style="padding-top: 20px;" class="img-fluid" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo  $content->file_location3; ?>" alt="Image">
                               <?php endif; ?>
                        </div>
                    <?php else: ?>
                      <div class="col-md-<?php echo $content->text_row; ?>">
                        <?php echo $content->description; ?>
                      </div>

                    <?php endif; ?>
                  </div>

                </div>


  						</div> <!-- /.blog-details-post-wrapper -->
					</div> <!-- /.col- -->


          <!-- ========================== Aside Bar ======================== -->
					<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">

					</div> <!-- /.col- -->


        </div>
      </div>
  </section>
