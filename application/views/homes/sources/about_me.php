<!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">
      <?php if (!empty($about->title)): ?>
        <div class="section-title">
          <h2><?php echo $about->title; ?></h2>
        </div>
      <?php endif; ?>


        <div class="row" style="padding-top: 10px;">
          <div class="offset-lg-1 col-lg-4 col-md-4 col-sm-12">
            <div class="about-img">
            <?php if (!empty($about->file_location)){?>
                <img style="max-width: 84%;" src="<?php echo base_url();?>core_media/adminpanel/dist/img/orga/<?php echo $about->file_location; ?>" class="img-fluid" alt="">
            <?php }else{?>

            <?php } ?>
            </div>

            <div class="about-contact" style="padding-bottom: 15px;margin-top: 50px;">
            <h6>  Social</h6>
            </div>
            <div class="header-social-links about-social-icon" style="padding-bottom: 30px;">
              <a target="_blank" href="<?php echo $org_info->twitter;  ?>" class="twitter"><i class="bi bi-twitter"></i></a>
              <a target="_blank" href="<?php echo $org_info->google_plus;  ?>" class="facebook">
              <!-- <img style="height:20px" src="https://alexeymakarin.github.io/assets/scholar-icon-29.jpg" /> -->
              <img style="margin-top: -5px;" src="<?php echo base_url();?>core_media/adminpanel/dist/img/icons8-google-scholar-20.png"/>

              </a>
              <!-- <a target="_blank" href="<?php echo $org_info->whatsapp;  ?>" class="instagram"><i class="bi bi-google"></i></a> -->
              <a  target="_blank" href="<?php echo $org_info->linkedin;  ?>" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
            </div>
            <div class="about-contact">
            <p>
              <?php echo $about->about_2; ?>
            </p>
          </div>
          </div>
          <div class="col-lg-6 col-md-8 col-sm-12 pt-4 pt-lg-0 content" style="text-align: justify;">
            <p>
              <?php echo $about->about_1; ?>
            </p>
          </div>

          <!-- <div class="offset-lg-1 col-lg-4 content" style="text-align: justify;margin-top: 10px;">
            <div style="padding-bottom: 15px;">
            <h6>  Social</h6>
            </div>
            <div class="header-social-links" style="padding-bottom: 30px;">
              <a target="_blank" href="<?php echo $org_info->twitter;  ?>" class="twitter"><i class="bi bi-twitter"></i></a>
              <a target="_blank" href="<?php echo $org_info->google_plus;  ?>" class="facebook">
              <img style="margin-top: -5px;" src="<?php echo base_url();?>core_media/adminpanel/dist/img/icons8-google-scholar-20.png"/>

              </a>
              <a  target="_blank" href="<?php echo $org_info->linkedin;  ?>" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
            </div>

            <p>
              <?php echo $about->about_2; ?>
            </p>
          </div> -->
        </div>

      </div>
    </section><!-- End About Section -->
