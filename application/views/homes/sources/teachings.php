<style>
h3{
  padding-bottom: 20px;
}
table{
  border: 1px solid #000 !important;
}

</style>
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">
      <?php if (count($new_course_list)>0) { ?>
        <div class="section-title">
          <h2><?php echo $new_course_list[0]['head_title']; ?></h2>
        </div>
      <?php } ?>
        <article class="blog-details-page">
        				<div class="container">
        					<div class="offset-lg-1 col-lg-10 col-md-10 col-sm-12 col-xs-12 p-fix">
        						<div style="text-align: justify;" class="blog-details-post-wrapper">
                    <?php foreach ($new_course_list as $value): ?>
                      <div class="row">
                        <?php if(!empty($value['title'])){?>
                        <h3><?php echo $value['title']; ?></h3>
                      <?php } ?>

                      <?php if(empty($value['file_location'])){?>
                        <div class="col-lg-12">
                          <?php echo $value['description']; ?>
                        </div>
                      <?php }else{ ?>

                        <div class="col-lg-8">
                          <?php echo $value['description']; ?>
                        </div>
                      <?php }?>

                      <?php if(!empty($value['file_location'])){?>
                        <div class="col-lg-4">

                          <?php
                              $ext = explode('.',$value['file_location']);
                              $end_ext=end($ext);
                           ?>
                           <?php if($end_ext=='pdf'){?>
                             <div class="mailbox-attachment-info" style="text-align: center;">
                               <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $value['file_location']; ?>" class="mailbox-attachment-name">
                                   <i style="font-size: 200px;" class="fas fa-file-pdf"></i>
                               </a>
                             </div>

                           <?php }elseif($end_ext=='doc'){?>
                             <div class="mailbox-attachment-info" style="text-align: center;">
                               <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $value['file_location']; ?>" class="mailbox-attachment-name">
                                   <i style="font-size: 200px;" class="fas fa-file-word"></i>
                               </a>
                             </div>

                           <?php }else{?>
                             <div class="mailbox-attachment-info" style="text-align: center;">
                                  <img class="img-fluid img-thumbnail w-100 mb-4" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $value['file_location']; ?>" alt="about image">
                             </div>
                             <?php } ?>



                        </div>
                      <?php } ?>
                      </div>
                    <?php endforeach; ?>

                    </div>

                  </div>
                </div>
            </div>
        </article>

      </div>







      <div class="container" data-aos="fade-up">
       <?php if (count($old_course_list)>0) { ?>
        <div class="section-title">
          <h2><?php echo $old_course_list[0]['head_title']; ?></h2>
        </div>
        <?php } ?>
        <article class="blog-details-page">
        				<div class="container">
        					<div class="offset-lg-1 col-lg-10 col-md-10 col-sm-12 col-xs-12 p-fix">
        						<div style="text-align: justify;" class="blog-details-post-wrapper">
                    <?php foreach ($old_course_list as $value): ?>
                      <div class="row">
                        <?php if(!empty($value['title'])){?>
                        <h3><?php echo $value['title']; ?></h3>
                      <?php } ?>

                      <?php if(empty($value['file_location'])){?>
                        <div class="col-lg-12">
                          <?php echo $value['description']; ?>
                        </div>
                      <?php }else{ ?>

                        <div class="col-lg-8">
                          <?php echo $value['description']; ?>
                        </div>
                      <?php }?>

                      <?php if(!empty($value['file_location'])){?>
                        <div class="col-lg-4">

                          <?php
                              $ext = explode('.',$value['file_location']);
                              $end_ext=end($ext);
                           ?>
                           <?php if($end_ext=='pdf'){?>
                             <div class="mailbox-attachment-info" style="text-align: center;">
                               <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $value['file_location']; ?>" class="mailbox-attachment-name">
                                   <i style="font-size: 200px;" class="fas fa-file-pdf"></i>
                               </a>
                             </div>

                           <?php }elseif($end_ext=='doc'){?>
                             <div class="mailbox-attachment-info" style="text-align: center;">
                               <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $value['file_location']; ?>" class="mailbox-attachment-name">
                                   <i style="font-size: 200px;" class="fas fa-file-word"></i>
                               </a>
                             </div>

                           <?php }else{?>
                             <div class="mailbox-attachment-info" style="text-align: center;">
                                  <img class="img-fluid img-thumbnail w-100 mb-4" src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $value['file_location']; ?>" alt="about image">
                             </div>
                             <?php } ?>



                        </div>
                      <?php } ?>
                      </div>
                    <?php endforeach; ?>

                    </div>

                  </div>
                </div>
            </div>
        </article>

      </div>

    </section>
