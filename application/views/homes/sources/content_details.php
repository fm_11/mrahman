
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title" style="padding-bottom: 0px !important;">
          <h2><?php echo $title; ?></h2>
        </div>

        <?php foreach ($content as $value): ?>
          <?php echo $value; ?>
        <?php endforeach; ?>

      </div>
    </section>
