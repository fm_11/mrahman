<!-- ======= Portfolio Section ======= -->
   <section id="portfolio" class="portfolio">
     <div class="container" data-aos="fade-up">

       <div class="section-title">
         <h2>PUBLICATIONS</h2>
       </div>

       <div class="row" data-aos="fade-up" data-aos-delay="100">
         <div class="col-lg-12 d-flex justify-content-center">
           <ul id="portfolio-flters">
             <li data-filter="*" class="filter-active">All</li>
             <?php foreach ($publication_list as  $value): ?>
                  <li data-filter=".filter-app-<?php echo $value['id'];?>"><?php echo $value['name'];?></li>
             <?php endforeach; ?>

           </ul>
         </div>
       </div>

       <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
          <div class="row">
            <div class="col-md-8 ">
        <?php foreach ($publication_list as $category): ?>
             <div class="row">
                 <div class="col-md-8 portfolio-item filter-app-<?php echo $category['id'];?>">
                      <!-- <div class="col-md-12 text-left"> -->
                         <ul style="">
                              <h2><?php echo $category['name'];?></h2>
                              <br>
                              <?php foreach ($category['year_list'] as $key => $year): ?>
                                <h4><?php echo $year['year'];?></h4><br>
                                    <?php $i=count($year[$year['year']]); foreach ($year[$year['year']] as  $value): ?>
                                          <li style="padding-left: 30px;">
                                            <h6><?php echo $i--;?>.</h6>
                                            <div style="margin-top: -22px;padding-left: <?php if($i>8){echo '30';}else{ echo '20';}?>px;">
                                              <?php echo $value['title'];?>
                                            </div>

                                          </li><br>
                                    <?php endforeach; ?>
                              <?php endforeach; ?>
                             <br><br>
        							</ul>
                    <!-- </div> -->
        					</div>
              </div>
        <?php endforeach; ?>
          </div>
        <div class="d-none d-sm-block d-sm-none d-md-block col-md-4">
          <?php foreach ($image_list as  $row) {?>
            <div style="padding-bottom: 5px;">
              <?php if($row['type']=='I'){?>
              <img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/images/<?php echo $row['url'];?>" alt="Image">
            <?php }else { ?>
               <object  data='https://www.youtube.com/embed/<?php echo $row['url'];?>?autoplay=0' width="100%"  height='275px'></object>
            <?php } ?>
            </div>
          <?php } ?>

        </div>
      </div>
       </div>
     </div>
   </section><!-- End Portfolio Section -->
