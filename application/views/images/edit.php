<script type="text/javascript">
	function getContenMaxOrder(type) {
		if(type != ''){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != false){

						var max_order = JSON.parse((xmlhttp.responseText));

						document.getElementById('order_label').innerHTML ="Max Order No. "+ max_order;
					}else{
						document.getElementById('order_label').innerHTML = "";
					}
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>images/getContenMaxOrder?type=" + type, true);
			xmlhttp.send();
		}else{
			document.getElementById('order_label').innerHTML = "";
		}
	}
  </script>
<style>
.mailbox-attachments li {
    border: 1px solid #eee;
    /* float: left; */
    margin-bottom: 10px;
    margin-right: 10px;
    /* width: 200px; */
    width: 30%;
    margin: auto;
}
</style>


<!-- /.card-body -->
   <?php if(!empty($row->url) &&  $row->url!='' && $row->type=='I'){?>
          <?php
              $ext = explode('.',$row->url);
              $end_ext=end($ext);
           ?>

					 <div class="card card-primary card-outline">
						 <div class="card-body box-profile">
							 <div class="text-center">
								 <img class="profile-user-img img-fluid"
											src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/images/<?php echo $row->url; ?>"
											alt="User profile picture">
							 </div>
						 </div>
						 <!-- /.card-body -->
					 </div>
					  <?php } ?>
            <!-- /.card-footer -->
  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>images/edit/<?php echo $row->id; ?>" method="post"  enctype="multipart/form-data">
    <div class="card-body">

			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="form-group">
						<label for="exampleInputEmail1"> Category&nbsp;<span class="required_label">*</span></label>
						<select class="form-control select2 ddlErrorMsg" name="category" id="category" style="width:100%;">
							<option value="">-- Please Select --</option>
							<option value="N" <?php if('N'==$row->category){echo "selected";} ?>>News</option>
							<option value="P" <?php if('P'==$row->category){echo "selected";} ?>>Publication</option>
						</select>
					</div>
				 </div>
				 <div class="col-lg-6 col-md-6 col-sm-12">
					 <div class="form-group">
						 <label for="exampleInputEmail1"> Type&nbsp;<span class="required_label">*</span></label>
						 <select class="form-control select2 ddlErrorMsg" name="type" id="type" onchange="getContenMaxOrder(this.value);" style="width:100%;">
							 <option value="">-- Please Select --</option>
							 <option value="I" <?php if('I'==$row->type){echo "selected";} ?>>Image</option>
							 <option value="U" <?php if('U'==$row->type){echo "selected";} ?>>Embedded Link</option>
						 </select>
					 </div>
					</div>
			</div>

			<div class="row">
	      <div class="col-lg-6 col-md-6 col-sm-12">
	        <div class="form-group">
	          <label for="exampleInputEmail1"> Order No&nbsp;<span class="required_label">*</span></label>
	          <label style="float: right;color: red;" id="order_label"> </label>
	          <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" value="<?php echo $row->order_no; ?>" placeholder="order no..." min="0" max="100">
	        </div>
	       </div>

	      <div class="col-lg-6 col-md-6 col-sm-12" id="div_image">
	        <div class="form-group">
	        <label for="customFile">File&nbsp;<span class="required_label">*</span></label>
					<span class="required_label">For best case please use width:410px height: 315px</span>
	          <div class="custom-file">
	            <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
	            <label class="custom-file-label" for="customFile">Choose file</label>
	          </div>
	        </div>
	       </div>

 			 <div class="col-lg-6 col-md-6 col-sm-12" id="div_url">
          <div class="form-group">
          <label for="customFile">Youtube Link Code&nbsp;<span class="required_label">*</span></label>
				   <input type="text" maxlength="20" name="url" id="url" class="form-control" value="<?php echo $row->url; ?>">
          </div>
         </div>
			 </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="old_photo_location" value="<?php echo $row->url; ?>" />
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>images">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
	<script>

	$(document).ready(function(){
		div_hide_show();
		$("#type").change(function(){
			 div_hide_show();
		 });
	});

	function div_hide_show()
	{
		var type=$('#type').val();
		if(type=='I')
		{
			$('#div_image').show();
			$('#div_url').hide();
			$('#url').val('');
			$('#url').removeClass("nameValidate");
			// $("#customFile").prop('required',true);

		}else{
			 $('#div_image').hide();
			 $('#div_url').show();
			 $('#customFile').val('');
			 $('#url').addClass("nameValidate");
       // $("#customFile").prop('required',false);
		}
	}

	</script>
