
<style>
.mailbox-attachments li {
    border: 1px solid #eee;
    /* float: left; */
    margin-bottom: 10px;
    margin-right: 10px;
    /* width: 200px; */
    width: 30%;
    margin: auto;
}
</style>


<!-- /.card-body -->
   <?php if(!empty($row->photo_location) &&  $row->photo_location!=''){?>
          <?php
              $ext = explode('.',$row->photo_location);
              $end_ext=end($ext);
           ?>
            <div class="card-footer bg-white">
              <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
                <li>
                  <span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/mother_concern/<?php echo $row->photo_location; ?>" alt="Attachment"></span>

                  <div class="mailbox-attachment-info">
                    <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/mother_concern/<?php echo $row->photo_location; ?>" class="mailbox-attachment-name"> <?php echo $row->photo_location; ?></a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.00 MB</span>
                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/mother_concern/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>

              </ul>
            </div>
					  <?php } ?>
            <!-- /.card-footer -->
  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>partnerships/edit/<?php echo $row->id; ?>" method="post"  enctype="multipart/form-data">
    <div class="card-body">

     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
					 <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." maxlength="300" value="<?php echo $row->title; ?>">
         </div>
        </div>
		 </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Order No&nbsp;<span class="required_label">*</span></label>
          <label style="float: right;color: red;" id="order_label"> </label>
          <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" value="<?php echo $row->order_no; ?>" placeholder="order no..." min="0" max="500">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Partnership Category&nbsp;<span class="required_label">*</span></label>
           <select class="form-control select2 ddlErrorMsg" name="type" id="type" style="width:100%;">
             <option value="">-Please Select--</option>
             <option value="AP" <?php if('AP'==$row->type){echo "selected";} ?>>Academic Partner</option>
             <option value="RDP" <?php if('RDP'==$row->type){echo "selected";} ?>>Research & Development Partner</option>
           </select>
         </div>
        </div>
    </div>
    <div class="row" id="div_file">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
        <label for="customFile">File</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
        </div>
       </div>
    </div>

    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Details Link<span class="required_label">*</span></label>
           <input type="text" name="link" class="form-control nameValidate" id="link" value="<?php echo $row->link; ?>" placeholder="link..." maxlength="400">
         </div>
        </div>
    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="old_photo_location" value="<?php echo $row->photo_location; ?>" />
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>mother_concern">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
  <script>

  $(document).ready(function(){

  });


  </script>
