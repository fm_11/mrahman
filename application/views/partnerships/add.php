
<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>partnerships/add" method="post"  enctype="multipart/form-data">
    <div class="card-body">

     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." maxlength="300">
         </div>
        </div>
		 </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Order No&nbsp;<span class="required_label">*</span></label>
          <label style="float: right;color: red;" id="order_label"> </label>
          <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" placeholder="order no..." min="0" max="500">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Partnership Category&nbsp;<span class="required_label">*</span></label>
           <select class="form-control select2 ddlErrorMsg" name="type" id="type" style="width:100%;">
             <option value="">-Please Select--</option>
             <option value="AP">Academic Partner</option>
             <option value="RDP">Research & Development Partner</option>

           </select>
         </div>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
        <label for="customFile">File&nbsp;<span class="required_label">*</span></label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" required name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose Photo</label>
          </div>
        </div>
       </div>
    </div>

    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Details Link<span class="required_label">*</span></label>
           <input type="text" name="link" class="form-control nameValidate" id="link" placeholder="link..." maxlength="400">
         </div>
        </div>
    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      <a href="<?php echo base_url(); ?>mother_concern">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>
	<!-- Page specific script -->
	<script>

  $(document).ready(function(){

  });

	</script>
