

<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>designations/edit/<?php echo $row->id; ?>" method="post">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Name&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="name" class="form-control nameValidate" id="name" placeholder="name..." maxlength="100" value="<?php echo $row->name; ?>">
         </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1"> Code&nbsp;<span class="required_label">*</span></label>
            <input type="text" name="code" class="form-control codeValidate" id="code" placeholder="code..." maxlength="50" value="<?php echo $row->code; ?>">
          </div>
         </div>

		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Description</label>
           <textarea class="form-control" rows="3" name="remarks" placeholder="enter ..." maxlength="200"><?php echo $row->remarks; ?></textarea>
         </div>
        </div>
		 </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>designations">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
	<script>
	$(function () {

	});
	</script>
