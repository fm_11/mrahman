
            <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th class="">SL.</th>
                <th class="">FAQ Category Name</th>
                <th class="">Details</th>
                <th class="">Order No</th>
                <th class="text-center">Action</th>
              </tr>
              </thead>
              <tbody>
              <?php  $i = 0;
                    foreach ($faq_list as $row):
                    $i++;?>
                    <tr>
                      <td class=""><?php echo $i; ?></td>
                      <td class=""><?php echo $row['faq_category']; ?></td>
                      <td class=""><?php echo $row['message']; ?></td>
                      <td class=""><?php echo $row['order_no']; ?></td>
                      <td>
                        <div class="input-group-prepend" style="margin: auto;width: 50%;">
                          <button type="button" style="padding: 0.20rem .80rem;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-edit"></i>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>faq/edit/<?php echo $row['id']; ?>" title="edit">Edit</a>    
                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>faq/delete/<?php echo $row['id']; ?>" title="delete">Delete</a>
                          </div>
                        </div>
                      </td>
                    </tr>
                 <?php endforeach; ?>
              </tbody>
              <!-- <tfoot>
              <tr>
                <th>SL.</th>
                <th>Role Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </tfoot> -->
            </table>
