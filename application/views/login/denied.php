<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<div class="popover-static-demo">
					<div class="popover bs-popover-bottom bs-popover-bottom-demo popover-danger">
						<div class="arrow"></div>
						<h3 class="popover-header"><?php echo $title; ?></h3>
						<div class="popover-body">
							<p>You are not permitted to access this page</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
