<script type="text/javascript">
	function getContenMaxOrder(type) {
		if(type != ''){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
          debugger;
					if(xmlhttp.responseText != false){
            debugger;
						var max_order = JSON.parse((xmlhttp.responseText));

						document.getElementById('order_label').innerHTML ="Max Order No. "+ max_order;
					}else{
						document.getElementById('order_label').innerHTML = "";
					}
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>content_paragraph/getContenMaxOrder?type=" + type, true);
			xmlhttp.send();
		}else{
			document.getElementById('order_label').innerHTML = "";
		}
	}
  </script>
<style>
.mailbox-attachments li {
    border: 1px solid #eee;
    /* float: left; */
    margin-bottom: 10px;
    margin-right: 10px;
    /* width: 200px; */
    width: 30%;
    margin: auto;
}
</style>


<!-- /.card-body -->
   <?php if(!empty($row->photo_location) &&  $row->photo_location!=''){?>
          <?php
              $ext = explode('.',$row->photo_location);
              $end_ext=end($ext);
           ?>
            <div class="card-footer bg-white">
              <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
							 <?php if($end_ext=='pdf'){?>
                <li>
                  <span class="mailbox-attachment-icon"><i class="far fa-file-pdf"></i></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"> <?php echo $row->photo_location; ?></a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.00 MB</span>
                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>
							<?php }elseif ($end_ext=='csv' || $end_ext=='doc' || $end_ext=='docx') { ?>
                <li>
                  <span class="mailbox-attachment-icon"><i class="far fa-file-word"></i></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fas fa-paperclip"></i> <?php echo $row->photo_location; ?></a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.00 MB</span>
                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>
								<?php }else { ?>
                <li>
                  <span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $row->photo_location; ?>" alt="Attachment"></span>

                  <div class="mailbox-attachment-info">
                    <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $row->photo_location; ?>" class="mailbox-attachment-name"></a>
                        <span class="mailbox-attachment-size clearfix mt-1">

                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/documents/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>
                 <?php } ?>
              </ul>
            </div>
					  <?php } ?>
            <!-- /.card-footer -->
  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>documents/edit/<?php echo $row->id; ?>" method="post"  enctype="multipart/form-data">
    <div class="card-body">

     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." value="<?php echo $row->title; ?>" maxlength="200">
         </div>
        </div>
		 </div>
		 <div class="row">
		  <div class="col-lg-12 col-md-12 col-sm-12">
		 	 <div class="form-group">
		 		 <label for="exampleInputEmail1"> Slogan</label>
		 		 <input type="text" name="slogan" class="form-control" id="slogan" value="<?php echo $row->slogan; ?>" placeholder="slogan..." maxlength="300">
		 	 </div>
		 	</div>
		 </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Order No&nbsp;<span class="required_label">*</span></label>
          <label style="float: right;color: red;" id="order_label"> </label>
          <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" value="<?php echo $row->order_no; ?>" placeholder="order no..." min="0" max="100">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Content Type&nbsp;<span class="required_label">*</span></label>
           <select class="form-control select2 ddlErrorMsg" name="type" id="type" style="width:100%;" disabled>
             <option value="">-Please Select--</option>
             <option value="S" <?php if('S'==$row->type){echo "selected";} ?>>Slider</option>
             <option value="D" <?php if('D'==$row->type){echo "selected";} ?>>Download</option>
           </select>
         </div>
        </div>
    </div>
    <div class="row" >
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<label for="exampleInputEmail1"> Description</label>
					<textarea class="form-control" rows="3"  name="description" id="description" placeholder="description ..." maxlength="600"><?php echo $row->description; ?></textarea>
				</div>
			 </div>
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
        <label for="customFile">File</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
        </div>
       </div>
    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="old_photo_location" value="<?php echo $row->photo_location; ?>" />
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
			<input type="hidden" name="type" value="<?php echo $row->type; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>documents">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
  <script>

  </script>
