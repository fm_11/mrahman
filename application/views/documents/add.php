
<script type="text/javascript">

	function getContenMaxOrder(type) {
		if(type != ''){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
          debugger;
					if(xmlhttp.responseText != false){
            debugger;
						var max_order = JSON.parse((xmlhttp.responseText));

						document.getElementById('order_label').innerHTML ="Max Order No. "+ max_order;
					}else{
						document.getElementById('order_label').innerHTML = "";
					}
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>documents/getContenMaxOrder?type=" + type, true);
			xmlhttp.send();
		}else{
			document.getElementById('order_label').innerHTML = "";
		}
	}


  </script>
<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>documents/add" method="post"  enctype="multipart/form-data">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." maxlength="300">
         </div>
        </div>
		 </div>
		 <div class="row">
		 	<div class="col-lg-12 col-md-12 col-sm-12">
		 		<div class="form-group">
		 			<label for="exampleInputEmail1"> Slogan</label>
		 			<input type="text" name="slogan" class="form-control" id="slogan" placeholder="slogan..." maxlength="300">
		 		</div>
		 	 </div>
		 </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Order No&nbsp;<span class="required_label">*</span></label>
          <label style="float: right;color: red;" id="order_label"> </label>
          <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" placeholder="order no..." min="0" max="100">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Type&nbsp;<span class="required_label">*</span></label>
           <select class="form-control select2 ddlErrorMsg" name="type" id="type" onchange="getContenMaxOrder(this.value);" style="width:100%;">
             <option value="">-Please Select--</option>
             <option value="S">Slider</option>
             <option value="D">Download</option>
           </select>
         </div>
        </div>
    </div>
    <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
		 		<div class="form-group">
		 			<label for="exampleInputEmail1"> Description</label>
		 		  <textarea class="form-control" rows="3"  name="description" id="description" placeholder="description ..." maxlength="600"></textarea>
		 		</div>
		 	 </div>
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
        <label for="customFile">File&nbsp;<span class="required_label">*</span></label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" required name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
        </div>
       </div>
    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      <a href="<?php echo base_url(); ?>documents">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>
	<!-- Page specific script -->
	<script>


	</script>
