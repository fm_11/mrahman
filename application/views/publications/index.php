<script type="text/javascript">
    function msgStatusUpdate(id, status) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>publications/updateMsgContentStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }

</script>
            <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th class="">SL.</th>
                <th class="">Category Name</th>
                <th class="">Publication</th>
                <th class="">Year</th>
                <th class="">Order No</th>
                <th class="">Status</th>
                <th class="text-center">Action</th>
              </tr>
              </thead>
              <tbody>
              <?php  $i = 0;
                    foreach ($publication_list as $row):
                    $i++;?>
                    <tr>
                      <td class=""><?php echo $i; ?></td>
                      <td class=""><?php echo $row['category_name']; ?></td>
                      <td class=""><?php echo $row['title']; ?></td>
                      <td class=""><?php echo $row['year']; ?></td>
                      <td class=""><?php echo $row['order_no']; ?></td>
                      <td class="" id="status_sction_<?php echo $row['id']; ?>">
                        <?php
                        if ($row['is_active'] == 1) {?>
                            <a title="Active" href="#"
                               onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)">
                               <button type="button" class="btn btn-block btn-success btn-sm">Active</button>
                             </a>
                        <?php } else {  ?>
                            <a title="Inactive" href="#"
                               onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)">
                               <button type="button" class="btn btn-block btn-danger btn-sm">Inactive</button>
                             </a>
                        <?php  } ?>
                        </td>
                      <td>
                        <div class="input-group-prepend" style="margin: auto;width: 50%;">
                          <button type="button" style="padding: 0.20rem .80rem;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-edit"></i>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>publications/edit/<?php echo $row['id']; ?>" title="edit">Edit</a>
                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>publications/delete/<?php echo $row['id']; ?>" title="delete">Delete</a>
                          </div>
                        </div>
                      </td>
                    </tr>
                 <?php endforeach; ?>
              </tbody>
              <!-- <tfoot>
              <tr>
                <th>SL.</th>
                <th>Role Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </tfoot> -->
            </table>
