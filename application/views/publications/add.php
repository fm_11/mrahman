<script type="text/javascript">

	function get_pub_max_order_by_pub_category_id(publication_category_id) {
		if(publication_category_id != ''){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
          debugger;
					if(xmlhttp.responseText != false){
            debugger;
						var max_order = JSON.parse((xmlhttp.responseText));

						document.getElementById('order_label').innerHTML ="Max Order No. "+ max_order;
					}else{
						document.getElementById('order_label').innerHTML = "";
					}
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>publications/get_pub_max_order_by_pub_category_id?publication_category_id=" + publication_category_id, true);
			xmlhttp.send();
		}else{
			document.getElementById('order_label').innerHTML = "";
		}
	}


  </script>

<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>publications/add" method="post">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1"> Publications Category&nbsp;<span class="required_label">*</span></label>
            <select class="form-control select2 ddlErrorMsg" name="publication_category_id" onchange="get_pub_max_order_by_pub_category_id(this.value);"  style="width:100%;">
              <option value="">-Please Select--</option>
              <?php
                 if (count($publications_category)) {
                     foreach ($publications_category as $list) {
                         ?>
                         <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                     <?php
                     }
                 }
                 ?>
            </select>
          </div>
         </div>
      </div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<div class="form-group">
						<label for="exampleInputEmail1"> Year&nbsp;<span class="required_label">*</span></label>
						<input type="number" name="year" class="form-control numberfieldErrorMsg" id="year" placeholder="year..." min="0">
					</div>
				 </div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<div class="form-group">
						<label for="exampleInputEmail1"> Order No&nbsp;<span class="required_label">*</span></label>
            <label style="float: right;color: red;" id="order_label"> </label>
						<input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" placeholder="order no..." min="0" max="500">
					</div>
				 </div>
			</div>
			<div class="row">
	       <div class="col-lg-12 col-md-12 col-sm-12">
	         <div class="form-group">
	           <label for="exampleInputEmail1">Publication&nbsp;<span class="required_label">*</span></label>
	           <textarea  name="title"  id="summernote" required placeholder="Publication details ..." ></textarea>
	         </div>
	        </div>
	    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      <a href="<?php echo base_url(); ?>publications">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>
