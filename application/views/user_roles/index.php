

            <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th class="">SL.</th>
                <th class="">Role Name</th>
                <th class="">Description</th>
                <th class="text-center">Action</th>
              </tr>
              </thead>
              <tbody>
              <?php  $i = 0;
                    foreach ($user_roles as $row):
                    $i++;?>
                    <tr>
                      <td class=""><?php echo $i; ?></td>
                      <td class=""><?php echo $row['role_name']; ?></td>
                      <td class=""><?php echo $row['role_description']; ?></td>
                      <td>
                        <div class="input-group-prepend" style="margin: auto;width: 50%;">
                          <button type="button" style="padding: 0.20rem .80rem;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-edit"></i>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>user_roles/edit/<?php echo $row['id']; ?>" title="edit">Edit</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>user_role_wise_privileges/index/<?php echo $row['id']; ?>" title="Chnage Privilege">Chnage Privilege</a>
                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>user_roles/delete/<?php echo $row['id']; ?>" title="delete">Delete</a>

                          </div>
                        </div>
                      </td>
                    </tr>
                 <?php endforeach; ?>
              </tbody>
              <!-- <tfoot>
              <tr>
                <th>SL.</th>
                <th>Role Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </tfoot> -->
            </table>
