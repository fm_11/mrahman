

<!-- Profile Image -->
<?php if(!empty($row->photo_location) &&  $row->photo_location!=''){?>
           <div class="card card-primary card-outline">
             <div class="card-body box-profile">
               <div class="text-center">
                 <img class="profile-user-img img-fluid"
                      src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/employee/<?php echo $row->photo_location; ?>"
                      alt="User profile picture">
               </div>
               <h3 class="profile-username text-center"><?php echo $row->name; ?></h3>
             </div>
             <!-- /.card-body -->
           </div>
  <?php } ?>
<!-- form start id="quickForm"-->

<form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>home_message/index" method="post"  enctype="multipart/form-data">
  <div class="card-body">
   <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12">
       <div class="form-group">
         <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
         <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." maxlength="255" value="<?php echo $row->title; ?>">
       </div>
      </div>
   </div>

  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="form-group">
        <label for="exampleInputEmail1"> Name&nbsp;<span class="required_label">*</span></label>
      <input type="text" name="name" class="form-control nameValidate" id="name" placeholder="name..." maxlength="100" value="<?php echo $row->name; ?>">
      </div>
     </div>
     <div class="col-lg-6 col-md-6 col-sm-12">
       <div class="form-group">
         <label for="exampleInputEmail1"> Designation&nbsp;<span class="required_label">*</span></label>
         <input type="text" name="designation" class="form-control nameValidate" id="designation" placeholder="designation..." maxlength="100" value="<?php echo $row->designation; ?>">
       </div>
      </div>
  </div>
  <div class="row" id="div_file">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="form-group">
      <label for="customFile">Photo</label>
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
          <label class="custom-file-label" for="customFile">Choose Photo</label>
        </div>
      </div>
     </div>
  </div>

  <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12">
       <div class="form-group">
         <label for="exampleInputEmail1">Message&nbsp;<span class="required_label">*</span></label>
         <textarea  name="long_message"  id="summernote" required placeholder="message ..." ><?php echo $row->long_message; ?></textarea>
       </div>
      </div>
  </div>
  </div>

  <!-- /.card-body -->
  <div class="card-footer">
      <input type="hidden" name="old_photo_location" value="<?php echo $row->photo_location; ?>" />
    <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
    <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
  </div>
</form>
