
<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>home_message/add" method="post"  enctype="multipart/form-data">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." maxlength="255">
         </div>
        </div>
		 </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Name&nbsp;<span class="required_label">*</span></label>
        <input type="text" name="name" class="form-control nameValidate" id="name" placeholder="name..." maxlength="100">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Designation&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="designation" class="form-control nameValidate" id="designation" placeholder="designation..." maxlength="100">
         </div>
        </div>
    </div>
    <div class="row" id="div_file">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
        <label for="customFile">Photo&nbsp;<span class="required_label">*</span></label>
          <div class="custom-file">
            <input type="file" required class="custom-file-input" name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose Photo</label>
          </div>
        </div>
       </div>
    </div>

    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Message&nbsp;<span class="required_label">*</span></label>
           <textarea  name="long_message"  id="summernote" required placeholder="message ..." ></textarea>
         </div>
        </div>
    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
    </div>
  </form>
