<?php
$controller = $this->uri->segment(1);
$action = $this->uri->segment(2);
?>
<?php
  $session_user = $this->session->userdata('user_info');
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="<?php echo base_url(); ?>dashboard/index" class="brand-link">
    <!-- <img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/orga/<?php echo  $session_user[0]->contact_info[0]['picture'];?>" alt="AdminLTE Logo" class="brand-image elevation-3" style="opacity: .8"> -->
    <img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light"><?php echo  $session_user[0]->contact_info[0]['school_name'];?></span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div> -->

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item menu-open">
          <a href="<?php echo base_url(); ?>dashboard/index" class="nav-link <?php if ($controller == 'dashboard') { ?>active<?php } ?>">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard

            </p>
          </a>

        </li>

        <li class="nav-item <?php if (in_array($controller, array('user_roles','users'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('user_roles','users'))) { ?>active<?php } ?>">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Users
              <i class="fas fa-angle-left right"></i>
              <!-- <span class="badge badge-info right">6</span> -->
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>user_roles/index" class="nav-link <?php if ($controller == 'user_roles') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>User Roles</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>users/index" class="nav-link <?php if ($controller == 'users' && ( $action=='index' || $action=='edit')) { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>User List</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>users/add" class="nav-link <?php if ($controller == 'users' && $action=='add') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>User Add</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>users/change_password" class="nav-link <?php if ($controller == 'users' && $action=='change_password') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Change Password</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-header">Configurations</li>
        <!-- <li class="nav-item">
          <a href="pages/calendar.html" class="nav-link">
            <i class="nav-icon far fa-calendar-alt"></i>
            <p>
              Calendar
              <span class="badge badge-info right">2</span>
            </p>
          </a>
        </li> -->
        <li class="nav-item <?php if (in_array($controller, array('designations','branches','org_infos','organizations','board_types','area_of_interest'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('designations','branches','org_infos','organizations','board_types','area_of_interest'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
              Basic Setting
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>org_infos/index" class="nav-link <?php if ($controller == 'org_infos') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Basic Informations</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item <?php if (in_array($controller, array('menus'))) { ?>menu-open<?php } ?>">
            <a href="#" class="nav-link <?php if (in_array($controller, array('menus'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
              <p>
                Dynamic Menu Config
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>menus/add" class="nav-link <?php if ($controller == 'menus' && ( $action=='add' || $action=='edit')) { ?>active<?php } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Menu Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>menus/index" class="nav-link <?php if ($controller == 'menus' && $action=='index') { ?>active<?php } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Menu List</p>
                </a>
              </li>
            </ul>
        </li>
        <li class="nav-item <?php if (in_array($controller, array('content_paragraph'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('content_paragraph'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
              Content Details
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>content_paragraph/index" class="nav-link <?php if ($controller == 'content_paragraph') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p> Content</p>
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item <?php if (in_array($controller, array('about_me'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('about_me'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
              About Me
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>about_me/index" class="nav-link <?php if ($controller == 'about_me') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Update About Me</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item <?php if (in_array($controller, array('researchs'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('researchs'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
            Research
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>researchs/index" class="nav-link <?php if ($controller == 'researchs' && $action=='index') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Research Infos</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>researchs/add" class="nav-link <?php if ($controller == 'researchs' && $action=='add') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Research Info</p>
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item <?php if (in_array($controller, array('events_category','news_and_blog'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('events_category','news_and_blog'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
            News
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>events_category/index" class="nav-link <?php if ($controller == 'events_category') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>News Category</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>news_and_blog/index" class="nav-link <?php if ($controller == 'news_and_blog') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>News</p>
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item <?php if (in_array($controller, array('publications_categorys','publications'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('publications_categorys','publications'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
            Publications
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>publications_categorys/index" class="nav-link <?php if ($controller == 'publications_categorys') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Publications Category</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>publications/index" class="nav-link <?php if ($controller == 'publications') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Publications</p>
              </a>
            </li>
          </ul>
        </li>



        <li class="nav-item <?php if (in_array($controller, array('teachings'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('teachings'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
            Teaching
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>teachings/index" class="nav-link <?php if ($controller == 'teachings') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Teaching Infos</p>
              </a>
            </li>

          </ul>
        </li>



        <li class="nav-item <?php if (in_array($controller, array('images'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('images'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
            Images/Link
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>images/index" class="nav-link <?php if ($controller == 'images') { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Images/Link List</p>
              </a>
            </li>

          </ul>
        </li>


        <li class="nav-item <?php if (in_array($controller, array('contacts'))) { ?>menu-open<?php } ?>">
          <a href="#" class="nav-link <?php if (in_array($controller, array('contacts'))) { ?>active<?php } ?>">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
              Contacts
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>contacts/index" class="nav-link <?php if ($controller == 'contacts'  && ( $action=='index')) { ?>active<?php } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Contacts List</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>

  </div>
  <!-- /.sidebar -->
</aside>
