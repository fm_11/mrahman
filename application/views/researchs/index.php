<script type="text/javascript">
    function msgStatusUpdate(id, status) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>researchs/updateMsgContentStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }

</script>

            <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th class="">SL.</th>
                <th class="">Title</th>
                <!-- <th class="">Description</th> -->
                <th class="text-center">Order No</th>
                <th class="text-center">Show Image</th>
                <th class="text-center">Status</th>
                <th class="text-center">Action</th>
              </tr>
              </thead>
              <tbody>
              <?php  $i = 0;
                    foreach ($content as $row):
                    $i++;?>
                    <tr>
                      <td class="text-center"><?php echo $i; ?></td>
                      <td class=""><?php echo $row['title']; ?></td>
                      <!-- <td class=""><?php echo $row['description']; ?></td> -->
                      <td class="text-center"><?php echo $row['order']; ?></td>
                      <td class="text-center">
                        <?php if($row['show_feature_image']=='0'){
                          echo '<span  class="badge badge-danger">No</span>';
                        }else {
                            echo '<span  class="badge badge-success">Yes</span>';
                        }
                        ?>
                      </td>
                      <td class="" id="status_sction_<?php echo $row['id']; ?>">
                        <?php
                        if ($row['is_active'] == 1) {?>
                            <a title="Active" href="#"
                               onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)">
                               <button type="button" class="btn btn-block btn-success btn-sm">Active</button>
                             </a>
                        <?php } else {  ?>
                            <a title="Inactive" href="#"
                               onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)">
                               <button type="button" class="btn btn-block btn-danger btn-xs">Inactive</button>
                             </a>
                        <?php  } ?>
                        </td>

                      <td>
                        <div class="input-group-prepend" style="margin: auto;width: 50%;">
                          <button type="button" style="padding: 0.20rem .80rem;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-edit"></i>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>researchs/edit/<?php echo $row['id']; ?>" title="edit">Edit</a>
                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>researchs/delete/<?php echo $row['id']; ?>" title="delete">Delete</a>
                          </div>
                        </div>
                      </td>
                    </tr>
                 <?php endforeach; ?>
              </tbody>
              <!-- <tfoot>
              <tr>
                <th>SL.</th>
                <th>Role Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </tfoot> -->
            </table>
