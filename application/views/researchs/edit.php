<?php if(!empty($row->file_location) &&  $row->file_location!=''){?>
           <div class="card card-primary card-outline">
             <div class="card-body box-profile">
               <div class="text-center">
                 <img class="profile-user-img img-fluid img-circle"
                      src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $row->file_location; ?>"
                      alt="No image found">
               </div>
             </div>
             <!-- /.card-body -->
           </div>
  <?php } ?>
  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>researchs/edit/<?php echo $row->id; ?>" method="post"  enctype="multipart/form-data">
    <div class="card-body">

     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." value="<?php echo $row->title; ?>" >
         </div>
        </div>
		 </div>
		 <div class="row">
		  <div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
 				 <label for="exampleInputEmail1"> Order No&nbsp;<span class="required_label">*</span></label>
 				 <input type="text" name="order" class="form-control numberfieldErrorMsg" id="order" value="<?php echo $row->order; ?>" placeholder="order no..." min="0" max="100">
 			 </div>
		 	</div>
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Show Image?&nbsp;<span class="required_label">*</span></label>
          <select class="form-control select2" name="show_feature_image" id="show_feature_image" style="width:100%;">
            <option value="1" <?php if('1'==$row->show_feature_image){echo "selected";} ?>>Yes</option>
            <option value="0" <?php if('0'==$row->show_feature_image){echo "selected";} ?>>No</option>
          </select>
        </div>
       </div>
		 </div>
       <span class="required_label">Total row size of Text and Image must be 12</span></label>
     <div class="row">
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Text Row&nbsp;<span class="required_label">*</span></label>
           <input type="number" name="text_row" class="form-control numberfieldErrorMsg" id="text_row" value="<?php echo $row->text_row; ?>"  min="0" max="12">
         </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1"> Image row&nbsp;<span class="required_label">*</span></label>
            <input type="number" name="image_row" class="form-control numberfieldErrorMsg" id="image_row" value="<?php echo $row->image_row; ?>"  min="0" max="12">
          </div>
         </div>

     </div>
		 <div class="row">
			 <div class="col-lg-12 col-md-12 col-sm-12">
 				<div class="form-group">
 					<label for="exampleInputEmail1"> Description</label>
 					<textarea class="form-control" rows="3"  name="description" id="summernote" placeholder="description ..." maxlength="600"><?php echo $row->description; ?></textarea>
 				</div>
 			 </div>
		 </div>
    <div class="row" >
			<!-- <div class="col-lg-4 col-md-4 col-sm-4">
				<span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $row->file_location; ?>"width="50" height="50"></span>
			</div> -->
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
        <label for="customFile">Image 1</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
        </div>
       </div>
    </div>

    <div class="row" >
      <?php if(!empty($row->file_location2) &&  $row->file_location2!=''){?>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $row->file_location2; ?>"width="50" height="50"></span>
			</div>
        <?php } ?>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="form-group">
        <label for="customFile">Image 2</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location2" id="customFile">
            <label class="custom-file-label" for="customFile">Choose Photo 2</label>
          </div>
        </div>
       </div>
    </div>

    <div class="row" >
      <?php if(!empty($row->file_location3) &&  $row->file_location3!=''){?>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/content/<?php echo $row->file_location3; ?>"width="50" height="50"></span>
			</div>
        <?php } ?>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="form-group">
        <label for="customFile">Image 3</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location3" id="customFile">
            <label class="custom-file-label" for="customFile">Choose Photo 3</label>
          </div>
        </div>
       </div>
    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="old_photo_location" value="<?php echo $row->file_location; ?>" />
      <input type="hidden" name="old_photo_location2" value="<?php echo $row->file_location2; ?>" />
        <input type="hidden" name="old_photo_location3" value="<?php echo $row->file_location3; ?>" />
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>researchs">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
  <script>

  </script>
