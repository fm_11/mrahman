


<!-- Profile Image -->
<?php if(!empty($row->file_location) &&  $row->file_location!=''){?>
           <div class="card card-primary card-outline">
             <div class="card-body box-profile">
               <div class="text-center">
                 <img class="profile-user-img img-fluid img-circle"
                      src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/orga/<?php echo $row->file_location; ?>"
                      alt="User profile picture">
               </div>
             </div>
             <!-- /.card-body -->
           </div>
  <?php } ?>
<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>about_me/index" method="post"  enctype="multipart/form-data">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Title&nbsp;</label>
           <input type="text" name="title" class="form-control" id="title" placeholder="title..." value="<?php echo $row->title; ?>">
         </div>
        </div>
		 </div>
     <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1">Short Description&nbsp;<span class="required_label">*</span></label>
            <textarea  name="about_1"  id="summernote" required placeholder="Short Description ..." ><?php echo $row->about_1; ?></textarea>
          </div>
         </div>
     </div>

     <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1">Long Description&nbsp;<span class="required_label">*</span></label>
            <textarea  name="about_2"  id="summernote2" required placeholder="Long Description ..." ><?php echo $row->about_2; ?></textarea>
          </div>
          [<span class="required_label">Note:</span>How To Insert Google Drive Image Link ( https://drive.google.com/uc?export=view&id=<span  class="required_label">Image Code </span> )]
         </div>
     </div>

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
        <label for="customFile">Photo</label>
        <span class="required_label">For best case please use height: 545px  width:415px</span>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose Image</label>
          </div>
        </div>
       </div>
    </div>




    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="old_photo_location" value="<?php echo $row->file_location; ?>" />
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>org_infos">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
    </div>
  </form>

	<!-- Page specific script -->
	<script>
	$(function () {
    $('#summernote').summernote({
        placeholder: 'Short Description....'});
    $('#summernote2').summernote({
        placeholder: 'Long Description....'});
 });
	</script>
