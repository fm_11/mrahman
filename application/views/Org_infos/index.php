


<!-- Profile Image -->
 <?php if(!empty($row->picture) &&  $row->picture!=''){?>
           <div class="card card-primary card-outline">
             <div class="card-body box-profile">
               <div class="text-center">
                 <video width="320" height="240" controls>
                    <source src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/orga/<?php echo $row->picture; ?>" type="video/mp4">
                    <source src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/orga/<?php echo $row->picture; ?>" type="video/ogg">
                    Your browser does not support the video tag.
                </video>
               </div>
               <h3 class="profile-username text-center"><?php echo $row->school_name; ?></h3>
             </div>
             <!-- /.card-body -->
           </div>
  <?php } ?>
<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>org_infos/index" method="post"  enctype="multipart/form-data">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Website Name&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="school_name" class="form-control nameValidate" id="school_name" placeholder="name..." value="<?php echo $row->school_name; ?>" maxlength="100">
         </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" name="tin_number" class="form-control" id="tin_number" placeholder="tin number..."  value="<?php echo $row->tin_number; ?>" maxlength="50">
          </div>
         </div>
		 </div>
     <div class="row">
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Mobile</label>
           <div class="input-group">
             <div class="input-group-prepend">
               <span class="input-group-text"><i class="fas fa-phone"></i></span>
             </div>
             <input type="text" name="mobile" class="form-control" value="<?php echo $row->mobile; ?>" maxlength="100">
           </div>
         </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" name="email" class="form-control " id="email" value="<?php echo $row->email; ?>" placeholder="email..." maxlength="100">
          </div>
         </div>
     </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Web Address</label>
          <input type="text" name="web_address" class="form-control scriptValidate" id="web_address" value="<?php echo $row->web_address; ?>" placeholder="web address..." maxlength="100">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Linkedin Link</label>
           <input type="text" name="linkedin" class="form-control scriptValidate" id="linkedin" value="<?php echo $row->linkedin; ?>" placeholder="linkedin link..." maxlength="200">
         </div>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Twitter Link</label>
          <input type="text" name="twitter" class="form-control scriptValidate" id="twitter" value="<?php echo $row->twitter; ?>" placeholder="twitter link..." maxlength="200">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Google Scholar Link</label>
           <input type="text" name="google_plus" class="form-control scriptValidate" id="google_plus" value="<?php echo $row->google_plus; ?>" placeholder="google plus link..." maxlength="200">
         </div>
        </div>
    </div>
    <!-- <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Youtube Link</label>
          <input type="text" name="youtube" class="form-control scriptValidate" id="youtube" value="<?php echo $row->youtube; ?>" placeholder="youtube link..." maxlength="200">
        </div>
       </div>
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Facebook Link</label>
          <input type="text" name="facebook_address" class="form-control scriptValidate" id="facebook_address" value="<?php echo $row->facebook_address; ?>" placeholder="facebook link..." maxlength="200">
        </div>
       </div>

    </div> -->

    <!-- <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Open Talk</label>
          <input type="number" name="open_talk" class="form-control" id="open_talk" value="<?php echo $row->open_talk; ?>" placeholder="open talk...">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Webinar/Seminar</label>
           <input type="number" name="webinar" class="form-control" id="webinar" value="<?php echo $row->webinar; ?>" placeholder="webinar...">
         </div>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Workshop/Bootcamp</label>
          <input type="number" name="workshop" class="form-control" id="workshop" value="<?php echo $row->workshop; ?>" placeholder="workshop...">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Course</label>
           <input type="number" name="course" class="form-control" id="course" value="<?php echo $row->course; ?>" placeholder="course...">
         </div>
        </div>
    </div> -->

    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Short Summary&nbsp;<span class="required_label">*</span></label>
           <textarea  name="facebook_address"  id="summernote2" placeholder="Short Summary ..."  maxlength="500"><?php echo $row->facebook_address; ?></textarea>
         </div>
        </div>
    </div>

    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Address&nbsp;<span class="required_label">*</span></label>
           <textarea  name="address"  id="summernote" placeholder="enter ..."  maxlength="500"><?php echo $row->address; ?></textarea>
         </div>
        </div>
    </div>
    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Embaded Google Map Link</label>
           <textarea  name="google_map" class="form-control" rows="4" id="google_map" placeholder="embaded google map link..."><?php echo $row->google_map; ?></textarea>
           <!-- <input type="text" name="google_map" class="form-control scriptValidate" id="google_map" value="<?php echo $row->google_map; ?>" placeholder="embaded google map..." > -->
         </div>
        </div>
    </div>
     <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Google Map Width</label>
          <input type="number" name="width" class="form-control" id="width" min="0" value="<?php echo $row->width; ?>" placeholder="width...">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Google Map Height</label>
           <input type="number" name="height" class="form-control" id="height"  min="0"  value="<?php echo $row->height; ?>" placeholder="height...">
         </div>
        </div>
    </div>

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
        <label for="customFile">Website Video</label>
        <span class="required_label">For best case please use small videos under 2MB</span>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose Video in .mp4</label>
          </div>
        </div>
       </div>
       <!-- <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <video width="320" height="240" controls>
              <source src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/orga/<?php echo $row->picture; ?>" type="video/mp4">
              <source src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/orga/<?php echo $row->picture; ?>" type="video/ogg">
              Your browser does not support the video tag.
          </video>
         </div>
        </div> -->
       <!-- <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Whatsapp Link</label>
           <input type="text" name="whatsapp" class="form-control scriptValidate" id="whatsapp" value="<?php echo $row->whatsapp; ?>" placeholder="whatsapp link..." maxlength="200">
         </div>
        </div> -->
    </div>
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="old_photo_location" value="<?php echo $row->picture; ?>" />
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>org_infos">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
	<script>
	$(function () {
    $('#quickForm').validate({
     rules: {
       address: {
         required: true
       }
     },
     messages: {
       address: {
         required: "Please enter a  address",
       }
     },
     errorPlacement: function (error, element) {
       error.addClass('invalid-feedback');
       element.closest('.form-group').append(error);
     },
     highlight: function (element, errorClass, validClass) {
       $(element).addClass('is-invalid');
     },
     unhighlight: function (element, errorClass, validClass) {
       $(element).removeClass('is-invalid');
     }
   });
 });
	</script>
