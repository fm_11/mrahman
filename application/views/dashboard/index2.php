<style>
	.custom_text_cls{
		font-weight: bold !important;
		color: #000000 !important;
	}

</style>

<?php
$session_user = $this->session->userdata('user_info');
$logo_name = PUBPATH . MEDIA_FOLDER . '/logos/' . $session_user[0]->contact_info[0]['picture'];
if(file_exists($logo_name)){
	//echo 88;die;
	$logo_name = base_url() . MEDIA_FOLDER . '/logos/' . $session_user[0]->contact_info[0]['picture'];
}else{
	$logo_name = base_url() . 'core_media/admin_v3/images/school360-logo.png';
}
$teacher_image = "core_media/images/teacher-big.png";
if (isset($dashboard_teacher_info)) {
	$teacher_image = MEDIA_FOLDER . "/teacher/" . $dashboard_teacher_info->photo_location;
}

?>

<div class="row">
	<div class="col-md-7 grid-margin stretch-card">
		<div class="card" style="border: 12px <?php echo $dashboard_color_info->school_name_border_color; ?> solid !important;">
			<div class="card-body">
				<div class="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center">
					<img src="<?php echo $logo_name ; ?>" class="rounded" style="width: 92px;" alt="profile image">
					<div class="ml-sm-3 ml-md-0 ml-xl-3 mt-2 mt-sm-0 mt-md-2 mt-xl-0">
						<h6 class="mb-0" style="font-size: 0.9rem !important;">
					
						</h6>

					</div>
				</div>
			</div>
		</div>
	</div>

</div>
