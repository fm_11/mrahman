

<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>events_category/edit/<?php echo $row->id; ?>" method="post">
    <div class="card-body">
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Name&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="name" class="form-control nameValidate" id="name" placeholder="name..." value="<?php echo $row->name; ?>" maxlength="200">
         </div>
        </div>

		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Order No&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="order_no" class="form-control numberfieldErrorMsg" id="order_no" placeholder="order no..." min="0" value="<?php echo $row->order_no; ?>">
         </div>
        </div>
		 </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>events_category">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
	<script>
	$(function () {
		$('#quickForm').validate({
			rules: {
				role_name: {
					maxlength: 200
				},
				role_description: {
					maxlength: 500
				},
			},
			messages: {
				role_name: {
					maxlength: "Maximum length should be 200 characters."
				},
				role_description: {
					maxlength: "Maximum length should be 500 characters."
				}
			},
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
	</script>
