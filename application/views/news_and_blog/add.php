
<!-- form start id="quickForm"-->

  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>news_and_blog/add" method="post"  enctype="multipart/form-data">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1"> Events Category&nbsp;<span class="required_label">*</span></label>
            <select class="form-control select2 ddlErrorMsg" name="category_id" style="width:100%;">
              <option value="">-Please Select--</option>
              <?php
                 if (count($category)) {
                     foreach ($category as $list) {
                         ?>
                         <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                     <?php
                     }
                 }
                 ?>
            </select>
          </div>
         </div>
 		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
           <textarea  name="title" class="form-control"  required id="summernote2" placeholder="description ..." ></textarea>
           <!-- <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." maxlength="300"> -->
         </div>
        </div>
		 </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
          <label for="exampleInputEmail1"> Year&nbsp;<span class="required_label">*</span></label>
          <input type="number" name="year" class="form-control numberfieldErrorMsg" id="year" placeholder="year..." min="0">
								</div>
						</div>
      <div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<label for="exampleInputEmail1"> Date&nbsp;<span class="required_label">*</span></label>
					<div class="input-group" id="reservationdate1" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input requiredDateErrMsg" name="date" data-target="#reservationdate1"/>
								<div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
						</div>
				 </div>
       </div>

    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Events Type&nbsp;<span class="required_label">*</span></label>
          <select class="form-control select2 ddlErrorMsg" name="type" id="type" style="width:100%;">
            <!-- <option value="B">Blog</option>
            <option value="E">Events</option> -->
            <option value="N" selected>News</option>
          </select>
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Is URL?&nbsp;</label>
           <select class="form-control select2" name="is_link_url" id="is_link_url" style="width:100%;">
             <option value="0" >No</option>
             <option value="1" >Yes</option>
           </select>
         </div>
        </div>
    </div>

    <!-- <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Youtube Embedded Link&nbsp;</label>
          <input type="text" name="embedded_youtube" class="form-control" id="embedded_youtube" placeholder="youtube embedded link..." maxlength="500">
        </div>
       </div>
    </div> -->

    <div class="row" id="div_url">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> URL</label>
          <textarea class="form-control" name="url" id="url"  placeholder="url ..."></textarea>
        </div>
       </div>
    <!-- <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Embaded Yoube Link</label>
          <textarea class="form-control" rows="3"  name="youtube_link" id="youtube_link" placeholder="embaded youtube link ..."></textarea>

        </div>
       </div>
    </div> -->
    <!-- <div class="row" id="div_google_map">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Embaded Google Map</label>
          <textarea class="form-control" rows="3"  name="google_map" id="google_map"  placeholder="embaded google map ..."></textarea>
        </div>
       </div>
    </div> -->
    <!-- <div class="row" id="div_venue">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Venue&nbsp;<span class="required_label">*</span></label>
          <input type="text" name="venue" class="form-control nameValidate" id="venue" placeholder="venue..." maxlength="300" >
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Time&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="time" class="form-control nameValidate" id="time" placeholder="time..." maxlength="300" >
         </div>
        </div>
    </div> -->
    <!-- <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Short Description&nbsp;</label>
          <textarea  name="short_description"  id="summernote2"  placeholder="short description ..." ></textarea>
        </div>
       </div>
    </div>
    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Description&nbsp;</label>
           <textarea  name="description"  id="summernote" placeholder="description ..." ></textarea>
         </div>
        </div>
    </div> -->
    </div>

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Description</label>
          <textarea  name="description"  id="summernote" placeholder="description ..." ></textarea>
        </div>
       </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      <a href="<?php echo base_url(); ?>news_and_blog">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>
	<!-- Page specific script -->
	<script>

  $(document).ready(function(){
    div_hide_show();
    $("#is_link_url").change(function(){
       div_hide_show();
     });
  });

  function div_hide_show()
  {
    var url=$('#is_link_url').val();
    if(url=='1')
    {
      $('#div_url').show();
    //  $('#google_map').addClass("embadedNameValidate");

      // $('#div_venue').show();
      // $('#venue').addClass("nameValidate");
      // $('#time').addClass("nameValidate");

    }else{
       // $('#url').removeClass("embadedNameValidate");
       // $('#venue').removeClass("nameValidate");
       // $('#time').removeClass("nameValidate");

       $('#url').val('');
       // $('#url').val('');
       // $('#time').val('');

       // $('#div_venue').hide();
       $('#div_url').hide();

    }
  }

	</script>
