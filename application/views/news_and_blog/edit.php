
<style>
.mailbox-attachments li {
    border: 1px solid #eee;
    /* float: left; */
    margin-bottom: 10px;
    margin-right: 10px;
    /* width: 200px; */
    width: 30%;
    margin: auto;
}
</style>


<!-- /.card-body -->
   <!-- <?php if(!empty($row->photo_location) &&  $row->photo_location!=''){?>
          <?php
              $ext = explode('.',$row->photo_location);
              $end_ext=end($ext);
           ?>
            <div class="card-footer bg-white">
              <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
							 <?php if($end_ext=='pdf'){?>
                <li>
                  <span class="mailbox-attachment-icon"><i class="far fa-file-pdf"></i></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"> <?php echo $row->photo_location; ?></a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.00 MB</span>
                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/newsblog/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>
							<?php }elseif ($end_ext=='csv' || $end_ext=='doc' || $end_ext=='docx') { ?>
                <li>
                  <span class="mailbox-attachment-icon"><i class="far fa-file-word"></i></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fas fa-paperclip"></i> <?php echo $row->photo_location; ?></a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.00 MB</span>
                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/newsblog/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>
								<?php }else { ?>
                <li>
                  <span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>core_media/adminpanel/dist/img/newsblog/<?php echo $row->photo_location; ?>" alt="Attachment"></span>

                  <div class="mailbox-attachment-info">
                    <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/newsblog/<?php echo $row->photo_location; ?>" class="mailbox-attachment-name"> <?php echo $row->photo_location; ?></a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.00 MB</span>
                          <a target="_blank" href="<?php echo base_url(); ?>core_media/adminpanel/dist/img/newsblog/<?php echo $row->photo_location; ?>" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                  </div>
                </li>
                 <?php } ?>
              </ul>
            </div>
					  <?php } ?> -->
            <!-- /.card-footer -->
  <form id="quickForm"  class="formSubmit" action="<?php echo base_url(); ?>news_and_blog/edit/<?php echo $row->id; ?>" method="post"  enctype="multipart/form-data">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1"> Events Category&nbsp;<span class="required_label">*</span></label>
            <select class="form-control select2 ddlErrorMsg" name="category_id" style="width:100%;">
              <option value="">-Please Select--</option>
              <?php
                 if (count($category)) {
                     foreach ($category as $list) {
                         ?>
                         <option value="<?php echo $list['id']; ?>" <?php if($list['id']==$row->category_id){echo "selected";} ?>><?php echo $list['name']; ?></option>
                     <?php
                     }
                 }
                 ?>
            </select>
          </div>
         </div>
 		 </div>
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
					 <label for="exampleInputEmail1"> Title&nbsp;<span class="required_label">*</span></label>
           <textarea  name="title" class="form-control"  required id="summernote2" placeholder="description ..." ><?php echo $row->title; ?></textarea>
           <!-- <input type="text" name="title" class="form-control nameValidate" id="title" placeholder="title..." maxlength="300" value="<?php echo $row->title; ?>"> -->
         </div>
        </div>

		 </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
          <label for="exampleInputEmail1"> Year&nbsp;<span class="required_label">*</span></label>
          <input type="number" name="year" class="form-control numberfieldErrorMsg" id="year" value="<?php echo $row->year; ?>" placeholder="year..." min="0">
								</div>
						</div>
      <div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<label for="exampleInputEmail1"> Date&nbsp;<span class="required_label">*</span></label>
					<div class="input-group" id="reservationdate1" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input requiredDateErrMsg" value="<?php echo $row->date; ?>" name="date" data-target="#reservationdate1"/>
								<div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
						</div>
				 </div>
       </div>
    </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Events Type&nbsp;<span class="required_label">*</span></label>
          <select class="form-control select2 ddlErrorMsg" name="type" id="type" style="width:100%;">
            <!-- <option value="B">Blog</option>
            <option value="E">Events</option> -->
            <option value="N" <?php if('N'==$row->type){echo "selected";} ?>>News</option>
          </select>
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Is URL?&nbsp;</label>
           <select class="form-control select2" name="is_link_url" id="is_link_url" style="width:100%;">
             <option value="0" <?php if('0'==$row->is_link_url){echo "selected";} ?>>No</option>
             <option value="1" <?php if('1'==$row->is_link_url){echo "selected";} ?>>Yes</option>
           </select>
         </div>
        </div>
    </div>

    <!-- <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Youtube Embedded Link&nbsp;</label>
          <input type="text" name="embedded_youtube" class="form-control" id="embedded_youtube" placeholder="youtube embedded link..." value="<?php echo $row->embedded_youtube; ?>" maxlength="500">
        </div>
       </div>
    </div> -->

    <div class="row" id="div_url">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> URL</label>
          <textarea class="form-control" name="url" id="url"  placeholder="url ..."><?php echo $row->url; ?></textarea>
        </div>
       </div>
     </div>
     <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            <label for="exampleInputEmail1">Description</label>
            <textarea  name="description"  id="summernote" placeholder="description ..." ><?php echo $row->description; ?></textarea>
          </div>
         </div>
     </div>
     <div>
       Please use this link: <span class="required_label"><?php echo base_url();?>homes/news_details/<?php echo $row->id; ?></span>
     </div>
    <!-- <div class="row" id="div_file">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
        <label for="customFile">File</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="txt_photo_location" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
        </div>
       </div>
    </div> -->
    <!-- <div class="row" id="div_youtube">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Embaded Yoube Link</label>
          <textarea class="form-control" rows="3"  name="youtube_link" id="youtube_link"  placeholder="embaded youtube link ..."><?php echo $row->youtube_link; ?></textarea>

        </div>
       </div>
    </div> -->
    <!-- <div class="row" id="div_google_map">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Embaded Google Map</label>
          <textarea class="form-control" rows="3"  name="google_map" id="google_map"  placeholder="embaded google map ..."><?php echo $row->google_map; ?></textarea>
        </div>
       </div>
    </div>
    <div class="row" id="div_venue">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> Venue&nbsp;<span class="required_label">*</span></label>
          <input type="text" name="venue" class="form-control nameValidate" id="venue" placeholder="venue..." maxlength="300" value="<?php echo $row->venue; ?>">
        </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1"> Time&nbsp;<span class="required_label">*</span></label>
           <input type="text" name="time" class="form-control nameValidate" id="time" placeholder="time..." maxlength="300" value="<?php echo $row->time; ?>">
         </div>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Short Description&nbsp;<span class="required_label">*</span></label>
          <textarea class="form-control embadedNameValidate" rows="3"  name="short_description" id="short_description" maxlength="150" placeholder="short description ..."><?php echo $row->short_description; ?></textarea>
        </div>
       </div>
    </div>
    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="form-group">
           <label for="exampleInputEmail1">Description&nbsp;<span class="required_label">*</span></label>
           <textarea  name="description"  id="summernote" required placeholder="description ..." ><?php echo $row->description; ?></textarea>
         </div>
        </div>
    </div> -->
    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <!-- <input type="hidden" name="old_photo_location" value="<?php echo $row->photo_location; ?>" /> -->
      <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
      <button type="button" id="btnReset" class="btn btn-info float-right">Clear</button>
      <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Update</button>
      <a href="<?php echo base_url(); ?>news_and_blog">
      <button type="button" class="btn btn-danger float-right" style="margin-right: 10px;">Cancel</button>
     </a>

    </div>
  </form>

	<!-- Page specific script -->
  <script>

  $(document).ready(function(){
    div_hide_show();
    $("#is_link_url").change(function(){
       div_hide_show();
     });
  });

  function div_hide_show()
  {
    var url=$('#is_link_url').val();
    if(url=='1')
    {
      $('#div_url').show();
    //  $('#google_map').addClass("embadedNameValidate");

      // $('#div_venue').show();
      // $('#venue').addClass("nameValidate");
      // $('#time').addClass("nameValidate");

    }else{
       // $('#url').removeClass("embadedNameValidate");
       // $('#venue').removeClass("nameValidate");
       // $('#time').removeClass("nameValidate");

       $('#url').val('');
       // $('#url').val('');
       // $('#time').val('');

       // $('#div_venue').hide();
       $('#div_url').hide();

    }
  }

	</script>
