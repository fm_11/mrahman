<?php

class Text_preview
{
    private $CI;

    function __construct()
    {
        $this->CI = get_instance();
    }

    function txt_preview($text,$number){


        if(!empty($text) && mb_strlen($text)>$number)
        {
          $text_only_spaces = preg_replace('/\s+/', ' ', $text);
        //  $text_truncated = mb_substr($text_only_spaces, 0, mb_strrpos($text_only_spaces, ' ', $number),'utf-8');
          $text_truncated = mb_substr($text_only_spaces, 0,  $number,'utf-8');
            if(empty($text_truncated))
            {
              return substr($text_only_spaces, 0, strrpos($text_only_spaces, " ")).'...';
            }
          $preview = trim(mb_substr($text_truncated, 0, mb_strrpos($text_truncated, ' '))).'...';
          return $preview;
        }
        return $text;
    }
}
