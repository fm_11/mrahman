<?php

class Language_number_convert
{
    private $CI;

    function __construct()
    {
        $this->CI = get_instance();
    }

    function en2bn($number){
      $search_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
      $replace_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
      $bn_number = str_replace($search_array, $replace_array, $number);
      return $bn_number;
    }
}