<?php

class User_role extends CI_Model
{
			public function __construct()
			{
				// Call the Model constructor
				parent::__construct();
			}
	    function get_list()
	    {
	        return $this->db->query("SELECT * FROM `user_roles` ORDER BY `id`")->result_array();
	    }

	    function add($data)
	    {
	        return $this->db->insert('user_roles', $data);
	    }


	    function edit($data, $id)
	    {
	        return $this->db->update('user_roles', $data, array('id' => $id));
	    }


	    function read($role_id)
	    {
	        return $this->db->get_where('user_roles', array('id' => $role_id))->row();
	    }

	    function delete($role_id)
	    {
	        return $this->db->delete('user_roles', array('id' => $role_id));
	    }
			public function checkifexist_role_name($name)
			{
				 $count=$this->db->get_where('user_roles', array('role_name'=> $name))->num_rows();
				if((int)$count==0){return 0;}else{return 1;}
			}
			public function checkifexist_update_role_name($name,$id)
			{
				 $count=$this->db->get_where('user_roles', array('id !='=> $id  , 'role_name'=> $name))->num_rows();
				if((int)$count==0){return 0;}else{return 1;}
			}
			function get_user_list()
			{
				$this->db->select('u.*,r.role_name');
				$this->db->from('tbl_user as u');
				$this->db->join('user_roles AS r', 'u.user_role=r.id','left');
				$this->db->order_by("u.id", "desc");
				$query = $this->db->get();
				return $query->result_array();
			}
			function add_user($data)
		 {
				 return $this->db->insert('tbl_user', $data);
		 }


		 function edit_user($data, $id)
		 {
				 return $this->db->update('tbl_user', $data, array('id' => $id));
		 }


		 function read_user($id)
		 {
				 return $this->db->get_where('tbl_user', array('id' => $id))->row();
		 }
		 function delete_user($id)
		 {
			   $this->db->delete('tbl_user_pass_history', array('user_id' => $id));
				 return $this->db->delete('tbl_user', array('id' => $id));
		 }
		 public function checkifexist_user_name($user_name)
		 {
			   $count=$this->db->get_where('tbl_user', array('user_name'=> $user_name))->num_rows();
			 if((int)$count==0){return 0;}else{return 1;}
		 }
		 public function checkifexist_update_user_name($user_name,$id)
		 {
			   $count=$this->db->get_where('tbl_user', array('id !='=> $id  , 'user_name'=> $user_name))->num_rows();
			 if((int)$count==0){return 0;}else{return 1;}
		 }
		public function checkifexist_employee_id($employee_id)
		{
		 $count=$this->db->get_where('tbl_user', array('employee_id'=> $employee_id))->num_rows();
			if((int)$count==0){return 0;}else{return 1;}
		}
		public function checkifexist_update_employee_id($employee_id,$id)
		{
	  $count=$this->db->get_where('tbl_user', array('id !='=> $id  , 'employee_id'=> $employee_id))->num_rows();
			if((int)$count==0){return 0;}else{return 1;}
		}
		public function checkifexist_user_for_any_dependency_by_user_id($id)
		{
			$error_msg="";
		 $user=	$this->db->query("SELECT `employee_id` FROM `tbl_user` where `id`=$id")->row();
		 if($user->employee_id>0)
		 {
       $error_msg="This user cannot be deleted due to employee dependency.Please select no employee from edit user form and update user information.Then delete user";
		 }
     return 	$error_msg;
		}
		public function compare_current_password_by_user_id($user_id,$md5_password)
		{
			 $count=$this->db->get_where('tbl_user', array('id ='=> $user_id  , 'user_password'=> $md5_password))->num_rows();
			 if((int)$count==0){return 0;}else{return 1;}
		}


}
