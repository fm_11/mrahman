<?php

class Admin_login extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }


    public function getBanglaDateTime($str){
		header('Content-Type: text/html; charset=utf-8');
		/*Date show end code*/
		$en = array(1,2,3,4,5,6,7,8,9,0);
		$bn = array('১','২','৩','৪','৫','৬','৭','৮','৯','০');
		$str = str_replace($en, $bn, $str);
		$en = array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );
		$en_short = array( 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
		$bn = array( 'জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'অগাস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর' );
		$str = str_replace( $en, $bn, $str );
		$str = str_replace( $en_short, $bn, $str );
		$en = array('Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday');
		$en_short = array('Sat','Sun','Mon','Tue','Wed','Thu','Fri');
		$bn_short = array('শনি', 'রবি','সোম','মঙ্গল','বুধ','বৃহঃ','শুক্র');
		$bn = array('শনিবার','রবিবার','সোমবার','মঙ্গলবার','বুধবার','বৃহস্পতিবার','শুক্রবার');
		$str = str_replace( $en, $bn, $str );
		$str = str_replace( $en_short, $bn_short, $str );
		$en = array( 'am', 'pm' );
		$bn = array( 'পূর্বাহ্ন', 'অপরাহ্ন' );
		$str = str_replace( $en, $bn, $str );
		return $str;
	}

	public function getWebsiteConfig(){
		$website_config = $this->db->query("SELECT * FROM tbl_website_config  AS wc;")->row();
		if(empty($website_config)){
			$sdata['exception'] = "Sorry ! Website configuration not found";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		return $website_config;
	}
  public function get_all_registration_index_list($limit, $offset, $value = '')
	{
		// print_r($value);
		// die();
			$this->db->select('r.*');
			$this->db->from('tbl_registration_from as r');
			if (isset($value) && !empty($value) && isset($value['cname']) && $value['cname'] != '') {
          $this->db->group_start();
          $this->db->like('r.name', $value['cname']);
          $this->db->or_like('r.mobile', $value['cname']);
          $this->db->or_like('r.payment_number', $value['cname']);
          $this->db->or_like('r.ticket_number', $value['cname']);
          $this->db->group_end();
			}
			if (isset($value) && !empty($value) && isset($value['payment_number'])  && $value['payment_number'] != '') {
					$this->db->where('r.`payment_number`', $value['payment_number']);
			}
			if (isset($value) && !empty($value) && isset($value['reference_number'])  && $value['reference_number'] != '') {
					$this->db->where('r.`transaction_id`', $value['reference_number']);
			}
      if (isset($value) && !empty($value) && isset($value['status'])  && $value['status'] != '') {
          $this->db->where('r.`status`', $value['status']);
      }
      if (isset($value) && !empty($value) && isset($value['registration_type'])  && $value['registration_type'] != '') {
          $this->db->where('r.`registration_type`', $value['registration_type']);
      }
			$this->db->order_by("r.ticket_number asc");
	//		$this->db->order_by("s.order_no", "asc");
			if (isset($limit) && $limit > 0) {
					$this->db->limit($limit, $offset);
			}
			$query = $this->db->get();
			return $query->result_array();
	}

  public function checkifexist_spot_ticket_number($ticket_number)
  {
    $count=0;
   if(!empty($ticket_number) && $ticket_number!='')
   {
     $count=$this->db->get_where('tbl_registration_from', array('ticket_number='=> $ticket_number))->num_rows();
     if((int)$count==0){}else{return 1;}
     $count=$this->db->get_where('tbl_registration_from', array('baby_ticket_1='=> $ticket_number))->num_rows();
     if((int)$count==0){}else{return 1;}
     $count=$this->db->get_where('tbl_registration_from', array('baby_ticket_2='=> $ticket_number))->num_rows();
     if((int)$count==0){}else{return 1;}
     $count=$this->db->get_where('tbl_registration_from', array('baby_ticket_3='=> $ticket_number))->num_rows();
     if((int)$count==0){}else{return 1;}
     $count=$this->db->get_where('tbl_registration_from', array('guest_ticket_1='=> $ticket_number))->num_rows();
     if((int)$count==0){}else{return 1;}
     $count=$this->db->get_where('tbl_registration_from', array('guest_ticket_2='=> $ticket_number))->num_rows();
     if((int)$count==0){}else{return 1;}
     $count=$this->db->get_where('tbl_registration_from', array('guest_ticket_3='=> $ticket_number))->num_rows();
   }
    if((int)$count==0){return 0;}else{return 1;}
  }
  public function get_organization_info()
  {
      return $this->db->get('tbl_contact_info')->row();
  }
  function edit_organization_info($data, $id)
  {
      return $this->db->update('tbl_contact_info', $data, array('id' => $id));
  }
  function get_designation_list()
  {
      return $this->db->query("SELECT * FROM `designation` ORDER BY `id`")->result_array();
  }
  function add_designation($data)
  {
      return $this->db->insert('designation', $data);
  }


  function edit_designation($data, $id)
  {
      return $this->db->update('designation', $data, array('id' => $id));
  }


  function read_designation($id)
  {
      return $this->db->get_where('designation', array('id' => $id))->row();
  }

  function delete_designation($id)
  {
      return $this->db->delete('designation', array('id' => $id));
  }
  public function checkifexist_designation_name($name)
  {
    $count=$this->db->get_where('designation', array('name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }
  public function checkifexist_update_designation_name($name,$id)
  {
    $count=$this->db->get_where('designation', array('id !='=> $id  , 'name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }
  public function checkifexist_designation_dependency_for_delete($id)
  {
    $message="";
       $count=$this->db->get_where('tbl_employee', array('designation_id'=> $id))->num_rows();
    if((int)$count>0){
      $message="This designation cannot be deleted due to employee dependency";
    }
    return $message;
  }
  function get_branch_list()
  {
      return $this->db->query("SELECT * FROM `tbl_branch` ORDER BY `id`")->result_array();
  }
  function add_branch($data)
  {
      return $this->db->insert('tbl_branch', $data);
  }


  function edit_branch($data, $id)
  {
      return $this->db->update('tbl_branch', $data, array('id' => $id));
  }


  function read_branch($id)
  {
      return $this->db->get_where('tbl_branch', array('id' => $id))->row();
  }

  function delete_branch($id)
  {
      return $this->db->delete('tbl_branch', array('id' => $id));
  }
  public function checkifexist_branch_name($name)
  {
      $count=$this->db->get_where('tbl_branch', array('name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }
  public function checkifexist_update_branch_name($name,$id)
  {
      $count=$this->db->get_where('tbl_branch', array('id !='=> $id  , 'name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }
  public function checkifexist_branch_dependency_for_delete($id)
  {
    $message="";
    $count =   $count=$this->db->get_where('tbl_employee', array('branch_id'=> $id))->num_rows();
    if((int)$count>0){
      $message="This branch cannot be deleted due to employee dependency";
    }
    return $message;
  }

  function get_gender_list()
  {
      return $this->db->query("SELECT * FROM `tbl_gender` ORDER BY `id`")->result_array();
  }
  function get_blood_group_list()
  {
      return $this->db->query("SELECT * FROM `tbl_blood_group` ORDER BY `id`")->result_array();
  }
  function get_faq_category_list()
  {
      return $this->db->query("SELECT * FROM `tbl_faq_category` ORDER BY `order_no`")->result_array();
  }
  function add_faq_category($data)
  {
      return $this->db->insert('tbl_faq_category', $data);
  }

  function edit_faq_category($data, $id)
  {
      return $this->db->update('tbl_faq_category', $data, array('id' => $id));
  }

  function read_faq_category($id)
  {
      return $this->db->get_where('tbl_faq_category', array('id' => $id))->row();
  }

  function delete_faq_category($id)
  {
      return $this->db->delete('tbl_faq_category', array('id' => $id));
  }

  public function checkifexist_faq_category_name($name)
  {
    $count =   $count=$this->db->get_where('tbl_faq_category', array('name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }

  public function checkifexist_update_faq_category_name($name,$id)
  {
      $count=$this->db->get_where('tbl_faq_category', array('id !='=> $id  , 'name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }
  public function checkifexist_dependency_for_faq_category($faq_category_id)
  {
     $count=$this->db->get_where('tbl_faq', array('faq_category_id'=> $faq_category_id))->num_rows();
    if((int)$count==0){
      return "";
    }else{
      return "This faq category cannot be deleted due to FAQ dependency.";
    }
  }
  function get_faq_list()
  {
    $this->db->select('f.*,c.name as faq_category');
    $this->db->from('tbl_faq as f');
    $this->db->join('tbl_faq_category AS c', 'f.faq_category_id=c.id');
    $this->db->order_by("c.order_no asc, f.order_no asc");
    $query = $this->db->get();
    return $query->result_array();
  }

  function add_faq($data)
  {
      return $this->db->insert('tbl_faq', $data);
  }

  function edit_faq($data, $id)
  {
      return $this->db->update('tbl_faq', $data, array('id' => $id));
  }

  function read_faq($id)
  {
      return $this->db->get_where('tbl_faq', array('id' => $id))->row();
  }

  function delete_faq($id)
  {
      return $this->db->delete('tbl_faq', array('id' => $id));
  }

  public function get_faq_max_order_by_faq_category_id($faq_category_id)
  {
      $result=$this->db->query("SELECT IFNULL(MAX(`order_no`),0) AS order_no FROM `tbl_faq` WHERE `faq_category_id`=$faq_category_id")->row();
      return $result->order_no;
  }
  function get_chairman_message()
  {
      return $this->db->query("SELECT * FROM `tbl_chairman_message`")->row();
  }
  function add_chairman_message($data)
  {
      return $this->db->insert('tbl_chairman_message', $data);
  }

  function edit_chairman_message($data, $id)
  {
      return $this->db->update('tbl_chairman_message', $data, array('id' => $id));
  }
  function get_max_reference_no()
  {

      $data1=$this->db->query("SELECT ISNULL(MAX(`reference_id`)) AS total FROM  `tbl_gallery`")->row();
      if($data1->total==1)
      {
        return 1;
      }
      $data=$this->db->query("SELECT MAX(`reference_id`) AS total FROM  `tbl_gallery`")->row();
      return $data->total+1;
  }
  public function get_all_galley_list($value = '')
  {

      $this->db->select('g.title,g.reference_id,g.`date`');
      $this->db->from('`tbl_gallery AS g');

      if (isset($value) && !empty($value) && isset($value['reference_id']) && $value['reference_id'] != '') {
          $this->db->like('g.`reference_id`', $value['reference_id']);
      }
      $this->db->group_by(array("g.`title`","g.`reference_id`","g.`date`"));
      $this->db->order_by("g.id", "desc");
      // if (isset($limit) && $limit > 0) {
      //     $this->db->limit($limit, $offset);
      // }
      $query = $this->db->get();
      $data= $query->result_array();
  //
      foreach( $data as $key=>$each ){
          $id=$each['reference_id'];
          $data[$key]['file_list']   = $this->db->query("SELECT * FROM `tbl_gallery`  WHERE `reference_id`=$id")->result_array();
      }
      return $data;
  }
  function read_gallery($id)
  {
      return $this->db->get_where('tbl_gallery', array('id' => $id))->row();
  }
  function read_gallery_by_referenceId($reference_id)
  {
      return $this->db->get_where('tbl_gallery', array('reference_id' => $reference_id))->result_array();
  }
  function edit_gallery_by_referenceId($data, $reference_id)
  {
      return $this->db->update('tbl_gallery', $data, array('reference_id' => $reference_id));
  }
  function delete_gallery($id)
  {
      return $this->db->delete('tbl_gallery', array('id' => $id));
  }
  function delete_gallery_by_referenceId($reference_id)
  {
      return $this->db->delete('tbl_gallery', array('reference_id' => $reference_id));
  }
  function get_career_list()
  {
    $this->db->select('c.*,d.name as designation');
    $this->db->from('tbl_career as c');
    $this->db->join('designation AS d', 'c.designation_id=d.id');
    $this->db->order_by("c.id desc");
    $query = $this->db->get();
    return $query->result_array();
  }
  function add_career($data)
  {
      return $this->db->insert('tbl_career', $data);
  }
  function read_career($id)
  {
      return $this->db->get_where('tbl_career', array('id' => $id))->row();
  }
  function edit_career_by_id($data, $id)
  {
      return $this->db->update('tbl_career', $data, array('id' => $id));
  }
  function delete_career($id)
  {
      return $this->db->delete('tbl_career', array('id' => $id));
  }
  function generateCareerCode() {
   $max_mem= $this->db->query("SELECT MAX(`career_code`) AS career_code FROM `tbl_career`")->row();
    $num=0;
   if(!empty($max_mem))
   {
     $num = ltrim($max_mem->career_code, '0');
   }
    $num++;
    return sprintf("%'.06d\n", $num);
  }

  public function get_all_cv_list()
  {
      $this->db->select('cv.`name`,cv.`email`,cv.`date`,cv.`location`,cv.`message`,c.`career_code`,d.`name` as designation,cv.id');
      $this->db->from('tbl_cv_list as cv');
      $this->db->join('tbl_career AS c', 'cv.career_id=c.id');
      $this->db->join('designation AS d', 'c.designation_id=d.id');
      $query = $this->db->get();
      return $query->result_array();
  }
  public function get_all_program_list()
  {
      return $this->db->query("SELECT * FROM `tbl_program`")->result_array();
  }
  function read_mail_configuration()
  {
      return $this->db->query("SELECT * FROM `tbl_mail_configuration`")->row();
  }
  function add_mail_configuration($data)
  {
      return $this->db->insert('tbl_mail_configuration', $data);
  }
  function edit_mail_configuration($data, $id)
  {
      return $this->db->update('tbl_mail_configuration', $data, array('id' => $id));
  }

  //Organizations

  function get_ogranizations_list()
  {
      return $this->db->query("SELECT * FROM `tbl_organizations`")->result_array();
  }

  public function checkifexist_add_ogranizations($name)
  {
      $count=$this->db->get_where('tbl_organizations', array('name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }

  function add_organizations($data)
  {
      return $this->db->insert('tbl_organizations', $data);
  }

  public function checkifexist_update_organizations($name,$id)
  {
      $count=$this->db->get_where('tbl_organizations', array('id !='=> $id  , 'name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }

  function edit_organizations($data, $id)
  {
      return $this->db->update('tbl_organizations', $data, array('id' => $id));
  }

  function read_organizations($id)
  {
      return $this->db->get_where('tbl_organizations', array('id' => $id))->row();
  }

  function delete_organizations($id)
  {
      return $this->db->delete('tbl_organizations', array('id' => $id));
  }

  public function get_all_video_galley_list($value = '')
  {

      $this->db->select('v.*,c.name as category_name');
      $this->db->from('`tbl_video_gallery AS v');
      $this->db->join('tbl_video_gallery_category AS c', 'v.video_category_id=c.id','left');

      if (isset($value) && !empty($value) && isset($value['video_category_id']) && $value['video_category_id'] != '') {
          $this->db->like('v.`video_category_id`', $value['video_category_id']);
      }
      if (isset($value) && !empty($value) && isset($value['is_active']) && $value['is_active'] != '') {
          $this->db->like('v.`is_active`', $value['is_active']);
      }
      $this->db->order_by("v.id", "desc");
      // if (isset($limit) && $limit > 0) {
      //     $this->db->limit($limit, $offset);
      // }
      $query = $this->db->get();
      $data= $query->result_array();
      return $data;
  }
  function add_video_gallery($data)
  {
      return $this->db->insert('tbl_video_gallery', $data);
  }
  function read_video_gallery($id)
  {
      return $this->db->get_where('tbl_video_gallery', array('id' => $id))->row();
  }
  function edit_video_gallery_by_Id($data, $id)
  {
      return $this->db->update('tbl_video_gallery', $data, array('id' => $id));
  }
  function delete_video_gallery($id)
  {
      return $this->db->delete('tbl_video_gallery', array('id' => $id));
  }

  public function checkifexist_video_gallery_code($link)
  {
    $count =   $count=$this->db->get_where('tbl_video_gallery', array('link'=> $link))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }

  public function checkifexist_update_video_gallery_code($link,$id)
  {
      $count=$this->db->get_where('tbl_video_gallery', array('id !='=> $id  , 'link'=> $link))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }

  // Board Types
  function get_board_types_list()
  {
    $this->db->select('b.*');
    $this->db->from('tbl_board_types as b');
    $this->db->order_by("b.id asc");
    $query = $this->db->get();
    return $query->result_array();
  }

  function add_board_types($data)
  {
      return $this->db->insert('tbl_board_types', $data);
  }

  function edit_board_types($data, $id)
  {
      return $this->db->update('tbl_board_types', $data, array('id' => $id));
  }

  function read_board_types($id)
  {
      return $this->db->get_where('tbl_board_types', array('id' => $id))->row();
  }
  public function checkifexist_dependency_for_board_types($board_type_id)
  {
     $count=$this->db->get_where('tbl_employee', array('board_type_id'=> $board_type_id))->num_rows();
    if((int)$count==0){
      return "";
    }else{
      return "This Board Type cannot be deleted due to Emplpyee dependency.";
    }
  }
  function delete_board_types($id)
  {
      return $this->db->delete('tbl_board_types', array('id' => $id));
  }
  //Progress board
  function get_progress_board_category_list($limit, $offset, $value = '')
  {
      $this->db->select('b.*');
      $this->db->from('tbl_progress_board_category as b');
      //$this->db->join('designation AS d', 'e.designation_id=d.id');
      if (isset($value) && !empty($value) && isset($value['is_active'])  && $value['is_active'] != '') {
        $this->db->where('n.is_active', $value['is_active']);
      }
      if (isset($limit) && $limit > 0) {
					$this->db->limit($limit, $offset);
			}
			$this->db->order_by('b.order_no desc');
			$query = $this->db->get();
			return $query->result_array();
  }

  function add_progress_board_category($data)
  {
      return $this->db->insert('tbl_progress_board_category', $data);
  }
  function edit_progress_board_category($data, $id)
  {
      return $this->db->update('tbl_progress_board_category', $data, array('id' => $id));
  }


  function read_progress_board_category($id)
  {
      return $this->db->get_where('tbl_progress_board_category', array('id' => $id))->row();
  }

  function delete_progress_board_category($id)
  {
      return $this->db->delete('tbl_progress_board_category', array('id' => $id));
  }
  public function checkifexist_progress_board_category_name($name)
  {
      $count=$this->db->get_where('tbl_progress_board_category', array('name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }
  public function checkifexist_update_progress_board_category_name($name,$id)
  {
      $count=$this->db->get_where('tbl_progress_board_category', array('id !='=> $id  , 'name'=> $name))->num_rows();
    if((int)$count==0){return 0;}else{return 1;}
  }

 public function checkifexist_progress_board_category_for_any_dependency_by_id($id)
 {
   $error_msg="";
  $user=	$this->db->query("SELECT `progress_board_category_id` FROM `tbl_progress_board_details` where `progress_board_category_id`=$id")->row();
  if($user->progress_board_category_id>0)
  {
    $error_msg="This category cannot be deleted due to news and blog dependency.Please first delete progress board details information.Then delete category";
  }
  return 	$error_msg;
 }
 //progress board Details
 function get_progress_board_details_list($limit, $offset, $value = '')
 {
     $this->db->select('d.*,c.name as category_name');
     $this->db->from('tbl_progress_board_details as d');
     $this->db->join('tbl_progress_board_category AS c', 'd.progress_board_category_id=c.id');
     if (isset($value) && !empty($value) && isset($value['is_active'])  && $value['is_active'] != '') {
       $this->db->where('c.is_active', $value['is_active']);
     }
     if (isset($limit) && $limit > 0) {
         $this->db->limit($limit, $offset);
     }
     $this->db->order_by('d.date desc');
     $query = $this->db->get();
     return $query->result_array();
 }

 function add_progress_board_details($data)
 {
     return $this->db->insert('tbl_progress_board_details', $data);
 }
 function edit_progress_board_details($data, $id)
 {
     return $this->db->update('tbl_progress_board_details', $data, array('id' => $id));
 }


 function read_progress_board_details($id)
 {
     return $this->db->get_where('tbl_progress_board_details', array('id' => $id))->row();
 }

 function delete_progress_board_details($id)
 {
     return $this->db->delete('tbl_progress_board_details', array('id' => $id));
 }
 function get_progress_board_details_for_home()
 {
       $data=$this->db->query("SELECT * FROM `tbl_progress_board_category` ORDER BY `order_no` LIMIT 4")->result_array();
       foreach( $data as $key=>$each ){
           $id=$each['id'];
           $data[$key]['progress']   = $this->db->query("SELECT COUNT(`id`) AS `events`,IFNULL(SUM(`participants`),0) AS `people`  FROM tbl_progress_board_details WHERE progress_board_category_id='$id'")->result_array();
       }
       return $data;
 }
 function get_all_progress_board_details_by_progress_board()
 {
       $data=$this->db->query("SELECT * FROM `tbl_progress_board_category` ORDER BY `order_no`")->result_array();
       foreach( $data as $key=>$each ){
           $id=$each['id'];
           $data[$key]['progress']   = $this->db->query("SELECT * FROM tbl_progress_board_details WHERE progress_board_category_id='$id' order by `date`")->result_array();
       }
       return $data;
 }
 //end progress board details
 //video category_name
 function get_video_category_list()
 {
     return $this->db->query("SELECT * FROM `tbl_video_gallery_category` ORDER BY `order_no`")->result_array();
 }
 function add_video_category($data)
 {
     return $this->db->insert('tbl_video_gallery_category', $data);
 }

 function edit_video_category($data, $id)
 {
     return $this->db->update('tbl_video_gallery_category', $data, array('id' => $id));
 }

 function read_video_category($id)
 {
     return $this->db->get_where('tbl_video_gallery_category', array('id' => $id))->row();
 }

 function delete_video_category($id)
 {
     return $this->db->delete('tbl_video_gallery_category', array('id' => $id));
 }

 public function checkifexist_video_category_name($name)
 {
   $count =   $count=$this->db->get_where('tbl_video_gallery_category', array('name'=> $name))->num_rows();
   if((int)$count==0){return 0;}else{return 1;}
 }

 public function checkifexist_update_video_category_name($name,$id)
 {
     $count=$this->db->get_where('tbl_video_gallery_category', array('id !='=> $id  , 'name'=> $name))->num_rows();
   if((int)$count==0){return 0;}else{return 1;}
 }
 public function checkifexist_dependency_for_video_category($video_category_id)
 {
    $count=$this->db->get_where('tbl_video_gallery', array('video_category_id'=> $video_category_id))->num_rows();
   if((int)$count==0){
     return "";
   }else{
     return "This video category cannot be deleted due to video gallery dependency.";
   }
 }
 function get_all_video_gallery_by_details()
 {
       $data=$this->db->query("SELECT DISTINCT c.`id`,c.`name` FROM tbl_video_gallery_category AS c INNER JOIN tbl_video_gallery AS v ON c.`id`=v.`video_category_id`
                                WHERE c.`is_active`=1 ORDER BY c.`order_no`")->result_array();
       foreach( $data as $key=>$each ){
           $id=$each['id'];
           $data[$key]['video']   = $this->db->query("SELECT * FROM tbl_video_gallery WHERE video_category_id='$id' order by id desc")->result_array();
       }
       return $data;
 }
 //end video category

 //About_me

 public function get_about_me()
 {
     return $this->db->get('tbl_about_me')->row();
 }

 function edit_about_me($data, $id)
 {
     return $this->db->update('tbl_about_me', $data, array('id' => $id));
 }

 // Teachings

 function get_teachings_list()
 {
   return $this->db->query("SELECT * FROM `tbl_teachings` ORDER BY `id`")->result_array();
 }

 function add_teachings($data)
 {
     return $this->db->insert('tbl_teachings', $data);
 }

 function edit_teachings($data, $id)
 {
     return $this->db->update('tbl_teachings', $data, array('id' => $id));
 }
 function read_teachings($id)
 {
     return $this->db->get_where('tbl_teachings', array('id' => $id))->row();
 }

 function delete_teachings($id)
 {
     return $this->db->delete('tbl_teachings', array('id' => $id));
 }

 public function get_teachings_max_order_by_menu_id($type)
 {
     $result=$this->db->query("SELECT IFNULL(MAX(`order_no`),0) AS order_no FROM `tbl_teachings` WHERE `type`='$type'")->row();
     return $result->order_no;
 }
}
