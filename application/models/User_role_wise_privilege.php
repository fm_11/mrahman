<?php

class User_role_wise_privilege extends CI_Model
{

    var $title = '';
    var $content = '';
    var $date = '';

    function __construct()
    {
        $this->load->database();
        parent::__construct();
    }

    /**
     * Generates a list of user wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To Generate a list of user wise privileges
     * @access  :   public
     * @param   :   int $offset, int $limit
     * @return  :   array
     */
    function get_list($offset, $limit)
    {
        $query = $this->db->get('user_role_wise_privileges', $offset, $limit);
        return $query->result();
    }

    function get_role_name_by_id($role_id)
    {
        return $this->db->query("SELECT `role_name` FROM `user_roles` WHERE `id` = '$role_id'")->row()->role_name;
    }

    /**
     * Counts number of rows of  user wise privileges table
     * @author  :   Amlan Chowdhury
     * @uses    :   To count number of rows of  user wise privileges table
     * @access  :   public
     * @return  :   int
     */
    function row_count()
    {
        return $this->db->count_all_results('user_role_wise_privileges');
    }

    /**
     * Adds data to user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To add data to user role wise privileges
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    function add($data)
    {
        if (isset($data['resources'])) {
            //echo '-------'.$data['role_id'].'=======';echo '<pre>';print_r($data);echo '</pre>';//die('model die');

            $this->db->trans_start();
            $this->db->delete('user_role_wise_privileges', array('role_id' => $data['role_id']));
            //$this->insert_rows('user_role_wise_privileges', $data['column_names'],$data['column_rows']);
            $this->db->insert_batch('user_role_wise_privileges', $data['resources']);
            $this->db->trans_complete();

            return $this->db->trans_status();
        } else {
            $this->db->delete('user_role_wise_privileges', array('role_id' => $data['role_id']));
            return true;
        }
    }

    /**
     * Updates data of user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To update data of user roles wise privileges
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    function edit($data)
    {
        return $this->db->update('user_role_wise_privileges', $data, array('id' => $data['id']));
    }

    /**
     * Reads data of specific user role wise privilege
     * @author  :   Amlan Chowdhury
     * @uses    :   To  read data of specific user role wise privilege
     * @access  :   public
     * @param   :   int $user_role_wise_privileges_id
     * @return  :   boolean
     */
    function read($user_role_wise_privileges_id)
    {
        return $this->db->get_where('user_role_wise_privileges', array('id' => $user_role_wise_privileges_id))->result();
    }

    /**
     * Gets data of user role wise privilege by role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   array
     */
    function get_by_role_id($role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by role id,controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by role id, controller name and action
     * @access  :   public
     * @param   :   int $role_id, string $controller, string $action
     * @return  :   array
     */
    function get_by_role_id_controller_action($role_id, $controller, $action)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id, 'controller' => $controller, 'action' => $action));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by controller name and action
     * @access  :   public
     * @param   :   string $controller, string $action
     * @return  :   array
     */
    function get_by_controller_action($controller, $action)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('controller' => $controller, 'action' => $action));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by controller name ,action and role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by controller name, action and role id
     * @access  :   public
     * @param   :   string $controller, string $action, int $role_id
     * @return  :   array
     */
    function get_by_controller_action_role($controller, $action, $role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('controller' => $controller, 'action' => $action, 'role_id' => $role_id));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privileged resourses by controller role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privileged resourses by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   array
     */
    function get_privileged_resources($role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id));
        {
            $amlan_field_officers_reports_ccdas = array();
            $results = $query->result_array();
            foreach ($results as $result) {
                if ($result['controller'] == 'amlan_field_officers_reports') {
                    $result['id'] += 1000;
                    $result['controller'] = 'amlan_field_officers_reports_ccdas';
                    array_push($amlan_field_officers_reports_ccdas, $result);
                }
            }
            foreach ($amlan_field_officers_reports_ccdas as $amlan_field_officers_reports_ccda) {
                array_push($results, $amlan_field_officers_reports_ccda);
            }
            return $results;
        }
        return $query->result_array();
    }

    /**
     * Deletes data of specific user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of specific user role wise privileges
     * @access  :   public
     * @param   :   int $user_role_wise_privileges_id
     * @return  :   boolean
     */
    function delete($user_role_wise_privileges_id)
    {
        return $this->db->delete('user_role_wise_privileges', array('id' => $user_role_wise_privileges_id));
    }

    /**
     * Deletes data of user role wise privileges by role id ,controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of user role wise privileges by role id ,controller name and action
     * @access  :   public
     * @param   :   int $role_id,string $controller, string $action
     * @return  :   boolean
     */
    function delete_by_role_id_controller_action($role_id, $controller, $action)
    {
        return $this->db->delete('user_role_wise_privileges', array('role_id' => $role_id, 'controller' => $controller, 'action' => $action));
    }

    /**
     * Deletes data of specific user role wise privileges by role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of specific user role wise privileges by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   boolean
     */
    function delete_by_role_id($role_id)
    {
        return $this->db->delete('user_role_wise_privileges', array('role_id' => $role_id));
    }

    /**
     * Checks permission of user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To  check permission of user role wise privileges
     * @access  :   public
     * @param   :   int $role_id, string $controller,string $action
     * @return  :   boolean
     */
    function check_permission($role_id, $controller, $action)
    {
        $controller = strtolower($controller);
        $action = strtolower($action);

        if ($this->is_globally_allowed_action($controller, $action)) { //if action is globally allowed, access is granted
            return true;
        } else {
            $check = $this->db->query("SELECT `id` FROM `user_role_wise_privileges` WHERE `controller` = '$controller' AND `action` = '$action' AND `role_id` = '$role_id'")->result_array();
            return !empty($check) ? true : false;
        }
    }

    /**
     * Checks if the action is globally dis-allowed or not
     * @author  :   Anis Alamgir
     * @uses    :   To check if the action is globally dis-allowed or not
     * @access  :   public
     * @param   :   string $controller,string $action
     * @return  :   boolean
     * @wiki    : http://203.188.255.195/tracker/projects/microfin360/wiki/Manage_User_Role#Only-For-Super-Admin
     */
    function is_globally_disallowed_action($controller, $action)
    {
        return isset($actions[$controller][$action]) ? true : false;
    }

    /**
     * Checks if the action is globally allowed or not
     * @author  :   Amlan Chowdhury
     * @uses    :   To check if the action is globally allowed or not
     * @access  :   public
     * @param   :   string $controller,string $action
     * @return  :   boolean
     */
    function is_globally_allowed_action($controller, $action)
    {
        $actions = array();
        $actions['login']['index'] = 1;
        $actions['login']['forgot_password'] = 1;
        $actions['login']['authentication'] = 1;
        $actions['login']['logout'] = 1;
        $actions['login']['denied'] = 1;
    		$actions['login']['change_password'] = 1;
    		$actions['login']['auto_login_from_partner_portal'] = 1;

        return isset($actions[$controller][$action]);
    }


	function get_all_resources_array($for='')
	{
		define("VIEW", "View");
		define("ADD", "Add");
		define("EDIT", "Edit");
		define("DELETE", "Delete");
		//initializing
    $i=0;
		$group_id = $i;
		$tmp = array();
		$audit_trial_list = array();
		//Organization management group
		$group_name = 'Dashboard';
		$subgroup_name = 'Dashboard';

		$entity_name = 'Dashboard';
		$controller = 'dashboard';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$audit_trial_list[$i]['entity_name'] = $entity_name;
		$audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $group_name = 'Users';
		$subgroup_name = 'Dashboard';

		$entity_name = 'User Roles';
		$controller = 'user_roles';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
		$audit_trial_list[$i]['entity_name'] = $entity_name;
		$audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'User List';
		$controller = 'users';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'updateMsgStatusEmployeeStatus', 'getEmployeeCodeByEmployeeId'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
		$audit_trial_list[$i]['entity_name'] = $entity_name;
		$audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'User Add';
    $controller = 'users';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'Change Password';
    $controller = 'users';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, array('index', 'change_password'));
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;
		#Configuration

		$group_name = 'Settings';
		$subgroup_name = 'Basic Settings';

		$entity_name = 'Organization Information';
		$controller = 'org_infos';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'my_file_upload','my_file_remove'));
		$audit_trial_list[$i]['entity_name'] = $entity_name;
		$audit_trial_list[$i]['controller'] = $controller;
    $i++;

		$entity_name = 'Designations';
		$controller = 'designations';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
		$audit_trial_list[$i]['entity_name'] = $entity_name;
		$audit_trial_list[$i]['controller'] = $controller;
    $i++;

		$entity_name = 'Branches';
		$controller = 'branches';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
		$audit_trial_list[$i]['entity_name'] = $entity_name;
		$audit_trial_list[$i]['controller'] = $controller;
    $i++;


    $group_name = 'Employees';
    $subgroup_name = 'Employees';

    $entity_name = 'Add Employee';
    $controller = 'employees';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'Employee List';
    $controller = 'employees';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'my_file_upload','my_file_remove'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $group_name = 'Our Courses';
    $subgroup_name = 'Our Courses';

    $entity_name = 'Course Category';
    $controller = 'course_categorys';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'updateMsgContentStatus'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'Course List';
    $controller = 'courses';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'updateMsgContentStatus'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'Course Shedules';
    $controller = 'course_shedules';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'updateMsgContentStatus','my_file_remove','my_file_upload','getContenMaxOrder'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $group_name = 'Blog and Events';
    $subgroup_name = 'Blog and Events';

    $entity_name = 'Events Category';
    $controller = 'events_category';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'Events and Blog';
    $controller = 'news_and_blog';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'updateMsgContentStatus','my_file_remove','my_file_upload','get_full_content_type'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $group_name = 'Instructors ';
    $subgroup_name = 'Instructors ';

    $entity_name = 'Add Instructors ';
    $controller = 'instructors';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add', 'my_file_upload'));
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'Instructors List';
    $controller = 'instructors';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'my_file_upload','my_file_remove'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $group_name = 'Partnerships';
    $subgroup_name = 'Partnerships';

    $entity_name = 'Add Partnerships';
    $controller = 'partnerships';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add', 'my_file_upload'));
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'Partnerships List';
    $controller = 'partnerships';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'my_file_upload','my_file_remove','get_full_content_type'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $group_name = 'FAQ';
    $subgroup_name = 'FAQ';

    $entity_name = 'FAQ Category';
    $controller = 'faq_category';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'FAQ';
    $controller = 'faq';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'get_faq_max_order_by_faq_category_id'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $group_name = 'Gallery ';
    $subgroup_name = 'Gallery';

    $entity_name = 'Add Gallery ';
    $controller = 'gallery';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add', 'my_file_upload'));
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'Gallery List';
    $controller = 'gallery';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'my_file_upload','my_file_remove'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, array('delete', 'delete_file'));
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $group_name = 'Documents';
    $subgroup_name = 'Documents';

    $entity_name = 'Add Documents ';
    $controller = 'documents';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add', 'my_file_upload'));
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;

    $entity_name = 'Documents List';
    $controller = 'documents';
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index', 'my_file_upload','my_file_remove','get_full_content_type','updateMsgContentStatus','getContenMaxOrder'));
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, array('delete'));
    $audit_trial_list[$i]['entity_name'] = $entity_name;
    $audit_trial_list[$i]['controller'] = $controller;
    $i++;


		if($for != ''){
			return $audit_trial_list;
		}

		return $tmp;
	}

    function create_resource(&$tmp, $group_name, $subgroup_name, $entity_name, $controller, $action_title, $actions = null)
    {
        if (is_null($actions)) {
            $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][0]['name'] = strtolower($action_title);
        } elseif (is_string($actions)) {
            $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][0]['name'] = $actions;
        } elseif (is_array($actions)) {
            foreach ($actions as $key => $row) {
                $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][$key]['name'] = $row;
            }
        }

        return $tmp;
    }

}
