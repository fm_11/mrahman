<?php

class Content extends CI_Model
{
			public function __construct()
			{
				// Call the Model constructor
				parent::__construct();
			}
	    function get_content_details_list()
	    {
				// $this->db->select('c.`id`,c.`is_active`,c.`title`,c.`order_no`,c.`content_type`,c.`menu_id`,m.name as menu_name');
				// $this->db->from('tbl_content_details as c');
				// $this->db->join('tbl_menu AS m', 'c.menu_id=m.id');
				// $this->db->order_by('m.order asc, c.order_no asc');
			$result=	$this->db->query("SELECT c.`id`,c.`is_active`,c.`title`,c.`order_no`,c.`content_type`,c.`menu_id`,m.name AS menu_name FROM `tbl_content_details` AS c
																	INNER JOIN
																	(
																	   SELECT f.`id`,(CASE WHEN f.pparent_name IS NULL THEN f.`name` ELSE CONCAT(f.`pparent_name`,' -> ',f.name) END) AS `name`,f.`order`
																	     FROM (
																		SELECT t.`id`,(CASE WHEN t.parent_name IS NULL THEN t.`name` ELSE CONCAT(t.`parent_name`,' -> ',t.name) END) AS `name`,
																		(SELECT `name` FROM `tbl_menu` WHERE `id`=t.`pparent_id`) AS pparent_name,(SELECT `order` FROM `tbl_menu` WHERE `id`=t.`pparent_id`) AS `order`
																		    FROM (
																			SELECT m.`id`, m.`name`,(SELECT `name` FROM `tbl_menu` WHERE `id`=m.`parent_id`) AS parent_name,
																			(SELECT `parent_id` FROM `tbl_menu` WHERE `id`=m.`parent_id`) AS pparent_id
																			   FROM `tbl_menu` AS m
																			   WHERE m.`is_active`=1 AND m.`link_able`=1
																		           ORDER BY m.`order` ASC
																			) AS t
																		) AS f
																	) AS m ON c.`menu_id`=m.`id`
																	ORDER BY m.`order`,c.`order_no`")->result_array();

				return $result;
	    }

	    function add_content_details($data)
	    {
	        return $this->db->insert('tbl_content_details', $data);
	    }


	    function edit_content_details($data, $id)
	    {
	        return $this->db->update('tbl_content_details', $data, array('id' => $id));
	    }


	    function read_content_details($id)
	    {
	        return $this->db->get_where('tbl_content_details', array('id' => $id))->row();
	    }

	    function delete_content_details($id)
	    {
	        return $this->db->delete('tbl_content_details', array('id' => $id));
	    }
			function get_link_able_menu()
		  {
		 		 return $this->db->query("SELECT f.`id`,(CASE WHEN f.pparent_name IS NULL THEN f.`name` ELSE CONCAT(f.`pparent_name`,' -> ',f.name) END) AS `name`
																	   FROM (
																	     SELECT t.`id`,(CASE WHEN t.parent_name IS NULL THEN t.`name` ELSE CONCAT(t.`parent_name`,' -> ',t.name) END) AS `name`,
																		        (SELECT `name` FROM `tbl_menu` WHERE `id`=t.`pparent_id`) AS pparent_name
																							 FROM (
																								SELECT m.`id`, m.`name`,(SELECT `name` FROM `tbl_menu` WHERE `id`=m.`parent_id`) AS parent_name,
																								(SELECT `parent_id` FROM `tbl_menu` WHERE `id`=m.`parent_id`) AS pparent_id
																								FROM `tbl_menu` AS m
																								WHERE m.`is_active`=1 AND m.`link_able`=1 and m.`is_url`=0
																								ORDER BY m.`order` ASC
																							      ) AS t
																		    ) AS f")->result_array();
		  }
			public function get_content_max_order_by_menu_id($menu_id)
			{
          $result=$this->db->query("SELECT IFNULL(MAX(`order_no`),0) AS order_no FROM `tbl_content_details` WHERE `menu_id`=$menu_id")->row();
					// print_r($result->order_no);
					// die;
					return $result->order_no;
			}

			function get_dentity_of_mother_concern_list()
	    {
				// $this->db->select('c.`id`,c.`is_active`,c.`title`,c.`order_no`,c.`content_type`,c.`menu_id`,m.name as menu_name');
				// $this->db->from('tbl_content_details as c');
				// $this->db->join('tbl_menu AS m', 'c.menu_id=m.id');
				// $this->db->order_by('m.order asc, c.order_no asc');
			  return $this->db->query("SELECT * FROM `tbl_identity_of_mother_concern` ORDER BY `type` asc, `order_no` asc")->result_array();
	    }

	    function add_dentity_of_mother_concern($data)
	    {
	        return $this->db->insert('tbl_identity_of_mother_concern', $data);
	    }
	    function edit_dentity_of_mother_concern($data, $id)
	    {
	        return $this->db->update('tbl_identity_of_mother_concern', $data, array('id' => $id));
	    }


	    function read_dentity_of_mother_concern($id)
	    {
	        return $this->db->get_where('tbl_identity_of_mother_concern', array('id' => $id))->row();
	    }

	    function delete_dentity_of_mother_concern($id)
	    {
	        return $this->db->delete('tbl_identity_of_mother_concern', array('id' => $id));
	    }
			function get_identity_of_mother_concern_by_type()
			{
					  $data=$this->db->query("SELECT `type` FROM `tbl_identity_of_mother_concern` GROUP BY `type`")->result_array();
						foreach( $data as $key=>$each ){
		            $type=$each['type'];
		            $data[$key]['partners']   = $this->db->query("SELECT * FROM `tbl_identity_of_mother_concern`  WHERE `type`='$type' order by `order_no`")->result_array();
		        }
		        return $data;
			}
			function get_documents_list()
	    {

			  return $this->db->query("SELECT * FROM `tbl_documents` ORDER BY `type` asc, `order_no` asc")->result_array();
	    }

	    function add_documents($data)
	    {
	        return $this->db->insert('tbl_documents', $data);
	    }

	    function edit_documents($data, $id)
	    {
	        return $this->db->update('tbl_documents', $data, array('id' => $id));
	    }

	    function read_documents($id)
	    {
	        return $this->db->get_where('tbl_documents', array('id' => $id))->row();
	    }

	    function delete_documents($id)
	    {
	        return $this->db->delete('tbl_documents', array('id' => $id));
	    }
			public function get_documents_max_order_by_menu_id($type)
			{
					$result=$this->db->query("SELECT IFNULL(MAX(`order_no`),0) AS order_no FROM `tbl_documents` WHERE `type`='$type'")->row();
					// print_r($result->order_no);
					// die;
					return $result->order_no;
			}
			// Contacts

			function get_contacts_list()
			{
				return $this->db->query("SELECT * FROM `tbl_contact`")->result_array();
			}

			function delete_contacts($id)
			{
					return $this->db->delete('tbl_contact', array('id' => $id));
			}

			//Images

			function get_images_list()
	    {
			  return $this->db->query("SELECT * FROM `tbl_image` ORDER BY `type` asc, `category` asc, `order_no` asc")->result_array();
	    }

			function add_images($data)
	    {
	        return $this->db->insert('tbl_image', $data);
	    }

			function edit_images($data, $id)
	    {
	        return $this->db->update('tbl_image', $data, array('id' => $id));
	    }

			function read_images($id)
	    {
	        return $this->db->get_where('tbl_image', array('id' => $id))->row();
	    }

			public function get_images_max_order_by_menu_id($type)
			{
					$result=$this->db->query("SELECT IFNULL(MAX(`order_no`),0) AS order_no FROM `tbl_image` WHERE `type`='$type'")->row();
					return $result->order_no;
			}

			function delete_images($id)
	    {
	        return $this->db->delete('tbl_image', array('id' => $id));
	    }
}
