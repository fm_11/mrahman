<?php

class Publication extends CI_Model
{
			public function __construct()
			{
				// Call the Model constructor
				parent::__construct();
			}
	    function get_publications_category_list()
	    {
			  return $this->db->query("SELECT * FROM `tbl_publication_category` ORDER BY `order_no`")->result_array();
	    }

	    function add_publications_category($data)
	    {
	        return $this->db->insert('tbl_publication_category', $data);
	    }
	    function edit_publications_category($data, $id)
	    {
	        return $this->db->update('tbl_publication_category', $data, array('id' => $id));
	    }


	    function read_publications_category($id)
	    {
	        return $this->db->get_where('tbl_publication_category', array('id' => $id))->row();
	    }

	    function delete_publications_category($id)
	    {
	        return $this->db->delete('tbl_publication_category', array('id' => $id));
	    }
			public function checkifexist_publications_category($name)
			{
					$count=$this->db->get_where('tbl_publication_category', array('name'=> $name))->num_rows();
				if((int)$count==0){return 0;}else{return 1;}
			}
			public function checkifexist_update_publications_category($name,$id)
			{
					$count=$this->db->get_where('tbl_publication_category', array('id !='=> $id  , 'name'=> $name))->num_rows();
				if((int)$count==0){return 0;}else{return 1;}
			}

    //Publications

		 public function checkifexist_category_for_any_dependency_by_id($id)
		 {
			 $error_msg="";
			$user=	$this->db->query("SELECT `publication_category_id` FROM `tbl_publications` where `publication_category_id`=$id")->row();
			if($user->publication_category_id>0)
			{
				$error_msg="This category cannot be deleted due to Publications dependency.<br>
				Please first delete Publication information related to this category.<br>
				Then delete category";
			}
			return 	$error_msg;
		 }

		 function get_publications_list()
		 {
			 $this->db->select('p.*,c.name as category_name');
		 	 $this->db->from('tbl_publications as p');
			 $this->db->join('tbl_publication_category AS c', 'p.publication_category_id=c.id');
			 $this->db->order_by('p.year desc, p.order_no asc');
			 $query = $this->db->get();
			 return $query->result_array();
		 }

		 function add_publications($data)
		 {
				 return $this->db->insert('tbl_publications', $data);
		 }
		 function edit_publications($data, $id)
		 {
				 return $this->db->update('tbl_publications', $data, array('id' => $id));
		 }


		 function read_publications($id)
		 {
				 return $this->db->get_where('tbl_publications', array('id' => $id))->row();
		 }

		 function delete_publications($id)
		 {
				 return $this->db->delete('tbl_publications', array('id' => $id));
		 }

		 public function get_pub_max_order_by_pub_category_id($publication_category_id)
	   {
	       $result=$this->db->query("SELECT IFNULL(MAX(`order_no`),0) AS order_no FROM `tbl_publications` WHERE `publication_category_id`=$publication_category_id")->row();
	       return $result->order_no;
	   }

		 function get_all_publications_by_details()
		 {
					 $data=$this->db->query("SELECT DISTINCT c.`id`,c.`name` FROM `tbl_publications` AS p
																	INNER JOIN `tbl_publication_category` AS c ON p.`publication_category_id`=c.`id`
																	WHERE c.`is_active`='1'
																	ORDER BY c.`order_no`")->result_array();
					 foreach( $data as $key=>$each ){
							 $id=$each['id'];
							 $year_list=$this->db->query("SELECT DISTINCT p.`publication_category_id`,p.`year` FROM `tbl_publications` AS p
																						WHERE p.`publication_category_id`='$id' AND p.`is_active`='1'
																						ORDER BY p.`year` DESC")->result_array();
							 $data[$key]['year_list']   = $year_list;
							 $total=0;

							 foreach ($year_list as  $key2=>$value) {
								 $publication_category_id=$value['publication_category_id'];
								 $year=$value['year'];
								 $data[$key]['year_list'][$key2][$year]=$this->db->query("SELECT * FROM `tbl_publications` AS p
																																	WHERE p.`publication_category_id`='$publication_category_id' AND p.`year`='$year' AND p.`is_active`='1'
																																	ORDER BY p.`order_no`")->result_array();
								$total +=count($data[$key]['year_list'][$key2][$year]);

							 }
							 $data[$key]['total_count']   = $total;

					 }
					 return $data;
		 }

}
