<?php

class News_and_event extends CI_Model
{
			public function __construct()
			{
				// Call the Model constructor
				parent::__construct();
			}
	    function get_news_and_blog_category_list()
	    {
				// $this->db->select('c.`id`,c.`is_active`,c.`title`,c.`order_no`,c.`content_type`,c.`menu_id`,m.name as menu_name');
				// $this->db->from('tbl_content_details as c');
				// $this->db->join('tbl_menu AS m', 'c.menu_id=m.id');
				// $this->db->order_by('m.order asc, c.order_no asc');
			  return $this->db->query("SELECT * FROM `tbl_news_and_blog_category` ORDER BY `order_no`")->result_array();
	    }

	    function add_news_and_blog_category($data)
	    {
	        return $this->db->insert('tbl_news_and_blog_category', $data);
	    }
	    function edit_news_and_blog_category($data, $id)
	    {
	        return $this->db->update('tbl_news_and_blog_category', $data, array('id' => $id));
	    }


	    function read_news_and_blog_category($id)
	    {
	        return $this->db->get_where('tbl_news_and_blog_category', array('id' => $id))->row();
	    }

	    function delete_news_and_blog_category($id)
	    {
	        return $this->db->delete('tbl_news_and_blog_category', array('id' => $id));
	    }
			public function checkifexist_news_and_blog_name($name)
			{
					$count=$this->db->get_where('tbl_news_and_blog_category', array('name'=> $name))->num_rows();
				if((int)$count==0){return 0;}else{return 1;}
			}
			public function checkifexist_update_news_and_blog_name($name,$id)
			{
					$count=$this->db->get_where('tbl_news_and_blog_category', array('id !='=> $id  , 'name'=> $name))->num_rows();
				if((int)$count==0){return 0;}else{return 1;}
			}

		 public function checkifexist_category_for_any_dependency_by_id($id)
		 {
			 $error_msg="";
			$user=	$this->db->query("SELECT `category_id` FROM `tbl_news_and_blog` where `category_id`=$id")->row();
			if($user->category_id>0)
			{
				$error_msg="This category cannot be deleted due to news and blog dependency.Please first delete news and blog information.Then delete category";
			}
			return 	$error_msg;
		 }
		 function get_events_and_blog_list($limit, $offset, $value = '')
		 {
			$this->db->select('n.*,c.name as category_name');
		 	$this->db->from('tbl_news_and_blog as n');
			$this->db->join('tbl_news_and_blog_category AS c', 'n.category_id=c.id');
			 if (isset($value) && !empty($value) && isset($value['course_id'])  && $value['course_id'] != '') {
				 $this->db->where('cs.course_id', $value['course_id']);
			 }
			 if (isset($value) && !empty($value) && isset($value['is_active'])  && $value['is_active'] != '') {
				 $this->db->where('n.is_active', $value['is_active']);
			 }
			 if (isset($value) && !empty($value) && isset($value['category_id'])  && $value['category_id'] != '') {
				 $this->db->where('n.category_id', $value['category_id']);
			 }
			 if (isset($value) && !empty($value) && isset($value['id'])  && $value['id'] != '') {
				 $this->db->where('n.id', $value['id']);
			 }
			 if (isset($limit) && $limit > 0) {
					 $this->db->limit($limit, $offset);
			 }
			 $this->db->order_by('c.order_no asc,n.date desc');
			 $query = $this->db->get();
			 return $query->result_array();
		 }

		 function get_news_and_blog_list()
		 {
			 $this->db->select('n.`id`,n.`is_active`,n.`title`,n.`type`,n.date,c.name as category_name');
			 $this->db->from('tbl_news_and_blog as n');
			 $this->db->join('tbl_news_and_blog_category AS c', 'n.category_id=c.id');
			 $this->db->order_by('c.order_no asc,n.date desc');
			 $query = $this->db->get();
			 return $query->result_array();
		 }

		 function add_news_and_blog($data)
		 {
				 return $this->db->insert('tbl_news_and_blog', $data);
		 }
		 function edit_news_and_blog($data, $id)
		 {
				 return $this->db->update('tbl_news_and_blog', $data, array('id' => $id));
		 }


		 function read_news_and_blog($id)
		 {
				 return $this->db->get_where('tbl_news_and_blog', array('id' => $id))->row();
		 }

		 function delete_news_and_blog($id)
		 {
				 return $this->db->delete('tbl_news_and_blog', array('id' => $id));
		 }

}
