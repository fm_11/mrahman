<?php

class Website extends CI_Model
{
	public function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	public function getInstituteInfo(){
		$list = $this->db->query("SELECT * FROM tbl_contact_info")->row();
		return $list;
	}
	public function get_gellery()
	{
		return $this->db->query("SELECT * FROM  `tbl_gallery` ORDER BY RAND ( )  LIMIT 8")->result_array();
	}
	function get_faq_details()
	{
			  $data=$this->db->query("SELECT * FROM `tbl_faq_category` ORDER BY `order_no`")->result_array();
				foreach( $data as $key=>$each ){
            $id=$each['id'];
            $data[$key]['faq_list']   = $this->db->query("SELECT * FROM `tbl_faq`  WHERE `faq_category_id`='$id'  ORDER BY `order_no`")->result_array();
        }
        return $data;
	}
	public function get_news_and_blog_list($limit, $offset, $value = '')
	{
	    	// echo "<pre>";
				// print_r($limit);
				// print_r($offset);


			$this->db->select('n.`id`,n.`is_active`,n.`title`,n.`type`,n.date,c.name as category_name,n.photo_location,u.user_name,n.description
			,n.time,n.venue,n.google_map,n.short_description');
			$this->db->from('tbl_news_and_blog as n');
			$this->db->join('tbl_news_and_blog_category AS c', 'n.category_id=c.id');
			$this->db->join('tbl_user AS u', 'n.created_by=u.id','left');
			if (isset($value) && !empty($value) && isset($value['type'])  && $value['type'] != '') {
				$this->db->where('n.type', $value['type']);
			}
			if (isset($value) && !empty($value) && isset($value['is_active'])  && $value['is_active'] != '') {
				$this->db->where('n.is_active', $value['is_active']);
			}
			if (isset($value) && !empty($value) && isset($value['id'])  && $value['id'] != '') {
				$this->db->where('n.id', $value['id']);
			}
			if (isset($value) && !empty($value) && isset($value['category_id'])  && $value['category_id'] != '') {
				$this->db->where('n.category_id', $value['category_id']);
			}
			if (isset($limit) && $limit > 0) {
					$this->db->limit($limit, $offset);
			}
			$this->db->order_by('c.order_no asc,n.date desc');
			$query = $this->db->get();
			return $query->result_array();
	}
	public function get_latest_news_and_blog($type)
	{
		return $this->db->query("SELECT `title`,`date`,`photo_location`,`id` FROM `tbl_news_and_blog` WHERE `is_active`=1 and `type`='$type'  ORDER BY id DESC LIMIT 3")->result_array();
	}
	function get_news_and_blog_category_list($type)
	{
		return $this->db->query("SELECT DISTINCT c.`id`,c.`name` FROM `tbl_news_and_blog_category` AS c
														INNER JOIN `tbl_news_and_blog` AS n ON c.`id`=n.`category_id`
														WHERE c.is_active=1 AND n.`type`='$type'
														ORDER BY c.`order_no`")->result_array();
	}


	function get_course_category_list($type)
	{
		return $this->db->query("SELECT DISTINCT cc.`id`,cc.`name` FROM `tbl_course_shedules`  AS cs
														INNER JOIN tbl_courses AS c ON cs.`course_id`=c.`id`
								       			INNER JOIN `tbl_course_category` AS cc ON c.`course_category_id`=cc.`id`
											      WHERE cc.is_active=1 AND cs.`course_type`='$type' ORDER BY cc.`order_no`")->result_array();



	}


	function get_slider_list()
	{
		return $this->db->query("SELECT * FROM `tbl_documents` where is_active=1 and type='S' ORDER BY `order_no`")->result_array();
	}
	function get_total_member_count(){
		$result=$this->db->query("SELECT IFNULL(COUNT(`id`),0) AS total_member FROM  `tbl_member`")->row();
		return $result->total_member;
	}
	function get_total_program_count(){
		$result=$this->db->query("SELECT IFNULL(COUNT(`id`),0) AS total_program FROM  `tbl_course_shedules` WHERE `course_type` IN('Workshop','Seminar')")->row();
		return $result->total_program;
	}
	function get_dynamic_menu_list()
	{
				$data=$this->db->query("SELECT * FROM `tbl_menu` WHERE `is_active`='1' AND `parent_id`='0' ORDER BY `order`")->result_array();
				foreach( $data as $key=>$each ){
						$id=$each['id'];
						$second_menu= $this->db->query("SELECT * FROM `tbl_menu` WHERE `is_active`='1' AND `parent_id`='$id' ORDER BY `order`")->result_array();
						$data[$key]['second_menu_list']   =$second_menu;

						foreach ($second_menu as $key2 => $value) {
							$parent_id=$value['id'];
							$third_menu= $this->db->query("SELECT * FROM `tbl_menu` WHERE `is_active`='1' AND `parent_id`='$parent_id' ORDER BY `order`")->result_array();
							$data[$key]['second_menu_list'][$key2]['third_menu_list']=$third_menu;
						}
				}
				// echo "<pre>";
				// print_r($data);
				// die;
				return $data;
	}
	function get_content_type_by_menu_id($menu_id)
	{
		return $this->db->query("SELECT d.`menu_id`,  d.`content_type`,(SELECT `name` FROM `tbl_menu` WHERE id=d.`menu_id`) AS menu_name FROM `tbl_content_details` AS d
														WHERE d.`menu_id`='$menu_id' AND d.`is_active`='1'
														GROUP BY d.`content_type`
														ORDER BY d.`order_no`")->result_array();

	}
	function get_content_details_by_menu_and_type($menu_id,$type)
	{
		$data=array();
     if($type=='E')
		 {
			 $data= $this->db->query("SELECT d.*,m.`name` AS menu_name,e.`name` AS employee_name,e.`education_qualification`,e.`email`,e.`about`,
															e.`facebook`,e.`linkedin`,e.`twitter`,e.`youtube`,de.`name` AS designations,e.`photo_location` as employee_photo
															 FROM `tbl_content_details` AS d
															INNER JOIN `tbl_menu` AS m ON d.`menu_id`=m.`id`
															INNER JOIN `tbl_employee` AS e ON d.`employee_id`=e.`id`
															LEFT JOIN `designation` AS de ON e.`designation_id`=de.`id`
			 									WHERE d.`menu_id`='$menu_id' AND d.`is_active`='1' AND d.`content_type`='$type'
			 									ORDER BY d.`order_no`")->result_array();
		 }elseif ($type=='I') {
			 $data= $this->db->query("SELECT d.*,m.`name` AS menu_name,i.`name` AS instructor_name,i.`facebook`,i.`instagram`,i.`linkedin`,
				                       i.`email`,i.`about`,i.`mobile`,i.`twitter`,i.`photo_location` AS instructor_photo_location,
															(SELECT GROUP_CONCAT(d.`name`)  FROM `tbl_instructor_experties_details` AS ed
															INNER JOIN `designation` AS d ON ed.`experties_id`=d.`id`
															WHERE ed.`instructor_id`=i.id GROUP BY ed.instructor_id) AS expertice

															FROM `tbl_content_details` AS d
															INNER JOIN `tbl_menu` AS m ON d.`menu_id`=m.`id`
				                      INNER JOIN `tbl_instructor` AS i ON d.`instructor_id`=i.`id`
						 									WHERE d.`menu_id`='$menu_id' AND d.`is_active`='1' AND d.`content_type`='$type'
						 									ORDER BY d.`order_no`")->result_array();
		 }else{
		  $data= $this->db->query("SELECT d.*,m.`name` AS menu_name FROM `tbl_content_details` AS d
			                	INNER JOIN `tbl_menu` AS m ON d.`menu_id`=m.`id`
												WHERE d.`menu_id`='$menu_id' AND d.`is_active`='1' AND d.`content_type`='$type'
												ORDER BY d.`order_no`")->result_array();
		 }
		 return $data;
	}
	function get_about_content_details()
	{

			$data= $this->db->query("SELECT d.*,m.`name` AS menu_name FROM `tbl_content_details` AS d
												INNER JOIN `tbl_menu` AS m ON d.`menu_id`=m.`id`
												WHERE d.`menu_id`='24' AND d.`is_active`='1'
												ORDER BY d.`order_no`")->result_array();

		 return $data;
	}
	function get_area_of_interest_list()
	{
		return $this->db->query("SELECT * FROM `tbl_area_of_interest` WHERE is_active=1 ORDER BY `order_no`")->result_array();
	}
	function get_payment_done_by_shedule_id($course_shedule_id)
	{
		return $this->db->query("SELECT COUNT(cs.`id`) AS total_register_people FROM `tbl_course_shedules` AS cs
														INNER JOIN `tbl_enroll_details` AS ed ON  cs.`id`= ed.`course_shedule_id`
														WHERE ed.`course_shedule_id`='$course_shedule_id' AND ed.`payment_status`='A'")->row();
	}
	function get_teachings_list_by_type($type)
	{
		return $this->db->query("SELECT * FROM tbl_teachings WHERE `is_active`='1' AND `type`='$type' ORDER BY `order_no`")->result_array();
	}
	function get_image_list_by_type($type)
	{
		return $this->db->query("SELECT * FROM `tbl_image`  WHERE `is_active`='1' AND `category`='$type' ORDER BY `order_no`")->result_array();
	}
	function get_all_news_and_blog_by_type($type)
	{
				$data=$this->db->query("SELECT DISTINCT `year` FROM `tbl_news_and_blog` WHERE `is_active`='1' AND `type`='$type' ORDER BY `year` DESC")->result_array();
				foreach( $data as $key=>$each ){
						$year=$each['year'];
						$data[$key]['news_list']   = $this->db->query("SELECT * FROM `tbl_news_and_blog` WHERE `is_active`='1' AND `type`='$type' AND `year`='$year' ORDER BY `date` DESC")->result_array();
				}
				return $data;
	}

}
