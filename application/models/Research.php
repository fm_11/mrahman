<?php

class Research extends CI_Model
{
  function get_research_list()
  {
    return $this->db->query("SELECT * FROM `tbl_research` ORDER BY `order` asc")->result_array();
  }

  function add_researchs($data)
  {
      return $this->db->insert('tbl_research', $data);
  }

  function edit_researchs($data, $id)
  {
      return $this->db->update('tbl_research', $data, array('id' => $id));
  }

  function read_researchs($id)
  {
      return $this->db->get_where('tbl_research', array('id' => $id))->row();
  }

  function delete_researchs($id)
  {
      return $this->db->delete('tbl_research', array('id' => $id));
  }
  function get_research_list_for_home()
  {
    return $this->db->query("SELECT * FROM `tbl_research` Where is_active='1' ORDER BY `order` asc")->result_array();
  }
}
