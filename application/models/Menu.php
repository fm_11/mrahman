<?php

class Menu extends CI_Model
{
			public function __construct()
			{
				// Call the Model constructor
				parent::__construct();
			}

      function get_menu_list()
      {
        // $this->db->select('m.*');
        // $this->db->from('tbl_menu as m');
        // $this->db->order_by("m.`order`");
        // $query = $this->db->get();
        // return $query->result_array();
				return $this->db->query("SELECT c.*,m.name AS menu FROM `tbl_menu` AS c
						INNER JOIN
						(
						   SELECT f.`id`,(CASE WHEN f.pparent_name IS NULL THEN f.`name` ELSE CONCAT(f.`pparent_name`,' -> ',f.name) END) AS `name`,f.`order`
						     FROM (
							SELECT t.`id`,(CASE WHEN t.parent_name IS NULL THEN t.`name` ELSE CONCAT(t.`parent_name`,' -> ',t.name) END) AS `name`,
							(SELECT `name` FROM `tbl_menu` WHERE `id`=t.`pparent_id`) AS pparent_name,(SELECT `order` FROM `tbl_menu` WHERE `id`=t.`pparent_id`) AS `order`
							    FROM (
								SELECT m.`id`, m.`name`,(SELECT `name` FROM `tbl_menu` WHERE `id`=m.`parent_id`) AS parent_name,
								(SELECT `parent_id` FROM `tbl_menu` WHERE `id`=m.`parent_id`) AS pparent_id
								   FROM `tbl_menu` AS m
								   ORDER BY m.`order` ASC
								) AS t
							) AS f
						) AS m ON c.`id`=m.`id`")->result_array();
      }

			function add_menu($data)
		 {
				 return $this->db->insert('tbl_menu', $data);
		 }

			function edit_menu($data, $id)
 		 {
 				 return $this->db->update('tbl_menu', $data, array('id' => $id));
 		 }

		 function get_menu_dropdown_list()
		 {
				 return $this->db->query("SELECT t.`id`,(CASE WHEN t.parent_name IS NULL THEN t.`name` ELSE CONCAT(t.`parent_name`,' -> ',t.name) END) AS `name`
																 FROM (
																	SELECT m.`id`, m.`name`,(SELECT `name` FROM `tbl_menu` WHERE `id`=m.`parent_id`) AS parent_name,
																	(SELECT `parent_id` FROM `tbl_menu` WHERE `id`=m.`parent_id`) AS pparent_id
																	FROM `tbl_menu` AS m
																	WHERE m.`is_active`=1 AND m.`link_able`=0
																	ORDER BY m.`order` ASC
																      ) AS t")->result_array();
		 }

		 public function checkifexist_menu($name)
		 {
			   $count=$this->db->get_where('tbl_menu', array('name'=> $name))->num_rows();
			 if((int)$count==0){return 0;}else{return 1;}
		 }

		 public function get_content_max_order_by_menu_id($menu_id)
		 {
				 $result=$this->db->query("SELECT IFNULL(MAX(`order`),0) AS order_no FROM `tbl_menu` WHERE `id`=$menu_id")->row();
				 return $result->order_no;
		 }

		 public function get_child_by_parent_id($menu_id)
		 {
				 $result=$this->db->query("SELECT `child_of` FROM `tbl_menu` WHERE `id`=$menu_id")->row();
				 return $result->child_of;
		 }

		 public function checkifexist_update_menu_name($name,$id)
		 {
			   $count=$this->db->get_where('tbl_menu', array('id !='=> $id  , 'name'=> $name))->num_rows();
			 if((int)$count==0){return 0;}else{return 1;}
		 }

		 public function checkifexist_update_menu_child($id)
		 {
			   $count=$this->db->get_where('tbl_menu', array('parent_id !='=> $id))->num_rows();
			 if((int)$count==0){return 0;}else{return 1;}
		 }

		 function read_menu($id)
		 {
				 return $this->db->get_where('tbl_menu', array('id' => $id))->row();
		 }

		 function delete_menu($id)
		 {
				 return $this->db->delete('tbl_menu', array('id' => $id));
		 }
		 public function checkifexist_menu_for_any_dependency_by_menu_id($id)
 		{
 			$error_msg="";
 		 $count=	$this->db->query("SELECT `menu_id` FROM `tbl_content_details` where `menu_id`=$id")->num_rows();
 		 if((int)$count>0)
 		 {
        $error_msg="This menu cannot be deleted due to content details dependency.";
 		 }
		 $count=	$this->db->query("SELECT `parent_id` FROM `tbl_menu` where `parent_id`=$id")->num_rows();
		 if((int)$count>0)
 		 {
        $error_msg =$error_msg." This menu cannot be deleted due to sub menu dependency.";
 		 }
      return 	$error_msg;
 		}
		public function checkifexist_child_by_id($id)
	 {
		 $count=$this->db->get_where('tbl_menu', array('parent_id'=> $id))->num_rows();
    	if((int)$count==0){return 0;}else{return 1;}
	 }


}
