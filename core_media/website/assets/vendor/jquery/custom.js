/* 01. Css Loading Util */




function deleteConfirm() {
	var result = confirm("Are you sure to delete?");
	if (result == true) {
		return true;
	} else {
		return false;
	}
}

function image_validate(fileId, in_height, in_width) {

	//Get reference of FileUpload.
	var fileUpload = document.getElementById(fileId);
	var imagefilename = $('#' + fileId).val();
	if (imagefilename != '') {
		//Check whether the file is valid Image.
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
		if (regex.test(fileUpload.value.toLowerCase())) {

			//Check whether HTML5 is supported.
			if (typeof(fileUpload.files) != "undefined") {
				//Initiate the FileReader object.
				var reader = new FileReader();
				//Read the contents of Image File.
				reader.readAsDataURL(fileUpload.files[0]);
				reader.onload = function(e) {
					//Initiate the JavaScript Image object.
					var image = new Image();

					//Set the Base64 string return from FileReader as source.
					image.src = e.target.result;

					//Validate the File Height and Width.
					image.onload = function() {
						var height = this.height;
						var width = this.width;
						if (height > in_height || width > in_width) {
							$("#file_error").html("Height and Width must not exceed 200px.");
							return false;
						}
						$("#file_error").html("Uploaded image has valid Height and Width.");
						return true;
					};

				}
			} else {
				$("#file_error").html("This browser does not support HTML5.");
				return false;
			}
		} else {
			$("#file_error").html("Please select a valid Image file");
			$(".imageInputBoxForVal").css("border-color", "#FF0000");
			return false;
		}


		$("#file_error").html("");
		$(".imageInputBoxForVal").css("border-color", "#F0F0F0");
		var file_size = $('#' + fileId)[0].files[0].size;
		if (file_size > 2097152) {
			$("#file_error").html("File size is greater than 2MB");
			$(".imageInputBoxForVal").css("border-color", "#FF0000");
			return false;
		}
		return true;
	}
	return true;

}


$(function() {

	// $(".date").datepicker({
	// 	autoclose: true,
	// 	todayHighlight: true,
	// 	format: 'yyyy-mm-dd',
	// }).datepicker();
});
//Checker For Scripting
function checker(value) {

    var specialCharArray = [
        "<",
        ">",
        "type=\"javascript\""

    ]
    var count = 0;
    for (var i = 0; i < specialCharArray.length; i++) {
        if (value.toLowerCase().indexOf(specialCharArray[i]) > -1) {
            return false;
        }
    }
    return true;
}

function sqlChecker(value) {
    var sqlInjectionScript = [
			  "delete",
        "select",
        "from",
        "where",
        "update",
        "set",
        "="
    ]
    var count = 0;
    for (var i = 0; i < sqlInjectionScript.length; i++) {
        if (value.toLowerCase().indexOf(sqlInjectionScript[i]) > -1) {
            count++;
        }
        if (value.toLowerCase().indexOf("insert into") > -1) {
            return false;
        }
    }

    if (count > 1) {
        return false;
    }
    return true;
}
function SwitchStatement(expression)
{
    var number;
    switch (expression) {
        case "1":
            number = 9;
            break;
        case "2":
            number = 99;
            break;
        case "3":
            number = 999;
            break;
        case "4":
            number = 9999;
            break;
        case "5":
            number = 99999;
            break;
        case "6":
            number = 999999;
            break;
        case "7":
            number = 9999999;
            break;
        case "8":
            number = 99999999;
            break;
        case "9":
            number = 999999999;
            break;
        case "10":
            number = 9999999999;
            break;
        case "11":
            number = 99999999999;
            break;
        case "12":
            number = 999999999999;
            break;
        case "13":
            number = 9999999999999;
            break;
        case "14":
            number = 99999999999999;
            break;
        case "14":
            number = 999999999999999;
            break;
        case "15":
            number = 9999999999999999;
            break;
        default:
            number = 99999999999999999;
    }
    return number;
}





$(document).ready(function() {
	$("form.formSubmit").submit(function (e) {
		 $('.errMsg').hide();

		 var FractionalValue = "2"; //$("#NumberfieldFractional").val();
		 var DecimalValue = "12";//$("#NumberfieldDecimal").val();
		 var FractionalMaxValue = 0;
		 var DecimalMaxValue = 0;
		 if (typeof DecimalValue === "undefined") {

		 } else {
				 var errorMessage = "";
				 DecimalMaxValue = SwitchStatement(DecimalValue);
				 if (typeof FractionalValue === "undefined") {
						 // ...
				 } else {
						 FractionalMaxValue = SwitchStatement(FractionalValue);

				 }
		 }
		 var isOk = true;
		 $(".nameValidate").each(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNotValid = false;
        var regex = /^[0-9\s]*$/;
        var isScriptDetected = true;
        var isSqlDetected = true;
        isNotValid = regex.test(val);
        isScriptDetected = checker(val);
        isSqlDetected = sqlChecker(val);
        if (isNotValid) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter valid Input(Must contain at least one alphabetic character!)</i></div>");
            isOk = false;
        } else if (!isScriptDetected) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter a valid Input(No script allowed!).</i></div>");
            isOk = false;
        } else if (!isSqlDetected) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i> Please enter a valid Input(Sql query detected!).</i></div>");
            isOk = false;
        }
    });

		$(".embadedNameValidate").each(function () {
			 var parent = $(this).parent();
			 var isVisible = parent.is(":visible");
			 var val = $(this).val();
			 var isNotValid = false;
			 var regex = /^[0-9\s]*$/;
			 var isScriptDetected = true;
			 var isSqlDetected = true;
			 isNotValid = regex.test(val);
			 isSqlDetected = sqlChecker(val);
			 if (isNotValid) {
					 $(this).parent().find('.errMsg').empty();
					 $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter valid Input(Must contain at least one alphabetic character!)</i></div>");
					 isOk = false;
			 } else if (!isSqlDetected) {
					 $(this).parent().find('.errMsg').empty();
					 $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i> Please enter a valid Input(Sql query detected!).</i></div>");
					 isOk = false;
			 }
	 });
    //endforeach
		$(".codeValidate").each(function () {
			 var parent = $(this).parent();
			 var isVisible = parent.is(":visible");
			 var val = $(this).val();
			 var isNumeric = $.isNumeric(val);
			 if (isVisible && (val == "" || val == null)) {
					 $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input only alphanumeric characters. Field cannot be empty.</i></div>");
					 isOk = false;
			 }
	 });
	 $(".possitiveNumberfieldAndAlollowFractional").each(function () {
	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();
	         var isNumeric = $.isNumeric(val);
	         var IsError = false;
	         var parts = val.toString().split(".");
	         if (parts.length > 0) {
	             var decimal = parts[0].replace(/[^0-9\.]/g, '');
	             if (decimal > DecimalMaxValue) {
	                 IsError = true;
	             }
	         }
	         if (parts.length > 1) {
	             var fractional = parts[1].replace(/[^0-9\.]/g, '');
	             if (fractional > FractionalMaxValue) {
	                 IsError = true;
	             }
	         }
	         if (isVisible && (val == " " || val == null || val < 0 || !isNumeric || IsError)) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>The value cannot be greater than " + DecimalMaxValue + "." + FractionalMaxValue + "</i></div>");
	             isOk = false;
	         }
	     });
			 $(".requiredPossitiveNumberfieldAndAlollowFractional").each(function () {
			 			 				var parent = $(this).parent();
			 			 				var isVisible = parent.is(":visible");
			 			 				var val = $(this).val();
			 			 				var isNumeric = $.isNumeric(val);
			 			 				var IsError = false;
			 			 				var parts = val.toString().split(".");
			 			 				if (parts.length > 0) {
			 			 						var decimal = parts[0].replace(/[^0-9\.]/g, '');
			 			 						if (decimal > DecimalMaxValue) {
			 			 								IsError = true;
			 			 						}
			 			 				}
			 			 				if (parts.length > 1) {
			 			 						var fractional = parts[1].replace(/[^0-9\.]/g, '');
			 			 						if (fractional > FractionalMaxValue) {
			 			 								IsError = true;
			 			 						}
			 			 				}
			 			 				if (isVisible && (val == " " || val == null || val < 1 || !isNumeric || IsError)) {
			 			 						$(this).parent().find('.errMsg').empty();
			 			 						$(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>The value cannot be greater than " + DecimalMaxValue + "." + FractionalMaxValue + "</i></div>");
			 			 						isOk = false;
			 			 				}
			 			 		});

	     $(".percentRange").each(function () {
	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();
	         //debugger;
	         if (val > 100 || val < 1) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input between 1 to 100 in percentage.</i></div>");
	             isOk = false;
	         }
	     });
	     $(".percentRangeTax").each(function () {

	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();
	         //debugger;
	         if (val > 100 || val < 1) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input between 1 to 100 in percentage.</i></div>");
	             isOk = false;
	         }
	     });
	     $(".percentRangeZeroAccepted").each(function () {

	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();
	         //debugger;
	         if (val == "" || val == null || val > 100 || val < 0) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input between 0 to 100 in percentage.</i></div>");
	             isOk = false;
	         }
	     });
	     $(".ddlErrorMsg").each(function () {

	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();

	         if (isVisible && (val == " " || val == null || val == 0)) {
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please select an option.</i></div>");
	             isOk = false;
	         }
	     });

	     $(".numberfieldMonth").each(function () {

	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();
	         var isNumeric = $.isNumeric(val);

	         if (isVisible && (val == " " || val == null || val > 31 || val < 1 || !isNumeric)) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input between 1 to 31.</i></div>");
	             IsErrorMonth = true;
	             isOk = false;
	         }
	     });

	     $(".scriptValidate").each(function () {
	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();
	         var isScriptDetected = true;
	         var isSqlDetected = true;
	         isScriptDetected = checker(val);
	         isSqlDetected = sqlChecker(val);
	         if (!isScriptDetected) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter a valid Input(No script allowed!).</i></div>");
	             isOk = false;
	         } else if (!isSqlDetected) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i> Please enter a valid Input(Sql query detected!).</i></div>");
	             isOk = false;
	         }

	     });

	     $(".requiredScriptValidate").each(function () {
	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();
	         var isScriptDetected = true;
	         var isSqlDetected = true;
	         isScriptDetected = checker(val);
	         isSqlDetected = sqlChecker(val);

	         if (val == '' || val == null) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>This field cannot be empty.</i></div>");
	             isOk = false;
	         }

	         if (!isScriptDetected) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter a valid Input(No script allowed!).</i></div>");
	             isOk = false;
	         } else if (!isSqlDetected) {
	             $(this).parent().find('.errMsg').empty();
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i> Please enter a valid Input(Sql query detected!).</i></div>");
	             isOk = false;
	         }

	     });

			 $(".requiredDateErrMsg").each(function () {
			        var parent = $(this).parent();
			        var isVisible = parent.is(":visible");
			        var val = $(this).val();
			        if (!Date.parse(val)) {
			            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please select a date.</i></div>");
			            isOk = false;
			        }
			    })

			    $(".numberfieldErrorMsg").each(function () {
			        var parent = $(this).parent();
			        var isVisible = parent.is(":visible");
			        var val = $(this).val();
			        var isNumeric = $.isNumeric(val);
			        //alert(isNumeric);
			        if (isVisible && (val == " " || val == null || val < 1 || !isNumeric)) {
			            //alert(val);
			            $(this).parent().find('.errMsg').empty();
			            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Required a number greater than 0.</i></div>");
			            isOk = false;
			        }
			    });

			    $(".numberfieldErrorMsgZero").each(function () {
			        var parent = $(this).parent();
			        var isVisible = parent.is(":visible");
			        var val = $(this).val();
			        var isNumeric = $.isNumeric(val);
			        //alert(isNumeric);
			        if (isVisible && (val == " " || val == null || val < 0 || !isNumeric)) {
			            //alert(val);
			            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input positive number.</i></div>");
			            isOk = false;
			        }
			    });

			    $(".monthfieldErrorMsg").each(function () {
			        var parent = $(this).parent();
			        var isVisible = parent.is(":visible");
			        var val = $(this).val();
			        var isNumeric = $.isNumeric(val);
			        //alert(isNumeric);
			        if (isVisible && (val == " " || val == null || val < 1 || !isNumeric)) {
			            //alert(val);
			            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input positive number between 1 to 999.</i></div>");
			            isOk = false;
			        }
			    });


    $(".numberfieldIntZero").each(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        if (isVisible && (val == " " || val == null || val < 0 || !isNumeric)) {
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input zero or positive number. No fractional value allowed.</i></div>");
            isOk = false;
        }
    });
    $(".numberfieldMaxIntZero").each(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //alert(isNumeric);
        if (isVisible && (val == " " || val == null || val < 0 || !isNumeric)) {
            //alert(val);
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input zero or positive number. No fractional value allowed.</i></div>");
            isOk = false;
        }
    });

    $(".numberfieldInt").each(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //alert(isNumeric);
        if (isVisible && (val == " " || val == null || val < 1 || !isNumeric)) {
            //alert(val);
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Required a number greater than 0. No fractional value allowed.</i></div>");
            isOk = false;
        }
    });
		$(".optionalNumberfieldValidation").each(function () {
	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();
	         var isNumeric = $.isNumeric(val);
	         //alert(isNumeric);
	         if (val != "") {
	             if (isVisible && (val < 1 || !isNumeric)) {
	                 //alert(val);
	                 $(this).parent().find('.errMsg').empty();
	                 $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Required a number greater than 0.</i></div>");
	                 isOk = false;
	             }
	         }
	     });
	     //End

	     $(".ddlErrorMsgForZero").each(function () {
	         var parent = $(this).parent();
	         var isVisible = parent.is(":visible");
	         var val = $(this).val();
	         if (isVisible && (val == " " || val == null || !val)) {
	             $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please select an option.</i></div>");
	             isOk = false;
	         }
	     });


	 if (!isOk)
		e.preventDefault();
//extends


	});
});






$(document).ready(function () {
    //reset all form field
    $('#btnReset').click(function () {
        $('.formSubmit').trigger('reset');
        $('.formSubmit select:not(:disabled)').val(null).trigger('change');
        $('.form-control').val(null);
        $('.formSubmit input:checkbox').removeAttr('checked');
        $('.formSubmit input:radio').removeAttr('checked');
        $('.errMsg').empty();
		    $('.error').empty();
			  $('.formSubmit input').removeClass('is-invalid');

    });
    //end reset form



    $(".numberfieldErrorMsg").keyup(function (e) {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //var val = $(this).val();
        this.value = this.value.replace(/[^0-9\.]/g, '');

        var showmsg = false;
        if (isVisible && (val == " " || val == null || val < 1 || !isNumeric)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Required a number greater than 0.</i></div>");
        }
        //alert(showmsg);
    })



    $(".numberfieldErrorMsgZero").keyup(function (e) {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //var val = $(this).val();
        this.value = this.value.replace(/[^0-9\.]/g, '');

        var showmsg = false;
        if (isVisible && (val == " " || val == null || val < 0 || !isNumeric)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input positive number.</i></div>");
        }
        //alert(showmsg);
    })

    $(".monthfieldErrorMsg").keyup(function (e) {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //var val = $(this).val();
        this.value = this.value.replace(/[^0-9\.]/g, '');

        var showmsg = false;
        if (isVisible && (val == " " || val == null || val < 1 || !isNumeric)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input positive number between 1 to 999.</i></div>");
        }
        //alert(showmsg);
    })

    $(".numberfieldIntZero").keyup(function (e) {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //var val = $(this).val();
        this.value = this.value.replace(/[^0-9\.]/g, '');

        var showmsg = false;
        if (isVisible && (val == " " || val == null || val < 0 || !isNumeric)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input zero or positive number. No fractional value allowed.</i></div>");
        }
        //alert(showmsg);
    })


    $(".numberfieldMaxIntZero").keyup(function (e) {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //var val = $(this).val();
        this.value = this.value.replace(/[^0-9\.]/g, '');

        var showmsg = false;
        if (isVisible && (val == " " || val == null || val < 0 || !isNumeric)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input zero or positive number. No fractional value allowed.</i></div>");
        }
        //alert(showmsg);
    })

    $(".numberfieldInt").keyup(function (e) {

        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //var val = $(this).val();
        this.value = this.value.replace(/[^0-9\.]/g, '');

        var showmsg = false;
        if (isVisible && (val == " " || val == null || val < 1 || !isNumeric)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Required a number greater than 0. No fractional value allowed.</i></div>");
        }
        //alert(showmsg);
    });
    /////////////////////////////

    $(".numberfieldMonth").keyup(function (e) {

        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);

        var showmsg = false;
        if (isVisible && (val == " " || val == null || val > 31 || val < 1 || !isNumeric)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input between 1 to 31.</i></div>");
        }

        //alert(showmsg);
    });
    //////////////////////////////
    var FractionalValue = "2"; //$("#NumberfieldFractional").val();
    var DecimalValue = "12";//$("#NumberfieldDecimal").val();
    var FractionalMaxValue = 0;
    var DecimalMaxValue = 0;
    if (typeof DecimalValue === "undefined") {

    } else {
        var errorMessage = "";
        DecimalMaxValue = SwitchStatement(DecimalValue);
        if (typeof FractionalValue === "undefined") {
            // ...
        } else {
            FractionalMaxValue = SwitchStatement(FractionalValue);

        }
    }

    $(".possitiveNumberfieldAndAlollowFractional").keyup(function (e) {

        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        var IsError = false;
        var parts = val.toString().split(".");
        if (parts.length > 0) {
            var decimal = parts[0].replace(/[^0-9\.]/g, '');

            if (decimal > DecimalMaxValue) {
                IsError = true;
            }
        }
        if (parts.length > 1) {
            var fractional = parts[1].replace(/[^0-9\.]/g, '');
            if (fractional > FractionalMaxValue) {
                IsError = true;
            }
        }
        if (isVisible && (val == " " || val == null || val < 0 || !isNumeric || IsError)) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>The value cannot be greater than " + DecimalMaxValue + "." + FractionalMaxValue + "</i></div>");

        } else {
            $(this).parent().find('.errMsg').empty();
        }

    })

		$(".requiredPossitiveNumberfieldAndAlollowFractional").on("change", function () {

				var parent = $(this).parent();
				var isVisible = parent.is(":visible");
				var val = $(this).val();
				var isNumeric = $.isNumeric(val);
				var IsError = false;
				var parts = val.toString().split(".");
				if (parts.length > 0) {
						var decimal = parts[0].replace(/[^0-9\.]/g, '');

						if (decimal > DecimalMaxValue) {
								IsError = true;
						}
				}
				if (parts.length > 1) {
						var fractional = parts[1].replace(/[^0-9\.]/g, '');
						if (fractional > FractionalMaxValue) {
								IsError = true;
						}
				}
				if (isVisible && (val == " " || val == null || val < 1 || !isNumeric || IsError)) {
						$(this).parent().find('.errMsg').empty();
						$(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>The value cannot be greater than " + DecimalMaxValue + "." + FractionalMaxValue + "</i></div>");

				} else {
						$(this).parent().find('.errMsg').empty();
				}

		});



    $(".codeValidate").keypress(function (event) {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        var showmsg = false;
        //var val = $(this).val();
        if (this.value == "" || this.value == null) {
            showmsg = true;
        } else {
            showmsg = false;
        }

        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input only alphanumeric characters. Field cannot be empty.</i></div>");
        }
        //alert(showmsg);
    })
    $(".realnumberfield").keyup(function (e) {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        this.value = this.value.replace(/(?!^-)[^0-9]/g, '');


        var showmsg = false;
        if (isVisible && (val == " " || val == null || !isNumeric)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input real number.</i></div>");
        }
        //alert(showmsg);
    });
		$(".percentRange").on("change", function () {
        var val = Math.abs(parseFloat(this.value));
        if (val > 100 || val < 1) {
            //this.value = 100;
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input between 1 to 100 in percentage.</i></div>");
        }
    });
    $(".percentRangeTax").on("change", function () {
        var val = Math.abs(parseFloat(this.value));
        if (val > 100 || val < 1) {
            //this.value = 100;
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input between 1 to 100 in percentage.</i></div>");
        }
        else {
            $(this).parent().find('.errMsg').empty();
        }
    });
    $(".percentRangeZeroAccepted").on("change", function () {
        var val = Math.abs(parseFloat(this.value));
        if (val > 100 || val < 0) {
            //this.value = 100;
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input between 0 to 100 in percentage.</i></div>");
        }
        else {
            $(this).parent().find('.errMsg').empty();
        }
    });

    $(".optionalNumberfieldValidation").keyup(function () {

        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //var val = $(this).val();
        var showmsg = false;
        if (isVisible && (val == " " || val < 1 || !isNumeric)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Required a number greater than 0.</i></div>");
        }
        //alert(showmsg);
    })

    $(".ddlErrorMsg").change(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //var val = $(this).val();
        var showmsg = false;
        if (isVisible && (val == " " || val == null || val == 0)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please select an option.</i></div>");
        }
        //alert(showmsg);
    })

    $(".ddlErrorMsgForZero").change(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isNumeric = $.isNumeric(val);
        //var val = $(this).val();
        var showmsg = false;
        if (isVisible && (val == " " || val == null || !val)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please select an option.</i></div>");
        }
        //alert(showmsg);
    })

    $(".requiredDateErrMsg").change(function () {

        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var showmsg = false;
        if (!Date.parse(val)) {
            showmsg = true;

        } else {
            showmsg = false;

        }
        if (!showmsg) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please select a date.</i></div>");
        }
    });


    $(".nameValidate").blur(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isValid = false;
        var showmsg = false;
        var showmsg2 = false;
        var showmsg3 = false;
        var regex = /^[0-9\s]*$/;
        isValid = regex.test(val);

        if (!checker(val)) {
            showmsg = true;

        } else if (!sqlChecker(val)) {
            showmsg2 = true;

        } else if (isVisible && isValid) {
            showmsg3 = true;
        }
        else {
            showmsg = false;
            showmsg2 = false;
            showmsg3 = false;
        }
        if (!showmsg && !showmsg2 && !showmsg3) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        } else if (showmsg2) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter a valid Input(Sql query detected!).</i></div>");
        } else if (showmsg3) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter valid Input(Must contain at least one alphabetic character!)</i></div>");
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i> Please enter a valid Input(No script allowed!).</i></div>");
        }
    });
		$(".embadedNameValidate").blur(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isValid = false;
        var showmsg = false;
        var showmsg2 = false;
        var showmsg3 = false;
        var regex = /^[0-9\s]*$/;
        isValid = regex.test(val);

       if (!sqlChecker(val)) {
            showmsg2 = true;

        } else if (isVisible && isValid) {
            showmsg3 = true;
        }
        else {
            showmsg = false;
            showmsg2 = false;
            showmsg3 = false;
        }
        if (!showmsg && !showmsg2 && !showmsg3) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        } else if (showmsg2) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter a valid Input(Sql query detected!).</i></div>");
        } else if (showmsg3) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter valid Input(Must contain at least one alphabetic character!)</i></div>");
        }
        else {
            $(this).parent().find('.errMsg').empty();
        }
    });

		$(".codeValidate").blur(function () {
			var parent = $(this).parent();
			var isVisible = parent.is(":visible");
			var val = $(this).val();
			var isNumeric = $.isNumeric(val);
			var showmsg = false;
			//var val = $(this).val();
			if (this.value == "" || this.value == null) {
					showmsg = true;
			} else {
					showmsg = false;
			}

			if (!showmsg) {
					$(this).parent().find('.errMsg').empty();
					//$('.errMsg').hide();
			}
			else {
					$(this).parent().find('.errMsg').empty();
					$(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please input only alphanumeric characters. Field cannot be empty.</i></div>");
			}
		})

    $(".requiredNumberAllowedTB").blur(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var showmsg = false;
        var showmsg2 = false;
        var showmsg3 = false;

        if (!checker(val)) {
            showmsg = true;

        } else if (!sqlChecker(val)) {
            showmsg2 = true;

        } else if (isVisible && !val) {
            showmsg3 = true;
        }
        else {
            showmsg = false;
            showmsg2 = false;
            showmsg3 = false;
        }
        if (!showmsg && !showmsg2 && !showmsg3) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        } else if (showmsg2) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter a valid Input(Sql query detected!).</i></div>");
        } else if (showmsg3) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter an input(Field can not be empty!)</i></div>");
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i> Please enter a valid Input(No script allowed!).</i></div>");
        }
    })

    $(".scriptValidate").blur(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isValid = false;
        var showmsg = false;
        var showmsg2 = false;
        var regex = /^[0-9\s]*$/;
        isValid = regex.test(val);

        if (!checker(val)) {
            showmsg = true;

        } else if (!sqlChecker(val)) {
            showmsg2 = true;

        }
        else {
            showmsg = false;
            showmsg2 = false;
        }
        if (!showmsg && !showmsg2) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        } else if (showmsg2) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter a valid Input(Sql query detected!).</i></div>");
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i> Please enter a valid Input(No script allowed!).</i></div>");
        }
    })

    $(".requiredScriptValidate").blur(function () {
        var parent = $(this).parent();
        var isVisible = parent.is(":visible");
        var val = $(this).val();
        var isValid = false;
        var showmsg = false;
        var showmsg2 = false;
        var regex = /^[0-9\s]*$/;
        isValid = regex.test(val);

        if (!checker(val)) {
            showmsg = true;

        } else if (!sqlChecker(val)) {
            showmsg2 = true;

        }
        else {
            showmsg = false;
            showmsg2 = false;
        }
        if (!showmsg && !showmsg2) {
            $(this).parent().find('.errMsg').empty();
            //$('.errMsg').hide();
        } else if (showmsg2) {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i>Please enter a valid Input(Sql query detected!).</i></div>");
        }
        else {
            $(this).parent().find('.errMsg').empty();
            $(this).parent().append("<div class='errMsg' style='color:#cc0000;margin-top:5px;'><i> Please enter a valid Input(No script allowed!).</i></div>");
        }
    });

	});
	$(function () {
        //Rakib
        $('.allowNumberAndOnlyDecimalPoint').keypress(function (e) {
            if (e.which === 69) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
      .on(".allowNumberAndOnlyDecimalPoint", function (e) {
          e.preventDefault();
      });
        $('.percentRangeZeroAccepted').keypress(function (e) {
            if (e.which === 69) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".percentRangeZeroAccepted", function (e) {
            e.preventDefault();
        });
        $('.numberfieldMonth').keypress(function (e) {
          //  alert(e.which);
            if (e.which === 69) return false;
            if (e.which === 46) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
       .on(".numberfieldMonth", function (e) {
           e.preventDefault();
       });

			 $('.requiredPossitiveNumberfieldAndAlollowFractional').keypress(function (e) {
			 					if (e.which === 69) return false;
			 					if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
			 			})
			 		.on(".requiredPossitiveNumberfieldAndAlollowFractional", function (e) {
			 				e.preventDefault();
			 		});
					
        //Rakib
        $('.possitiveNumberfieldAndAlollowFractional').keypress(function (e) {
            if (e.which === 69) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
       .on(".possitiveNumberfieldAndAlollowFractional", function (e) {
           e.preventDefault();
       });
        $('.numberfieldAllowZero').keypress(function (e) {
           // if (this.value.length > 8) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".numberfieldAllowZero", function (e) {
            e.preventDefault();
        })
        //allowing only number and 15 digit
        $('.numberfieldErrorMsg').keypress(function (e) {
            if (this.value.length > 8) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".numberfieldErrorMsg", function (e) {
            e.preventDefault();
        });

        $('.numberfieldErrorMsgZero').keypress(function (e) {
            if (this.value.length > 8) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".numberfieldErrorMsgZero", function (e) {
            e.preventDefault();
        });

        $('.monthfieldErrorMsg').keypress(function (e) {
            if (this.value.length > 2) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".monthfieldErrorMsg", function (e) {
            e.preventDefault();
        });
        //end

        //allowing non fractional number with 15 digit max, 0 allowed
        $('.numberfieldIntZero').keypress(function (e) {
            if (e.which === 46) return false;
            if (this.value.length > 9) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".numberfieldIntZero", function (e) {
            e.preventDefault();
        });

        $('.loanYear').keypress(function (e) {
            if (e.which === 46) return false;
            if (this.value.length > 1) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".loanYear", function (e) {
            e.preventDefault();
        });

        $('.percentRange').keypress(function (e) {
            if (e.which === 46) return false;
            if (this.value.length > 2) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".percentRange", function (e) {
            e.preventDefault();
        });

        //end

        //code special character block
        $('.codeValidate').keypress(function (event) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                return false;
            }
        })
        .on(".codeValidate", function (e) {
            e.preventDefault();
        });
        //end code

        //Rakibul
        $('.numberfieldMaxIntZero').keypress(function (e) {
            if (e.which === 46) return false;
            if (this.value.length > 8) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        //Rakibul
       .on(".numberfieldMaxIntZero", function (e) {
           e.preventDefault();
       });
        //Rakibul
        $('.numberfieldMaxIntZero,.numberfieldIntZero,.numberfieldErrorMsg,.loanYear').on("paste", function (e) {
            e.preventDefault();
        });
        //allowing non fractional number with 15 digit max, 0 not allowed
        $('.numberfieldInt').keypress(function (e) {
            if (e.which === 46) return false;
            if (this.value.length > 15) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".numberfieldInt", function (e) {
            e.preventDefault();
        });
        //end
        $('.realnumberfield').keypress(function (e) {
            if (this.value.length > 8) return false;
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
        .on(".realnumberfield", function (e) {
            e.preventDefault();
        });
    });
